﻿
namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirbinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrircToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boutonEnregistrerSous = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerbinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compilerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affichageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simulateurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rAMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pCSPCLKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vHDLFichierTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codeSimuléToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editeurDeTexteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView_dataMemory = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valeur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView_registers = new System.Windows.Forms.DataGridView();
            this.Registre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REG_valeur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView_simulation = new System.Windows.Forms.DataGridView();
            this.Column_etape = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_instruction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_operation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_instructionAssembleur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_resultat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView_instructionMemory = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_dataMemory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_simulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_instructionMemory)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.richTextBox1.Location = new System.Drawing.Point(0, 63);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(509, 528);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.affichageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(132, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouvrirToolStripMenuItem,
            this.sauvegarderToolStripMenuItem,
            this.compilerToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // ouvrirToolStripMenuItem
            // 
            this.ouvrirToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouvrirbinToolStripMenuItem,
            this.ouvrircToolStripMenuItem});
            this.ouvrirToolStripMenuItem.Name = "ouvrirToolStripMenuItem";
            this.ouvrirToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.ouvrirToolStripMenuItem.Text = "Ouvrir";
            // 
            // ouvrirbinToolStripMenuItem
            // 
            this.ouvrirbinToolStripMenuItem.Name = "ouvrirbinToolStripMenuItem";
            this.ouvrirbinToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.ouvrirbinToolStripMenuItem.Text = "Ouvrir .bin";
            this.ouvrirbinToolStripMenuItem.Click += new System.EventHandler(this.ouvrirbinToolStripMenuItem_Click);
            // 
            // ouvrircToolStripMenuItem
            // 
            this.ouvrircToolStripMenuItem.Name = "ouvrircToolStripMenuItem";
            this.ouvrircToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.ouvrircToolStripMenuItem.Text = "Ouvrir .c";
            this.ouvrircToolStripMenuItem.Click += new System.EventHandler(this.ouvrircToolStripMenuItem_Click);
            // 
            // sauvegarderToolStripMenuItem
            // 
            this.sauvegarderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boutonEnregistrerSous,
            this.enregistrerbinToolStripMenuItem});
            this.sauvegarderToolStripMenuItem.Name = "sauvegarderToolStripMenuItem";
            this.sauvegarderToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.sauvegarderToolStripMenuItem.Text = "Enregistrer";
            this.sauvegarderToolStripMenuItem.Click += new System.EventHandler(this.sauvegarderToolStripMenuItem_Click);
            // 
            // boutonEnregistrerSous
            // 
            this.boutonEnregistrerSous.Name = "boutonEnregistrerSous";
            this.boutonEnregistrerSous.Size = new System.Drawing.Size(153, 22);
            this.boutonEnregistrerSous.Text = "Enregistrer .c";
            this.boutonEnregistrerSous.Click += new System.EventHandler(this.boutonEnregistrerSous_Click);
            // 
            // enregistrerbinToolStripMenuItem
            // 
            this.enregistrerbinToolStripMenuItem.Name = "enregistrerbinToolStripMenuItem";
            this.enregistrerbinToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.enregistrerbinToolStripMenuItem.Text = "Enregistrer .bin";
            // 
            // compilerToolStripMenuItem
            // 
            this.compilerToolStripMenuItem.Name = "compilerToolStripMenuItem";
            this.compilerToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.compilerToolStripMenuItem.Text = "Compiler";
            // 
            // affichageToolStripMenuItem
            // 
            this.affichageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simulateurToolStripMenuItem,
            this.vHDLFichierTestToolStripMenuItem,
            this.codeSimuléToolStripMenuItem,
            this.editeurDeTexteToolStripMenuItem});
            this.affichageToolStripMenuItem.Name = "affichageToolStripMenuItem";
            this.affichageToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.affichageToolStripMenuItem.Text = "Affichage";
            // 
            // simulateurToolStripMenuItem
            // 
            this.simulateurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rAMToolStripMenuItem,
            this.registresToolStripMenuItem,
            this.pCSPCLKToolStripMenuItem});
            this.simulateurToolStripMenuItem.Name = "simulateurToolStripMenuItem";
            this.simulateurToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.simulateurToolStripMenuItem.Text = "Simulateur";
            // 
            // rAMToolStripMenuItem
            // 
            this.rAMToolStripMenuItem.Name = "rAMToolStripMenuItem";
            this.rAMToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.rAMToolStripMenuItem.Text = "RAM";
            // 
            // registresToolStripMenuItem
            // 
            this.registresToolStripMenuItem.Name = "registresToolStripMenuItem";
            this.registresToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.registresToolStripMenuItem.Text = "Registres";
            // 
            // pCSPCLKToolStripMenuItem
            // 
            this.pCSPCLKToolStripMenuItem.Name = "pCSPCLKToolStripMenuItem";
            this.pCSPCLKToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.pCSPCLKToolStripMenuItem.Text = "PC/SP/CLK";
            // 
            // vHDLFichierTestToolStripMenuItem
            // 
            this.vHDLFichierTestToolStripMenuItem.Name = "vHDLFichierTestToolStripMenuItem";
            this.vHDLFichierTestToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.vHDLFichierTestToolStripMenuItem.Text = "VHDL Fichier test";
            // 
            // codeSimuléToolStripMenuItem
            // 
            this.codeSimuléToolStripMenuItem.Name = "codeSimuléToolStripMenuItem";
            this.codeSimuléToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.codeSimuléToolStripMenuItem.Text = "Code simulé";
            // 
            // editeurDeTexteToolStripMenuItem
            // 
            this.editeurDeTexteToolStripMenuItem.Name = "editeurDeTexteToolStripMenuItem";
            this.editeurDeTexteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.editeurDeTexteToolStripMenuItem.Text = "Editeur de texte";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Compiler et Exécuter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(1040, 61);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Suivant";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(1795, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Quitter";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dataGridView_dataMemory
            // 
            this.dataGridView_dataMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_dataMemory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_dataMemory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_dataMemory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Valeur});
            this.dataGridView_dataMemory.Location = new System.Drawing.Point(1316, 654);
            this.dataGridView_dataMemory.Name = "dataGridView_dataMemory";
            this.dataGridView_dataMemory.RowHeadersVisible = false;
            this.dataGridView_dataMemory.RowTemplate.Height = 25;
            this.dataGridView_dataMemory.Size = new System.Drawing.Size(305, 244);
            this.dataGridView_dataMemory.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Adresse";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 73;
            // 
            // Valeur
            // 
            this.Valeur.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Valeur.HeaderText = "Valeur";
            this.Valeur.Name = "Valeur";
            // 
            // dataGridView_registers
            // 
            this.dataGridView_registers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_registers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_registers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_registers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Registre,
            this.REG_valeur});
            this.dataGridView_registers.Location = new System.Drawing.Point(1040, 654);
            this.dataGridView_registers.Name = "dataGridView_registers";
            this.dataGridView_registers.RowHeadersVisible = false;
            this.dataGridView_registers.RowTemplate.Height = 25;
            this.dataGridView_registers.Size = new System.Drawing.Size(270, 244);
            this.dataGridView_registers.TabIndex = 8;
            this.dataGridView_registers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.selectedRowsButton_Click);
            // 
            // Registre
            // 
            this.Registre.HeaderText = "Registre";
            this.Registre.Name = "Registre";
            this.Registre.Width = 74;
            // 
            // REG_valeur
            // 
            this.REG_valeur.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.REG_valeur.HeaderText = "Valeur";
            this.REG_valeur.Name = "REG_valeur";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "PC instr";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(138, 34);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "Exécuter simulation";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridView_simulation
            // 
            this.dataGridView_simulation.AllowUserToAddRows = false;
            this.dataGridView_simulation.AllowUserToDeleteRows = false;
            this.dataGridView_simulation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_simulation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_simulation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_simulation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_etape,
            this.column_adresse,
            this.column_instruction,
            this.column_operation,
            this.column_instructionAssembleur,
            this.column_resultat});
            this.dataGridView_simulation.Location = new System.Drawing.Point(1040, 90);
            this.dataGridView_simulation.Name = "dataGridView_simulation";
            this.dataGridView_simulation.RowTemplate.Height = 25;
            this.dataGridView_simulation.Size = new System.Drawing.Size(824, 528);
            this.dataGridView_simulation.TabIndex = 10;
            this.dataGridView_simulation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.selectedRowsButton_Click);
            // 
            // Column_etape
            // 
            this.Column_etape.HeaderText = "Etape";
            this.Column_etape.Name = "Column_etape";
            this.Column_etape.Width = 61;
            // 
            // column_adresse
            // 
            this.column_adresse.HeaderText = "Adresse";
            this.column_adresse.Name = "column_adresse";
            this.column_adresse.Width = 73;
            // 
            // column_instruction
            // 
            this.column_instruction.HeaderText = "Instruction";
            this.column_instruction.Name = "column_instruction";
            this.column_instruction.Width = 89;
            // 
            // column_operation
            // 
            this.column_operation.HeaderText = "Opération";
            this.column_operation.Name = "column_operation";
            this.column_operation.Width = 85;
            // 
            // column_instructionAssembleur
            // 
            this.column_instructionAssembleur.HeaderText = "Instruction assembleur";
            this.column_instructionAssembleur.Name = "column_instructionAssembleur";
            this.column_instructionAssembleur.Width = 139;
            // 
            // column_resultat
            // 
            this.column_resultat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.column_resultat.HeaderText = "Résultat";
            this.column_resultat.Name = "column_resultat";
            // 
            // dataGridView_instructionMemory
            // 
            this.dataGridView_instructionMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_instructionMemory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_instructionMemory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_instructionMemory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView_instructionMemory.Location = new System.Drawing.Point(1627, 654);
            this.dataGridView_instructionMemory.Name = "dataGridView_instructionMemory";
            this.dataGridView_instructionMemory.RowHeadersVisible = false;
            this.dataGridView_instructionMemory.RowTemplate.Height = 25;
            this.dataGridView_instructionMemory.Size = new System.Drawing.Size(243, 244);
            this.dataGridView_instructionMemory.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Adresse";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 73;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Instruction";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.richTextBox2.Location = new System.Drawing.Point(515, 63);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(508, 528);
            this.richTextBox2.TabIndex = 12;
            this.richTextBox2.Text = "";
            this.richTextBox2.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 30);
            this.label1.TabIndex = 13;
            this.label1.Text = "Code";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(515, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 30);
            this.label2.TabIndex = 14;
            this.label2.Text = "VHDL";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(1040, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 30);
            this.label3.TabIndex = 15;
            this.label3.Text = "Simulation";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(1316, 621);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 30);
            this.label4.TabIndex = 16;
            this.label4.Text = "Mémoire de données";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(1627, 621);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 30);
            this.label5.TabIndex = 17;
            this.label5.Text = "Mémoire d\'instructions";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(1040, 621);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 30);
            this.label6.TabIndex = 18;
            this.label6.Text = "Registres";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.richTextBox3.Location = new System.Drawing.Point(12, 654);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(1022, 244);
            this.richTextBox3.TabIndex = 19;
            this.richTextBox3.Text = "";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(12, 621);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 30);
            this.label7.TabIndex = 20;
            this.label7.Text = "Console";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.richTextBox2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Location = new System.Drawing.Point(12, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 591);
            this.panel1.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1882, 910);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView_instructionMemory);
            this.Controls.Add(this.dataGridView_simulation);
            this.Controls.Add(this.dataGridView_registers);
            this.Controls.Add(this.dataGridView_dataMemory);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_dataMemory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_simulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_instructionMemory)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirbinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrircToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderbinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem affichageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simulateurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rAMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pCSPCLKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vHDLFichierTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codeSimuléToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editeurDeTexteToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dataGridView_dataMemory;
        private System.Windows.Forms.DataGridView dataGridView_registers;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripMenuItem boutonEnregistrerSous;
        private System.Windows.Forms.ToolStripMenuItem enregistrerbinToolStripMenuItem;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView_simulation;
        private System.Windows.Forms.DataGridView dataGridView_instructionMemory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_etape;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_instruction;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_operation;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_instructionAssembleur;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_resultat;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Registre;
        private System.Windows.Forms.DataGridViewTextBoxColumn REG_valeur;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valeur;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
    }
}

