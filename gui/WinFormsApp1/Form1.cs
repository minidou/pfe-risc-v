﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using CliWrap;
using Nito.AsyncEx;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        static string GCC_PATH = "gcc-riscv/";
        static string SHELL_PATH = "tools/MinGW/msys/1.0/bin/sh.exe";
        static string SHELL_TOOLS = "tools/MinGW/msys/1.0/bin/";
        static string RISCV_TOOLCHAINS = "tools/riscv32-unknown-elf/bin/";
        static string ROOT_FOLDER = "../../../../../";



        static string binFilePath = ROOT_FOLDER + "gcc-riscv/main.bin";
        string instructionMemory = string.Empty;
        static int breakpoint = 0;
        //static string vhdlPath = "C:\\Users\\tritr\\source\\repos\\pfe-risc-v\\testbench_Generator\\output\\testbench.vhd";
        static string vhdlPath = string.Empty;
        static Boolean fileIsC = false;
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // Bouton Enregis
        private void sauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        OpenFileDialog openFileBin = new OpenFileDialog();
        OpenFileDialog openFileC = new OpenFileDialog();
        private String bashOutput = string.Empty;

        // Ouverture de fichier binaire
        private void ouvrirbinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "./";
                openFileDialog.Filter = "bin files (*.bin)|*.bin|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                    Console.WriteLine(filePath);
                    binFilePath = filePath.Replace("\\", "/");
                    Console.WriteLine(binFilePath);
                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                    fileIsC = false;

                }
            }
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = ROOT_FOLDER + SHELL_PATH;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.Arguments = "";
            //"-l -x -c 'export PATH=\"$PATH:$PWD\"' 'cd C:/Users/tritr/source/repos/pfe-risc-v/gcc-riscv/' ./compil.sh"
            using (Process exeProcess = Process.Start(startInfo))
            {
                using (StreamWriter str = exeProcess.StandardInput)
                {
                    str.WriteLine("cd " + ROOT_FOLDER);
                    str.Flush();
                    str.WriteLine("export PATH=$PATH:$PWD/" + SHELL_TOOLS);
                    str.Flush();
                    str.WriteLine("export PATH=$PATH:$PWD/" + RISCV_TOOLCHAINS);
                    str.Flush();
                    str.WriteLine("riscv32-unknown-elf-objdump.exe -b binary --adjust-vma=0x0 -D " + binFilePath + " -m riscv");
                    str.Flush();
                }

                exeProcess.WaitForExit();
                string output = exeProcess.StandardOutput.ReadToEnd();
                string error = exeProcess.StandardError.ReadToEnd();

                richTextBox1.AppendText(output);
                richTextBox3.AppendText(error);
            }


        }

        // Ouverture de fichier .c
        private void ouvrircToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "./";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = File.OpenRead(filePath);

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                    fileIsC=true;
                }
            }
            richTextBox1.Text = fileContent;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous quitter ?", "Quitter", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void boutonEnregistrerSous_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog enregistrerSous = new SaveFileDialog();
            String filePath  = string.Empty;
            enregistrerSous.Filter = "c files (*.c)|*.c|All files (*.*)|*.*";
            enregistrerSous.FilterIndex = 2;
            enregistrerSous.RestoreDirectory = true;

            if(enregistrerSous.ShowDialog() == DialogResult.OK)
            {
                filePath=enregistrerSous.FileName;


                    using (myStream = File.Open(filePath, FileMode.OpenOrCreate))
                    using (StreamWriter sw = new StreamWriter(myStream))
                    {
                        sw.Write(richTextBox1.Text);
                    }

                    myStream.Close();

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (binFilePath != string.Empty)
            {
                dataGridView_simulation.Rows.Clear();
                List<string[]> linesSimulation = new List<string[]>();
                List<string[]> linesInstructionMemory = new List<string[]>();
                List<string[]> linesRegisters = new List<string[]>();
                List<string[]> linesDataMemory = new List<string[]>();
                int linecounter = 0;
                if (vhdlPath == String.Empty)
                {
                    SaveFileDialog enregistrerSous = new SaveFileDialog();
                    enregistrerSous.Filter = "vhd files (*.vhd)|*.vhd|All files (*.*)|*.*";
                    enregistrerSous.FilterIndex = 2;
                    enregistrerSous.RestoreDirectory = true;

                    if (enregistrerSous.ShowDialog() == DialogResult.OK)
                    {
                        vhdlPath = enregistrerSous.FileName;
                    }
                }
                Process proc;

                proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "../../../../../simulator/code/simulator.sh",
                        // args : input output breakpoint -g
                        Arguments = binFilePath + " " + vhdlPath + " " + breakpoint + " " + " -g",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }

                };

                proc.Start();
                while (!proc.StandardOutput.EndOfStream)
                {

                    string line = proc.StandardOutput.ReadLine();
                    line = line.Trim();
                    if (line.StartsWith("Simulation"))
                    {
                        linecounter = 0;
                        continue;
                    }

                    if (line.StartsWith("Instruction memory"))
                    {
                        linecounter = 1;
                        continue;

                    }

                    if (line.StartsWith("Registers"))
                    {
                        linecounter = 2;
                        continue;
                    }

                    if (line.StartsWith("Data Memory"))
                    {
                        linecounter = 3;
                        continue;

                    }

                    if (linecounter == 0)
                    {
                        string[] content = line.Split(':');
                        linesSimulation.Add(content);
                    }
                    else if (linecounter == 1)
                    {
                        string[] content = line.Split(':');
                        linesInstructionMemory.Add(content);
                    }
                    else if (linecounter == 2)
                    {
                        string[] content = line.Split(':');
                        linesRegisters.Add(content);
                    }
                    else if (linecounter == 3)
                    {
                        string[] content = line.Split(':');
                        linesDataMemory.Add(content);
                    }


                    // Do something with line

                }
                foreach (var c in linesInstructionMemory)
                {
                    dataGridView_instructionMemory.Rows.Add(c[0], c[1]);
                }

                foreach (var c in linesDataMemory)
                {
                    dataGridView_dataMemory.Rows.Add(c[0], c[1]);
                }

                int i = 0;
                string etape = string.Empty;
                string adresse = string.Empty;
                string instruction = string.Empty;
                string operation = string.Empty;
                string operationAssembleur = string.Empty;
                string resultat = string.Empty;
                foreach (var c in linesSimulation)
                {
                    if (i % 2 == 0)
                    {

                        etape = c[0];
                        adresse = c[1];
                        instruction = c[2];
                        operation = c[3];
                        operationAssembleur = c[4];
                        etape = String.Concat(etape.Where(c => !Char.IsWhiteSpace(c)));
                        instruction = String.Concat(instruction.Where(c => !Char.IsWhiteSpace(c)));
                        operation = String.Concat(operation.Where(c => !Char.IsWhiteSpace(c)));
                        operationAssembleur = String.Concat(operationAssembleur.Where(c => !Char.IsWhiteSpace(c)));

                    }
                    else
                    {
                        resultat = c[0];
                        resultat = String.Concat(resultat.Where(c => !Char.IsWhiteSpace(c)));
                        dataGridView_simulation.Rows.Add(etape, adresse, instruction, operation, operationAssembleur, resultat);
                    }

                    i++;

                }
                foreach (var c in linesRegisters)
                {
                    dataGridView_registers.Rows.Add(c[0], c[1]);

                }
                //Read the contents of the file into a stream
                var fileStream = File.OpenRead(vhdlPath);
                string fileContent = string.Empty;
                using (StreamReader reader = new StreamReader(fileStream))
                {
                    fileContent = reader.ReadToEnd();
                }

                richTextBox2.Text = fileContent;
            }
        }

        private void dataGridView_registery_CellContentClick(object sender, EventArgs e)
        {
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void selectedRowsButton_Click(object sender, System.EventArgs e)
        {
            Int32 selectedRowCount =
            dataGridView_simulation.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {

                for (int i = 0; i < selectedRowCount; i++)
                {
                    breakpoint = dataGridView_simulation.SelectedRows[i].Index + 1;
                }

            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (fileIsC)
            {
                string text = richTextBox1.Text.Trim();
                if ( text != "")
                {
                    Stream myStream;
                    SaveFileDialog enregistrerSous = new SaveFileDialog();
                    String filePath = "../../../../../gcc-riscv/main.c";

                    using (myStream = File.Open(filePath, FileMode.OpenOrCreate))
                    using (StreamWriter sw = new StreamWriter(myStream))
                    {
                        sw.Write(richTextBox1.Text);
                    }

                    myStream.Close();
                }
            }

            /*
                var stdOutBuffer = new StringBuilder();
                var stdErrBuffer = new StringBuilder();
                var result = await Cli.Wrap("../../../../../gcc-riscv/compil.sh")
                //.WithArguments("./compil.sh")
                .WithWorkingDirectory("C:\\Users\\tritr\\source\\repos\\pfe-risc-v")
                .WithStandardOutputPipe(PipeTarget.ToStringBuilder(stdOutBuffer))
                .WithStandardErrorPipe(PipeTarget.ToStringBuilder(stdErrBuffer))
                .ExecuteAsync();

                var stdOut = stdOutBuffer.ToString();
                var stdErr = stdErrBuffer.ToString();
                richTextBox1.AppendText(stdOut);
                richTextBox1.AppendText(stdErr);
                richTextBox1.AppendText("test");
            */

            string command = "ls";
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = ROOT_FOLDER+SHELL_PATH;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.Arguments = "";
            //"-l -x -c 'export PATH=\"$PATH:$PWD\"' 'cd C:/Users/tritr/source/repos/pfe-risc-v/gcc-riscv/' ./compil.sh"
            using (Process exeProcess = Process.Start(startInfo))
            {
             using (StreamWriter str = exeProcess.StandardInput)
                {
                    str.WriteLine("cd " + ROOT_FOLDER);
                    str.Flush();
                    str.WriteLine("export PATH=$PATH:$PWD/"+SHELL_TOOLS);
                    str.Flush();
                    str.WriteLine("cd " + GCC_PATH);
                    str.Flush();
                    str.WriteLine("./compil.sh");
                    str.Flush();
                }

                exeProcess.WaitForExit();
                string output = exeProcess.StandardOutput.ReadToEnd();
                string error = exeProcess.StandardError.ReadToEnd();

                richTextBox1.AppendText(output);
                richTextBox3.AppendText(error);
            }

            /*
            var process = Process.Start(startInfo);
            process.WaitForExit();

            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            var exitCode = process.ExitCode;

            process.Close();

            richTextBox1.AppendText(output);
            richTextBox1.AppendText(error);
            */
            

        }
       
    }
}
