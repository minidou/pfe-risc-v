
main.elf:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <_start>:
   0:	00001137          	lui	sp,0x1
   4:	034000ef          	jal	ra,38 <main>
   8:	00100073          	ebreak
   c:	0000006f          	j	c <_start+0xc>

00000010 <getCode7Seg>:
  10:	fe050513          	addi	a0,a0,-32
  14:	0ff57513          	zext.b	a0,a0
  18:	03f00793          	li	a5,63
  1c:	00a7ea63          	bltu	a5,a0,30 <getCode7Seg+0x20>
  20:	13800793          	li	a5,312
  24:	00a787b3          	add	a5,a5,a0
  28:	0007c503          	lbu	a0,0(a5)
  2c:	00008067          	ret
  30:	07f00513          	li	a0,127
  34:	00008067          	ret

00000038 <main>:
  38:	ff010113          	addi	sp,sp,-16 # ff0 <s+0xe78>
  3c:	00912223          	sw	s1,4(sp)
  40:	00812423          	sw	s0,8(sp)
  44:	17800413          	li	s0,376
  48:	01212023          	sw	s2,0(sp)
  4c:	00112623          	sw	ra,12(sp)
  50:	04440913          	addi	s2,s0,68
  54:	17800493          	li	s1,376
  58:	0004c503          	lbu	a0,0(s1)
  5c:	00148493          	addi	s1,s1,1
  60:	fb1ff0ef          	jal	ra,10 <getCode7Seg>
  64:	fea48fa3          	sb	a0,-1(s1)
  68:	fe9918e3          	bne	s2,s1,58 <main+0x20>
  6c:	000107b7          	lui	a5,0x10
  70:	f0078793          	addi	a5,a5,-256 # ff00 <s+0xfd88>
  74:	ffa00713          	li	a4,-6
  78:	04300613          	li	a2,67
  7c:	00ff0eb7          	lui	t4,0xff0
  80:	00078313          	mv	t1,a5
  84:	800008b7          	lui	a7,0x80000
  88:	04400e13          	li	t3,68
  8c:	00170593          	addi	a1,a4,1
  90:	0ff7e513          	ori	a0,a5,255
  94:	00b66663          	bltu	a2,a1,a0 <main+0x68>
  98:	ffb44503          	lbu	a0,-5(s0)
  9c:	00f56533          	or	a0,a0,a5
  a0:	00270693          	addi	a3,a4,2
  a4:	ff0007b7          	lui	a5,0xff000
  a8:	00d66663          	bltu	a2,a3,b4 <main+0x7c>
  ac:	ffc44783          	lbu	a5,-4(s0)
  b0:	01879793          	slli	a5,a5,0x18
  b4:	00370813          	addi	a6,a4,3
  b8:	01d7e6b3          	or	a3,a5,t4
  bc:	01066863          	bltu	a2,a6,cc <main+0x94>
  c0:	ffd44683          	lbu	a3,-3(s0)
  c4:	01069693          	slli	a3,a3,0x10
  c8:	00f6e6b3          	or	a3,a3,a5
  cc:	00470813          	addi	a6,a4,4
  d0:	0066e7b3          	or	a5,a3,t1
  d4:	01066863          	bltu	a2,a6,e4 <main+0xac>
  d8:	ffe44783          	lbu	a5,-2(s0)
  dc:	00879793          	slli	a5,a5,0x8
  e0:	00d7e7b3          	or	a5,a5,a3
  e4:	00570713          	addi	a4,a4,5
  e8:	0ff7e693          	ori	a3,a5,255
  ec:	00e66663          	bltu	a2,a4,f8 <main+0xc0>
  f0:	fff44683          	lbu	a3,-1(s0)
  f4:	00f6e6b3          	or	a3,a3,a5
  f8:	00d8a223          	sw	a3,4(a7) # 80000004 <s+0x7ffffe8c>
  fc:	00a8a423          	sw	a0,8(a7)
 100:	00140413          	addi	s0,s0,1
 104:	01c59e63          	bne	a1,t3,120 <main+0xe8>
 108:	00c12083          	lw	ra,12(sp)
 10c:	00812403          	lw	s0,8(sp)
 110:	00412483          	lw	s1,4(sp)
 114:	00012903          	lw	s2,0(sp)
 118:	01010113          	addi	sp,sp,16
 11c:	00008067          	ret
 120:	00030793          	mv	a5,t1
 124:	00b66663          	bltu	a2,a1,130 <main+0xf8>
 128:	ffa44783          	lbu	a5,-6(s0)
 12c:	00879793          	slli	a5,a5,0x8
 130:	00058713          	mv	a4,a1
 134:	f59ff06f          	j	8c <main+0x54>

Disassembly of section .rodata:

00000138 <CSWTCH.1>:
 138:	7fff                	0x7fff
 13a:	7f7f                	0x7f7f
 13c:	7f7f                	0x7f7f
 13e:	7f7f                	0x7f7f
 140:	7f7f                	0x7f7f
 142:	7f7f                	0x7f7f
 144:	bf7f                	0xbf7f
 146:	7f7f                	0x7f7f
 148:	f9c0                	fsw	fs0,52(a1)
 14a:	b0a4                	fsd	fs1,96(s1)
 14c:	9299                	srli	a3,a3,0x26
 14e:	f882                	fsw	ft0,112(sp)
 150:	9080                	0x9080
 152:	7f7f                	0x7f7f
 154:	7f7f                	0x7f7f
 156:	7f7f                	0x7f7f
 158:	887f                	0x887f
 15a:	86a1c683          	lbu	a3,-1942(gp)
 15e:	c28e                	sw	gp,68(sp)
 160:	8ae1fb8b          	0x8ae1fb8b
 164:	a3abaac7          	fmsub.d	fs5,fs7,fs10,fs4,rdn
 168:	988c                	0x988c
 16a:	c38792af          	0xc38792af
 16e:	95b5                	srai	a1,a1,0x2d
 170:	9189                	srli	a1,a1,0x22
 172:	7fa4                	flw	fs1,120(a5)
 174:	7f7f                	0x7f7f
 176:	f77f                	0xf77f

Disassembly of section .data:

00000178 <s>:
 178:	5250                	lw	a2,36(a2)
 17a:	452d434f          	0x452d434f
 17e:	2d204543          	0x2d204543
 182:	4c20                	lw	s0,88(s0)
 184:	4455                	li	s0,21
 186:	4349564f          	fnmadd.d	fa2,fs2,fs4,fs0,unknown
 18a:	2d20                	fld	fs0,88(a0)
 18c:	5020                	lw	s0,96(s0)
 18e:	5541                	li	a0,-16
 190:	204c                	fld	fa1,128(s0)
 192:	202d                	jal	1bc <s+0x44>
 194:	554a                	lw	a0,176(sp)
 196:	494c                	lw	a1,20(a0)
 198:	5445                	li	s0,-15
 19a:	4554                	lw	a3,12(a0)
 19c:	2d20                	fld	fs0,88(a0)
 19e:	5120                	lw	s0,96(a0)
 1a0:	4555                	li	a0,21
 1a2:	544e                	lw	s0,240(sp)
 1a4:	4e49                	li	t3,18
 1a6:	2d20                	fld	fs0,88(a0)
 1a8:	5920                	lw	s0,112(a0)
 1aa:	5353554f          	fnmadd.d	fa0,ft6,fs5,fa0,unknown
 1ae:	4850554f          	fnmadd.s	fa0,ft0,ft5,fs1,unknown
 1b2:	2d20                	fld	fs0,88(a0)
 1b4:	5020                	lw	s0,96(s0)
 1b6:	4549                	li	a0,18
 1b8:	5252                	lw	tp,52(sp)
 1ba:	0045                	c.nop	17

Disassembly of section .riscv.attributes:

00000000 <.riscv.attributes>:
   0:	1b41                	addi	s6,s6,-16
   2:	0000                	unimp
   4:	7200                	flw	fs0,32(a2)
   6:	7369                	lui	t1,0xffffa
   8:	01007663          	bgeu	zero,a6,14 <getCode7Seg+0x4>
   c:	0011                	c.nop	4
   e:	0000                	unimp
  10:	1004                	addi	s1,sp,32
  12:	7205                	lui	tp,0xfffe1
  14:	3376                	fld	ft6,376(sp)
  16:	6932                	flw	fs2,12(sp)
  18:	7032                	flw	ft0,44(sp)
  1a:	0030                	addi	a2,sp,8

Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347          	fmsub.d	ft6,ft6,ft4,ft7,rmm
   4:	2820                	fld	fs0,80(s0)
   6:	736f7263          	bgeu	t5,s6,72a <s+0x5b2>
   a:	6f6f7473          	csrrci	s0,0x6f6,30
   e:	2d6c                	fld	fa1,216(a0)
  10:	474e                	lw	a4,208(sp)
  12:	3120                	fld	fs0,96(a0)
  14:	322e                	fld	ft4,232(sp)
  16:	2e34                	fld	fa3,88(a2)
  18:	2e30                	fld	fa2,88(a2)
  1a:	3035                	jal	fffff846 <s+0xfffff6ce>
  1c:	5f30                	lw	a2,120(a4)
  1e:	3835                	jal	fffff85a <s+0xfffff6e2>
  20:	6534                	flw	fa3,72(a0)
  22:	3735                	jal	ffffff4e <s+0xfffffdd6>
  24:	2965                	jal	4dc <s+0x364>
  26:	3120                	fld	fs0,96(a0)
  28:	2e31                	jal	344 <s+0x1cc>
  2a:	2e32                	fld	ft8,264(sp)
  2c:	0030                	addi	a2,sp,8
