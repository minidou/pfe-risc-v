#!/bin/bash
export  PATH="$PATH:../tools/riscv32-unknown-elf/bin/"

riscv32-unknown-elf-as -march=rv32i -mabi=ilp32 res/vectors.s -o vectors.o
riscv32-unknown-elf-gcc -march=rv32i -mabi=ilp32 -nostdlib -nostartfiles -ffreestanding -c -Os input/main.c -o main.o
riscv32-unknown-elf-ld vectors.o main.o -T res/memmap -o main.elf
riscv32-unknown-elf-objdump -D main.elf > out/dump_ASM.txt
riscv32-unknown-elf-objcopy main.elf -O binary out/main.bin

rm main.elf main.o vectors.o 