#include <stdint.h>

// adresses in data memory
#define AD_CPT32    *((volatile uint32_t *)0x80000000)
#define AD_DISPLAY1 *((volatile uint32_t *)0x80000004)   // MSB=Hex3, Hex2, Hex1, LSB=Hex0 (Hex described in DE10LITE User manual)
#define AD_DISPLAY2 *((volatile uint32_t *)0x80000008)   // MSB=X, X, Hex5, LSB=Hex4
#define AD_LEDS *((volatile uint32_t *)0x8000000C)
#define MASTER_CLK_RATE 50 * 1000000
#define PLL_DIV  50
#define CLK_RATE    MASTER_CLK_RATE/PLL_DIV
#define MS_TICK    CLK_RATE/1000

uint8_t s[] = "PROC-ECE - LUDOVIC - PAUL - JULIETTE - QUENTIN - YOUSSOUPH - PIERRE";

uint8_t getCode7Seg(uint8_t c);
uint8_t seg7ToChar(uint8_t c);
void printString(uint32_t disp1, uint32_t disp2);

void main(void)
{
    uint32_t strLength, pos, i;
    uint32_t disp1, disp2;
    
    strLength = sizeof(s);

    for(i=0; i<strLength; i++)
    {
        s[i]=getCode7Seg(s[i]);
    }

    // for(;;)
    // {
        for(pos=0; pos<strLength+6; pos++)
        {
        // hex5
            i=6;
            if((pos-i)>=0 && (pos-i)<strLength) disp2=((uint8_t)s[pos-i] &0xff) << 8; else disp2=0xff << 8;
            // hex4
            i--;
            if((pos-i)>=0 && (pos-i)<strLength) disp2|=((uint8_t)s[pos-i] &0xff); else disp2|=0xff;
            // hex3
            i--;
            if((pos-i)>=0 && (pos-i)<strLength) disp1=((uint8_t)s[pos-i] &0xff) << 24; else disp1=0xff << 24;
            // hex2
            i--;
            if((pos-i)>=0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff) << 16; else disp1|=0xff << 16;
            // hex1
            i--;
            if((pos-i)>=0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff) << 8; else disp1|=0xff << 8;
            // hex0
            i--;
            if((pos-i)>=0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff); else disp1|=0xff;
            
            AD_DISPLAY1 = disp1;
            AD_DISPLAY2 = disp2;
            // AD_LEDS = leds[i];
        }
        // while(AD_CPT32 < (MS_TICK * 500));  //500ms delay
        // AD_CPT32 = 0;
}
// return the corresponding code of the specified uint8_t to be displayed on 7 segments display
// encoded value is coded as shown in "DE10Lite User Manual". '0' to light a segment.
uint8_t getCode7Seg(uint8_t c) 
{
	uint8_t hexVal = 0;
    
    switch(c) 
    {
        case '0' : hexVal = 0xc0; break;
        case '1' : hexVal = 0xf9; break;
        case '2' : hexVal = 0xa4; break;
        case '3' : hexVal = 0xb0; break;
        case '4' : hexVal = 0x99; break;
        case '5' : hexVal = 0x92; break;
        case '6' : hexVal = 0x82; break;
        case '7' : hexVal = 0xf8; break;
        case '8' : hexVal = 0x80; break;
        case '9' : hexVal = 0x90; break;
        case 'A' : hexVal = 0x88; break;
        case 'B' : hexVal = 0x83; break;
        case 'C' : hexVal = 0xc6; break;
        case 'D' : hexVal = 0xa1; break;
        case 'E' : hexVal = 0x86; break;
        case 'F' : hexVal = 0x8e; break;
        case 'G' : hexVal = 0xc2; break;
        case 'H' : hexVal = 0x8b; break;
        case 'I' : hexVal = 0xfb; break;
        case 'J' : hexVal = 0xe1; break;
        case 'K' : hexVal = 0x8a; break;
        case 'L' : hexVal = 0xc7; break;
        case 'M' : hexVal = 0xaa; break;
        case 'N' : hexVal = 0xab; break;
        case 'O' : hexVal = 0xa3; break;
        case 'P' : hexVal = 0x8c; break;
        case 'Q' : hexVal = 0x98; break;
        case 'R' : hexVal = 0xaf; break;
        case 'S' : hexVal = 0x92; break;
        case 'T' : hexVal = 0x87; break;
        case 'U' : hexVal = 0xc3; break;
        case 'V' : hexVal = 0xb5; break;
        case 'W' : hexVal = 0x95; break;
        case 'X' : hexVal = 0x89; break;
        case 'Y' : hexVal = 0x91; break;
        case 'Z' : hexVal = 0xa4; break;
        case ' ' : hexVal = 0xff; break;
        case '.' : hexVal = 0x7f; break;
        case '-' : hexVal = 0xbf; break;
        case '_' : hexVal = 0xf7; break;
        default  : hexVal = 0x7f; break;
	}
    return hexVal;
}