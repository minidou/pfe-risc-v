#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/*
* Program binary file path as input. Then the program will execute each command
* and write the vhd file to test the command sequence.
*/

uint16_t instrCount = 0;
uint32_t instrSize = 0;
uint8_t* instrMem = NULL;
uint8_t dataMem[4096] = {0};
uint32_t reg[32] = {0};
uint32_t PC = 0;
uint8_t regsOn;

/* bit field are purely indicative of the length used, we use bitwise mask when we fill
* the structure */

struct command
{
    uint32_t opcode : 7;
    uint32_t rd     : 5;
    uint32_t funct3 : 3;
    uint32_t rs1    : 5;
    uint32_t rs2    : 5;
    uint32_t funct7 : 7;
    uint32_t imm5   : 5;
    uint32_t imm7   : 7;
    uint32_t imm12  : 12;
    uint32_t imm20  : 20;
    uint32_t instr  : 32;
};

/* Function declaration */
void printVerbose(void);
void printGui(void);
void structBuilding(uint32_t instruction, struct command* instr);
void readInstrFile(const char* filepath);
void initVHDLFile(FILE **VHDLFile, const char* filepath);
void endVHDLFile(FILE *VHDLFile);
void printInstr(struct command* instr);
void initSimulation(void);

void executeInstr(struct command* instr, FILE *VHDLFile);

void executeLUI(struct command* instr, FILE *VHDLFile);
void executeAUIPC(struct command* instr, FILE *VHDLFile);
void executeJAL(struct command* instr, FILE *VHDLFile);
void executeJALR(struct command* instr, FILE *VHDLFile);
void executeBRANCH(struct command* instr, FILE *VHDLFile);
void executeLOAD(struct command* instr, FILE *VHDLFile);
void executeSTORE(struct command* instr, FILE *VHDLFile);
void executeOP_IMM(struct command* instr, FILE *VHDLFile);
void executeOP(struct command* instr, FILE *VHDLFile);
void executeMISC_MEM(struct command* instr, FILE *VHDLFile);
void executeSYSTEM(struct command* instr, FILE *VHDLFile);

/* write uint8_t, uint16_t or uint32_t data in dataMem[] with
* a little endianess */
void static inline writeDataMemory(uint32_t addr, uint32_t data, uint8_t sizeInByte)
{
    if (addr < sizeof(dataMem))
    {
        switch (sizeInByte)
        {
            case 1:
                dataMem[addr] = data & 0x000000ff;
            break;

            case 2:
                dataMem[addr]   = data & 0x000000ff;
                dataMem[addr+1] = (data >> 8) & 0x000000ff;
            break;

            case 4:
                dataMem[addr]   = data & 0x000000ff;
                dataMem[addr+1] = (data >> 8) & 0x000000ff;
                dataMem[addr+2] = (data >> 16) & 0x000000ff;
                dataMem[addr+3] = (data >> 24) & 0x000000ff;
            break;
            default: break;
        }
    }
    else
    {
        printf("***OOM***");
    }
}

/* Return uint8_t, uint16_t or uint32_t data store in uint32_t with
* a little endianess */
uint32_t static inline readDataMemory(uint32_t addr, uint8_t sizeInByte)
{
    uint32_t data = 0;
    
    if (addr < sizeof(dataMem))
    {
        switch (sizeInByte)
        {
            case 1:
                data = dataMem[addr];
            break;

            case 2:
                data = dataMem[addr] | (dataMem[addr+1] << 8);
            break;

            case 4:
                data = dataMem[addr]           |
                      (dataMem[addr+1] << 8)   |
                      (dataMem[addr+2] << 16)  |
                      (dataMem[addr+3] << 24);
            break;
            default: break;
        }
    }
    else
    {
        printf("***OOM***");
    }
    return data;
}

/* Return uint32_t instruction with a little endianess */
uint32_t static inline readProgramMemory(uint32_t addr)
{
    uint32_t instruction = instrMem[addr]           |
                          (instrMem[addr+1] << 8)   |
                          (instrMem[addr+2] << 16)  |
                          (instrMem[addr+3] << 24);
    return instruction;
}

void static inline assertRegs(uint16_t step, FILE *VHDLFile)
{
    fprintf(VHDLFile, "\t\t\tassert reg00 = x\"00000000\" report \"reg00 error at step %d\" severity error;\n", step);

    for (uint8_t i = 1 ; i < sizeof(reg)/sizeof(uint32_t) ; i++)
    {
        fprintf(VHDLFile, "\t\t\tassert reg%.2x = x\"%.8x\" report \"reg%.2x error at step %d\" severity error;\n", i, reg[i], i, step);
    }
}

/* Define to add register to test bench */
#define ASSERT_REG 1
enum
{
    OP_LUI      = 0b0110111,
    OP_AUIPC    = 0b0010111,
    OP_JAL      = 0b1101111,
    OP_JALR     = 0b1100111,
    OP_BRANCH   = 0b1100011,
    OP_LOAD     = 0b0000011,
    OP_STORE    = 0b0100011,
    OP_OP_IMM   = 0b0010011,
    OP_OP       = 0b0110011,
    OP_MISC_MEM = 0b0001111,
    OP_SYSTEM   = 0b1110011
} OPcode_enum;

enum
{
    F3_JALR    = 0b000,
    F3_BEQ     = 0b000,
    F3_BNE     = 0b001,
    F3_BLT     = 0b100,
    F3_BGE     = 0b101,
    F3_BLTU    = 0b110,
    F3_BGEU    = 0b111,
    F3_LB      = 0b000,
    F3_LH      = 0b001,
    F3_LW      = 0b010,
    F3_LBU     = 0b100,
    F3_LHU     = 0b101,
    F3_SB      = 0b000,
    F3_SH      = 0b001,
    F3_SW      = 0b010,
    F3_ADDI    = 0b000,
    F3_SLTI    = 0b010,
    F3_SLTIU   = 0b011,
    F3_XORI    = 0b100,
    F3_ORI     = 0b110,
    F3_ANDI    = 0b111,
    F3_SLLI    = 0b001,
    F3_SRLI    = 0b101,
    F3_SRAI    = 0b101,
    F3_ADD     = 0b000,
    F3_SUB     = 0b000,
    F3_SLL     = 0b001,
    F3_SLT     = 0b010,
    F3_SLTU    = 0b011,
    F3_XOR     = 0b100,
    F3_SRL     = 0b101,
    F3_SRA     = 0b101,
    F3_OR      = 0b110,
    F3_AND     = 0b111,
    F3_FENCE   = 0b000,
    F3_FENCE_I = 0b001,
    F3_ECALL   = 0b000,
    F3_EBREAK  = 0b000,
    F3_CSRRW   = 0b001,
    F3_CSRRS   = 0b010,
    F3_CSRRC   = 0b011,
    F3_CSRRWI  = 0b101,
    F3_CSRRSI  = 0b110,
    F3_CSRRCI  = 0b111
} funct3_enum;

/* Only funct7 different from 0 are useful but we choose to enumerate all informations found in data sheet*/
enum
{
    F7_SLLI    = 0b0000000,
    F7_SRLI    = 0b0000000,
    F7_SRAI    = 0b0100000,
    F7_ADD     = 0b0000000,
    F7_SUB     = 0b0100000,
    F7_SLL     = 0b0000000,
    F7_SLT     = 0b0000000,
    F7_SLTU    = 0b0000000,
    F7_XOR     = 0b0000000,
    F7_SRL     = 0b0000000,
    F7_SRA     = 0b0100000,
    F7_OR      = 0b0000000,
    F7_AND     = 0b0000000
} funct7_enum;

void main(int argc, char** argv)
{
    /* Declare a pointer that will point on VHDL Test bench file*/
    FILE *VHDLFile;
    struct command instr;
    /* Read the file passed in argument of the script execution */
    readInstrFile(argv[1]);
    /* Init the simulation with SP / RA / FP values set in config_enum */
    initSimulation();
    /* Init VHDL file with all the static information as reg port and signal names */
    initVHDLFile(&VHDLFile, argv[2]);
    /* set breakpoint limit*/
    uint16_t breakpoint = atoi(argv[3]);
    /* declare argument value */
    uint8_t verbose = 0, guiReport = 0, i = 4;
    regsOn = 0;
    /* Check if argument exist and set true argument */
    while ( argv[i] != NULL)
    {
        if (argv[i][1] == 'v') verbose = 1;
        if (argv[i][1] == 'g') guiReport = 1;
        if (argv[i][1] == 'r') regsOn = 1;
        i++;
    }

    printf("Simulation with verbose:%d GUI:%d RegOn:%d \n\n", verbose, guiReport, regsOn);
    /* loop as long RA did not come back to its init value or breakpoint reached */
    do
    {
        /* print in console execution infos */
        printf("%3d : 0x%.8x : 0x%.8x : ", instrCount, PC,readProgramMemory(PC));
        /* reset struct data */
        memset(&instr, 0, sizeof(struct command));
        /* Build the structure with the instruction returned at the PC addr in instrMem[] */
        structBuilding(readProgramMemory(PC), &instr);
        /* Execute the instruction */
        executeInstr(&instr, VHDLFile);
        /* reset hard-wired zero register in case an instruction edited the register */
        reg[0] = 0;
        instrCount++;
    }
    while (PC != 8 && instrCount != breakpoint);

    endVHDLFile(VHDLFile);

    if (verbose == 1)
    {
        /* print register and ram memory */
        printVerbose();
    }

    if (guiReport == 1)
    {
        printGui();
    }
}

/* Init the simulation */
void initSimulation(void)
{
    memset(reg, 0, sizeof(reg));
    PC = 0;
}

/* Function that read the instruction and split it in different variable
  * to fill the instr structure that will ease instruction execution later */
void structBuilding(uint32_t instruction, struct command* instr)
{
    /* Some value are stored in two places,
     * it's a not efficient way to have the right name later */
    instr->opcode = instruction       & 0x7f;
    instr->rd     = instruction >> 7  & 0x1f;
    instr->imm5   = instruction >> 7  & 0x1f;
    instr->imm20  = instruction >> 12 & 0xfffff;
    instr->funct3 = instruction >> 12 & 0x7;
    instr->rs1    = instruction >> 15 & 0x1f;
    instr->imm12  = instruction >> 20 & 0xfff;
    instr->rs2    = instruction >> 20 & 0x1f;
    instr->funct7 = instruction >> 25 & 0x7f;
    instr->imm7   = instruction >> 25 & 0x7f;
    instr->instr  = instruction;
}

/* Function that open a bin file with instruction in it, t
 * he file can be checked in bash terminal with xxd.exe -g 4 -u main.bin */
void readInstrFile(const char* filepath)
{
    FILE *instrFile;
    /* Open the program file in binary read mode */
    instrFile = fopen(filepath, "rb");
    /* seek the end of the file to get the size */
    fseek(instrFile, 0L, SEEK_END);
    /* store file size in chunk of uint8_t in instrSize*/
    instrSize = ftell(instrFile);
    /* Allocate instr array base on the size of the file read */
    instrMem = malloc((instrSize));
    /* reset pointer address on file */
    rewind(instrFile);
    /* copy chunk of uint8_t data from file into instrMem[] */
    fread(instrMem, sizeof(uint8_t), instrSize, instrFile);
    /* close file */
    fclose(instrFile);
    
    memcpy(dataMem, instrMem, instrSize);
}

/* Function that init the VHDL test file */
void initVHDLFile(FILE **VHDLFile, const char* filepath)
{
    /* Open the vhd file in write mode to erase it */
    *VHDLFile = fopen(filepath, "w");

    char* text =
        "library IEEE;\n"
        "use IEEE.STD_LOGIC_1164.ALL;\n"
        "use IEEE.NUMERIC_STD.ALL;\n"
        "\n"
        "entity testbench is\n"
        "end testbench;\n"
        "\n"
        "architecture vhdl of testbench is\n"
        "\n"
        "\tcomponent Processor is\n"
        "\t\tPort (\n"
        "\t\t-- INPUTS\n"
        "\t\tPROCclock        : in std_logic;\n"
        "\t\tPROCreset        : in std_logic;\n"
        "\t\tPROCinstruction  : in std_logic_vector(31 downto 0);\n"
        "\t\tPROCoutputDM     : in std_logic_vector(31 downto 0);\n"
        "\t\t-- OUTPUTS\n"
        "\t\tPROCprogcounter  : out std_logic_vector(31 downto 0);\n"
        "\t\tPROCstore        : out std_logic;\n"
        "\t\tPROCload         : out std_logic;\n"
        "\t\tPROCfunct3       : out std_logic_vector(2 downto 0);\n"
        "\t\tPROCaddrDM       : out std_logic_vector(31 downto 0);\n"
        "\t\tPROCinputDM      : out std_logic_vector(31 downto 0);\n"
        "\t\t-- 32 registers of register file\n"
        "\t\tPROCreg00        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg01        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg02        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg03        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg04        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg05        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg06        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg07        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg08        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg09        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0A        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0B        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0C        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0D        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0E        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg0F        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg10        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg11        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg12        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg13        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg14        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg15        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg16        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg17        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg18        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg19        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1A        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1B        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1C        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1D        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1E        : inout std_logic_vector(31 downto 0);\n"
        "\t\tPROCreg1F        : inout std_logic_vector(31 downto 0)\n"
        "\t);\n"
        "\tend component;\n"
        "\n"
        "\tsignal ck, reset, load, store : STD_LOGIC;\n"
        "\tsignal dataLength : std_logic_vector(2 downto 0);\n"
        "\tsignal instr, progcounter, inputData, outputData, dataAddr: std_logic_vector(31 downto 0);\n"
        "\tsignal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);\n"
        "\n"
        "\tBEGIN\n"
        "\n"
        "\t--instanciation de l'entité PROC\n"
        "\tiProcessor : Processor port map (\n"
        "\t\tPROCclock        => ck,\n"
        "\t\tPROCreset        => reset,\n"
        "\t\tPROCinstruction  => instr,\n"
        "\t\tPROCoutputDM     => outputData,\n"
        "\t\tPROCprogcounter  => progcounter,\n"
        "\t\tPROCstore        => store,\n"
        "\t\tPROCload         => load,\n"
        "\t\tPROCfunct3       => dataLength,\n"
        "\t\tPROCaddrDM       => dataAddr,\n"
        "\t\tPROCinputDM      => inputData,\n"
        "\t\tPROCreg00        => reg00,\n"
        "\t\tPROCreg01        => reg01,\n"
        "\t\tPROCreg02        => reg02,\n"
        "\t\tPROCreg03        => reg03,\n"
        "\t\tPROCreg04        => reg04,\n"
        "\t\tPROCreg05        => reg05,\n"
        "\t\tPROCreg06        => reg06,\n"
        "\t\tPROCreg07        => reg07,\n"
        "\t\tPROCreg08        => reg08,\n"
        "\t\tPROCreg09        => reg09,\n"
        "\t\tPROCreg0A        => reg0A,\n"
        "\t\tPROCreg0B        => reg0B,\n"
        "\t\tPROCreg0C        => reg0C,\n"
        "\t\tPROCreg0D        => reg0D,\n"
        "\t\tPROCreg0E        => reg0E,\n"
        "\t\tPROCreg0F        => reg0F,\n"
        "\t\tPROCreg10        => reg10,\n"
        "\t\tPROCreg11        => reg11,\n"
        "\t\tPROCreg12        => reg12,\n"
        "\t\tPROCreg13        => reg13,\n"
        "\t\tPROCreg14        => reg14,\n"
        "\t\tPROCreg15        => reg15,\n"
        "\t\tPROCreg16        => reg16,\n"
        "\t\tPROCreg17        => reg17,\n"
        "\t\tPROCreg18        => reg18,\n"
        "\t\tPROCreg19        => reg19,\n"
        "\t\tPROCreg1A        => reg1A,\n"
        "\t\tPROCreg1B        => reg1B,\n"
        "\t\tPROCreg1C        => reg1C,\n"
        "\t\tPROCreg1D        => reg1D,\n"
        "\t\tPROCreg1E        => reg1E,\n"
        "\t\tPROCreg1F        => reg1F\n"
        "\t);\n"
        "\n"
        "\tVecteurTest : process\n"
        "\t\tbegin\n"
        "\t\t-- init  simulation\n"
        "\t\t\tck <= '0';\n"
        "\t\t\treset <= '1';\n"
        "\t\t\tinstr <= x\"00000000\"; -- init\n"
        "\t\t\twait for 10 ns;\n"
        "\t\t\tck <= '1';\n"
        "\t\t\twait for 5 ns;\n"
        "\t\t\treset <= '0';\n"
        "\t\t\twait for 5 ns;\n"
        "\t\t\tassert false report \"Index;Instruction;Description;Status;Note\" severity note;\n"
        "\n"
    ;
    /* write string sequence in VHDL file*/
    fprintf(*VHDLFile, "%s", text);
}

/* Function that close the VHDL test file*/
void endVHDLFile(FILE *VHDLFile)
{
    char* text =
        "\t\t\twait;\n"
        "\t\tend process;\n"
        "END vhdl;";
    /* write string sequence in VHDL file*/
    fprintf(VHDLFile, "%s", text);
    /* close file */
    fclose(VHDLFile);
}

/* Function that execute the right OPcode subroutine */
void executeInstr(struct command* instr, FILE *VHDLFile)
{
    switch(instr->opcode)
    {
        case OP_LUI:
            executeLUI(instr, VHDLFile);
        break;

        case OP_AUIPC:
            executeAUIPC(instr, VHDLFile);
        break;

        case OP_JAL:
            executeJAL(instr, VHDLFile);
        break;

        case OP_JALR:
            executeJALR(instr, VHDLFile);
        break;

        case OP_BRANCH:
            executeBRANCH(instr, VHDLFile);
        break;

        case OP_LOAD:
            executeLOAD(instr, VHDLFile);
        break;

        case OP_STORE:
            executeSTORE(instr, VHDLFile);
        break;

        case OP_OP_IMM:
            executeOP_IMM(instr, VHDLFile);
        break;

        case OP_OP:
            executeOP(instr, VHDLFile);
        break;

        case OP_MISC_MEM:
            executeMISC_MEM(instr, VHDLFile);
        break;

        case OP_SYSTEM:
            executeSYSTEM(instr, VHDLFile);
        break;
    }
}

/* Load upper immediate OPcode type subroutine */
void executeLUI(struct command* instr, FILE *VHDLFile)
{
    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");
    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LUI : reg[%.2d] = 0x%x << 12\n", instr->instr, instr->rd, instr->imm20);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LUI : reg[%.2d] = 0x%x << 12;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->imm20);
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

    printf("LUI\t: reg[%.2d] = 0x%x << 12\n", instr->rd, instr->imm20);
    reg[instr->rd] = instr->imm20 << 12;
    printf("%41s reg[%.2d] = 0x%x\n", "", instr->rd, reg[instr->rd]);
    PC = PC + 4;

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* add upper immediate to PC OPcode type subroutine */
void executeAUIPC(struct command* instr, FILE *VHDLFile)
{
    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");
    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- AUIPC : PC = PC + 0x%x << 12\n", instr->instr, instr->imm20);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;AUIPC : PC = PC + 0x%x << 12;OK; ;\" severity note;\n",instrCount, instr->instr, instr->imm20);
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

    printf("AUIPC\t: PC = PC + 0x%x << 12\n", instr->imm20);
    printf("%41s PC = 0x%x + 0x%x = ", "", PC, (instr->imm20 << 12));
    PC = PC + ( instr->imm20 << 12 );
    reg[instr->rd] = PC;
    printf("0x%x also reg[%.2d] = PC\n", PC, instr->rd);

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* Jump and link OPcode type subroutine */
void executeJAL(struct command* instr, FILE *VHDLFile)
{
    uint32_t offset = (( instr->imm20 >> 9  & 0x3ff) << 1  ) | // 10:0 0 forced to 0
                      (( instr->imm20 >> 8  & 0x01 ) << 11 ) | // 11
                      (( instr->imm20 >> 0  & 0xff ) << 12 ) | // 19:12
                      (( instr->imm20 >> 19 & 0x01 ) << 20 );  // 20 sign

    offset = (offset & 0x100000) ? offset| ~0x000fffff : offset & 0x000fffff;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- JAL : reg[%.2d] = PC+4 and PC = 0x%x + %d\n", instr->instr, instr->rd, PC, offset);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;JAL : reg[%.2d] = PC+4 and PC = 0x%x + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, PC, offset);
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

    printf("JAL\t: reg[%.2d] = PC+4 and PC = 0x%x + %d\n", instr->rd, PC, offset);
    reg[instr->rd] = PC + 4;
    printf("%41s reg[%.2d] = 0x%x and PC = ", "", instr->rd, reg[instr->rd]);
    PC = PC + offset;
    printf("0x%x\n", PC);

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* Indirect Jump and link OPcode type subroutine */
void executeJALR(struct command* instr, FILE *VHDLFile)
{
    int32_t offset = (instr->imm12 & 0x800) ? instr->imm12 | ~0x000007ff : instr->imm12 & 0x000007ff;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- JALR : reg[%.2d] = PC+4 and PC = (reg[%.2d] + %d) & ~1 \n", instr->instr, instr->rs1, offset);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;JALR : reg[%.2d] = PC+4 and PC = (reg[%.2d] + %d) & ~1;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

    printf("JALR\t: reg[%.2d] = PC+4 and PC = (reg[%.2d] + %d) & ~1 \n",instr->rd, instr->rs1, offset);
    reg[instr->rd] = PC + 4;
    printf("%41s reg[%.2d] = 0x%x and PC = ", "", instr->rd, reg[instr->rd]);
    PC = reg[instr->rs1] + offset;
    printf("0x%x\n", PC);

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* Branch OPcode type subroutine */
void executeBRANCH(struct command* instr, FILE *VHDLFile)
{
    int32_t offset =  (  instr->imm5 & 0x1e        )         | // 4:0 with 0 forced 0
                      (( instr->imm7 & 0x3f        ) << 5  ) | // 10:5
                      (( instr->imm5 & 0x01        ) << 11 ) | // 11
                      (((instr->imm7 & 0x40) >> 6  ) << 12 );  // 12 sign

    offset = (offset & 0x1000) ? offset | ~0x00000fff : offset & 0x00000fff;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    /* All branches work the same, if the condition is true jump to
     * PC + Immediate within 4Kib range */
    switch(instr->funct3)
    {
        case F3_BEQ: // Branch if equal
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BEQ : if ( reg[%.2d] == reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BEQ : if ( reg[%.2d] == reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BEQ\t: if ( reg[%.2d] == reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( 0x%x == 0x%x ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( reg[instr->rs1] == reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;

        case F3_BNE: // branch if not equal
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BNE : if ( reg[%.2d] != reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BNE : if ( reg[%.2d] != reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BNE\t: if ( reg[%.2d] != reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( 0x%x != 0x%x ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( reg[instr->rs1] != reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;

        case F3_BLT: // branch if lesser than with sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BLT : if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BLT : if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BLT\t: if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( %d < %d ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( (int32_t) reg[instr->rs1] < (int32_t) reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;

        case F3_BGE: // branch if greater than with sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BGE : if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BGE : if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BGE\t: if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( %d > %d ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( (int32_t) reg[instr->rs1] > (int32_t) reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;

        case F3_BLTU: // branch if lesser than without sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BLTU: if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BLTU: if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BLTU\t: if ( reg[%.2d] < reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( %d < %d ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( reg[instr->rs1] < reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;

        case F3_BGEU: // branch if greater than without sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- BGEU : if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d\n", instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;BGEU : if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rs1, instr->rs2, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("BGEU\t: if ( reg[%.2d] > reg[%.2d] ) PC = PC + %d \n", instr->rs1, instr->rs2, offset);
            printf("%41s if ( %d > %d ) PC = 0x%x + %d => ", "", reg[instr->rs1], reg[instr->rs2], PC, offset);
            if ( reg[instr->rs1] > reg[instr->rs2] ) PC = PC + offset;
            else PC = PC + 4;
            printf("PC = 0x%x\n", PC);
        break;
    }
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* LOAD OPcode type subroutine */
void executeLOAD(struct command* instr, FILE *VHDLFile)
{
    uint32_t data;
    uint32_t offset = (instr->imm12 & 0x800) ? instr->imm12 | ~0x000007ff : instr->imm12 & 0x000007ff;
    uint32_t addr = reg[instr->rs1] + offset;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    switch(instr->funct3)
    {
        case F3_LB: // Load int8_t
            printf("LDB\t: reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->rd, instr->rs1, offset);
            data = readDataMemory(addr, sizeof(uint8_t));
            reg[instr->rd] = (data & 0xff >> 7) ? data | ~0x000000ff : data & 0x000000ff;
            printf("%41s reg[%.2d] = dataMem[Ox%x] = 0x%x\n", "", instr->rd, addr, reg[instr->rd]);

            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LDB : reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\toutputData <= x\"%.8x\";\n",reg[instr->rd]);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LDB : reg[%.2d] = dataMem[reg[%.2d] + %d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"000\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert load = '1'                report \"load error at step %d\"        severity error;\n", instrCount*2);
        break;

        case F3_LH: // Load int16_t
            printf("LDH\t: reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->rd, instr->rs1, offset);
            data = readDataMemory(addr, sizeof(uint16_t));
            reg[instr->rd] = (data & 0xffff >> 15) ? data | ~0x0000ffff : data & 0x0000ffff;
            printf("%41s reg[%.2d] = dataMem[Ox%x] = 0x%x\n", "", instr->rd, addr, reg[instr->rd]);

            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LDH : reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\toutputData <= x\"%.8x\";\n",reg[instr->rd]);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LDH : reg[%.2d] = dataMem[reg[%.2d] + %d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"001\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert load = '1'                report \"load error at step %d\"        severity error;\n", instrCount*2);
        break;

        case F3_LW: // Load int32_t / uint32_t
            printf("LDW\t: reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->rd, instr->rs1, offset);
            reg[instr->rd] = readDataMemory(addr, sizeof(uint32_t));
            printf("%41s reg[%.2d] = dataMem[Ox%x] = 0x%x\n", "", instr->rd, addr, reg[instr->rd]);

            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LDW : reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\toutputData <= x\"%.8x\";\n",reg[instr->rd]);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LDW : reg[%.2d] = dataMem[reg[%.2d] + %d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"010\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert load = '1'                report \"load error at step %d\"        severity error;\n", instrCount*2);
        break;

        case F3_LBU: // Load uint8_t
            printf("LBU\t: reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->rd, instr->rs1, offset);
            reg[instr->rd] = readDataMemory(addr, sizeof(uint8_t));
            printf("%41s reg[%.2d] = dataMem[Ox%x] = 0x%x\n", "", instr->rd, addr, reg[instr->rd]);

            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LBU : reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\toutputData <= x\"%.8x\";\n",reg[instr->rd]);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LBU : reg[%.2d] = dataMem[reg[%.2d] + %d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"100\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert load = '1'                report \"load error at step %d\"        severity error;\n", instrCount*2);
        break;

        case F3_LHU: // Load uint16_t
            printf("LDHU\t: reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->rd, instr->rs1, offset);
            reg[instr->rd] = readDataMemory(addr, sizeof(uint16_t));
            printf("%41s reg[%.2d] = dataMem[Ox%x] = 0x%x\n", "", instr->rd, addr, reg[instr->rd]);

            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- LHU : reg[%.2d] = dataMem[reg[%.2d] + %d]\n", instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\toutputData <= x\"%.8x\";\n",reg[instr->rd]);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;LHU : reg[%.2d] = dataMem[reg[%.2d] + %d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, offset);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"101\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert load = '1'                report \"load error at step %d\"        severity error;\n", instrCount*2);
        break;
    }
    PC = PC + 4;

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* STORE OPcode type subroutine */
void executeSTORE(struct command* instr, FILE *VHDLFile)
{
    uint32_t offset = (instr->imm7 << 5) | (instr->imm5);
    offset = (offset & 0x800) ? offset | ~0x000007ff : offset & 0x000007ff;
    uint32_t addr = reg[instr->rs1] + offset;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    switch(instr->funct3)
    {
        case F3_SB: // Store a uint8_t
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- STRB : dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->instr, instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;STRB : dataMem[reg[%.2d] + %d] = reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr,  instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert inputData = x\"%.8x\"   report \"data error at step  %d\"       severity error;\n",reg[instr->rs2], instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"000\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert store = '1'               report \"store error at step %d\"       severity error;\n", instrCount*2);

            printf("STRB\t: dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->rs1, offset, instr->rs2);
            writeDataMemory(addr, reg[instr->rs2], sizeof(uint8_t));
            printf("%41s dataMem[Ox%x] = 0x%x\n", "", addr, reg[instr->rs2]);
        break;

        case F3_SH: // Store a uint16_t
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- STRH : dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->instr, instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;STRH : dataMem[reg[%.2d] + %d] = reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr,  instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert inputData = x\"%.8x\"   report \"data error at step  %d\"       severity error;\n",reg[instr->rs2], instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"001\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert store = '1'               report \"store error at step %d\"       severity error;\n", instrCount*2);

            printf("STRH\t: dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->rs1, offset, instr->rs2);
            writeDataMemory(addr, reg[instr->rs2], sizeof(uint16_t));
            printf("%41s dataMem[Ox%x] = 0x%x\n", "", addr, reg[instr->rs2]);
        break;

        case F3_SW: // Store a uint32_t
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- STRW : dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->instr, instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;STRW : dataMem[reg[%.2d] + %d] = reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr,  instr->rs1, offset, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataAddr = x\"%.8x\"    report \"address error at step %d\"     severity error;\n", addr, instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert inputData = x\"%.8x\"   report \"data error at step  %d\"       severity error;\n",reg[instr->rs2], instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert dataLength = \"010\"        report \"length error at step %d\"      severity error;\n", instrCount*2);
            fprintf(VHDLFile, "\t\t\tassert store = '1'               report \"store error at step %d\"       severity error;\n", instrCount*2);

            printf("STRW\t: dataMem[reg[%.2d] + %d] = reg[%.2d]\n", instr->rs1, offset, instr->rs2);
            writeDataMemory(addr, reg[instr->rs2], sizeof(uint32_t));
            printf("%41s dataMem[Ox%x] = 0x%x\n", "", addr, reg[instr->rs2]);
        break;
    }
    PC = PC + 4;

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* IMM OPcode type subroutine */
void executeOP_IMM(struct command* instr, FILE *VHDLFile)
{
    uint32_t immExt = (instr->imm12 & 0x800) ? instr->imm12 | ~0x000007ff : instr->imm12 & 0x000007ff;
    uint32_t shift = instr->imm12 & 0x1f;

    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");
    switch(instr->funct3)
    {
        case F3_ADDI: // ADD with Immediate
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- ADDI : reg[%.2d] = reg[%.2d] + %d\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;ADDI : reg[%.2d] = reg[%.2d] + %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("ADDI\t: reg[%.2d] = reg[%.2d] + %d\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = 0x%x + %d = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = reg[instr->rs1] + immExt;
            printf("0x%x\n", reg[instr->rd]);

        break;

        case F3_SLTI: // Logical compare with Immediate and sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLTI : reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLTI : reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLTI\t: reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = ( %d < %d ) ? = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = ((int32_t) reg[instr->rs1] < (int32_t) immExt ) ? 1 : 0;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_SLTIU: // Logical compare with Immediate and without sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLTI : reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLTIU : reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLTIU\t: reg[%.2d] = ( reg[%.2d] < %d ) ? 1 : 0\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = ( Ox%x < Ox%x ) ? = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = ( reg[instr->rs1] < immExt ) ? 1 : 0;
            printf("%d\n", reg[instr->rd]);
        break;

        case F3_XORI: // XOR with Immediate
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- XORI: reg[%.2d] = reg[%.2d] ^ %d\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;XORI: reg[%.2d] = reg[%.2d] ^ %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("XORI\t: reg[%.2d] = reg[%.2d] ^ %d\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = 0x%x ^ 0x%x = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = reg[instr->rs1] ^ immExt;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_ORI: // OR with Immediate
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- ORI: reg[%.2d] = reg[%.2d] | %d\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;ORI: reg[%.2d] = reg[%.2d] | %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("ORI\t: reg[%.2d] = reg[%.2d] | %d\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = 0x%x | 0x%x = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = reg[instr->rs1] | immExt;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_ANDI: // AND with Immediate
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- ANDI: reg[%.2d] = reg[%.2d] & %d\n", instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;ANDI: reg[%.2d] = reg[%.2d] & %d;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, immExt);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("ANDI\t: reg[%.2d] = reg[%.2d] & %d\n", instr->rd, instr->rs1, immExt);
            printf("%41s reg[%.2d] = 0x%x & 0x%x = ", "", instr->rd, reg[instr->rs1], immExt);
            reg[instr->rd] = reg[instr->rs1] & immExt;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_SLLI: // Immediate Left Logical Shift
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLLI : reg[%.2d] = reg[%.2d] << %d & 0x1f\n", instr->instr, instr->rd, instr->rs1, shift);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLLI : reg[%.2d] = reg[%.2d] << %d & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, shift);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLLI\t: reg[%.2d] = reg[%.2d] << %d\n", instr->rd, instr->rs1, shift);
            printf("%41s reg[%.2d] = 0x%x << %d = ", "", instr->rd, reg[instr->rs1], shift);
            reg[instr->rd] = reg[instr->rs1] << shift;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_SRLI:
        //case F3_SRAI: duplicate value
            switch(instr->funct7)
            {
                case F7_SRLI: // Immediate Right Logical Shift
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SRLI : reg[%.2d] = reg[%.2d] >> %d & 0x1f\n", instr->instr, instr->rd, instr->rs1, shift);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SRLI : reg[%.2d] = reg[%.2d] >> %d & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, shift);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("SRLI\t: reg[%.2d] = reg[%.2d] >> %d\n", instr->rd, instr->rs1, shift);
                    printf("%41s reg[%.2d] = 0x%x >> %d = ", "", instr->rd, reg[instr->rs1], shift);
                    reg[instr->rd] = reg[instr->rs1] >> shift;
                    printf("0x%x\n", reg[instr->rd]);
                break;
                case F7_SRAI: // Aritmethic Right Logical Shift
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SRAI : reg[%.2d] = (int) reg[%.2d] >> (int) %d & 0x1f\n", instr->instr, instr->rd, instr->rs1, shift);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SRAI : reg[%.2d] = (int) reg[%.2d] >> (int) %d & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, shift);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("SRLAI\t: reg[%.2d] = (int) reg[%.2d] >> (int) %d & 0x1f\n", instr->rd, instr->rs1, shift);
                    printf("%41s reg[%.2d] = 0x%x >> (int) %d = ", "", instr->rd, reg[instr->rs1], shift);
                    reg[instr->rd] = (int32_t) reg[instr->rs1] >> (int32_t) shift;
                    printf("0x%x\n", reg[instr->rd]);
                break;
            }
        break;
    }
    PC = PC + 4;

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

/* OP OPcode type subroutine */
void executeOP(struct command* instr, FILE *VHDLFile)
{
    fprintf(VHDLFile, "\t\t-- load instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '0';\n");

    uint32_t shift = reg[instr->rs2] & 0x1f;

    switch(instr->funct3)
    {
        case F3_ADD:
        // case F3_SUB: duplicate value
            switch(instr->funct7)
            {
                case F7_ADD: // ADD
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- ADD: reg[%.2d] = reg[%.2d] + reg[%.2d]\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;ADD: reg[%.2d] = reg[%.2d] + reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("ADD\t: reg[%.2d] = reg[%.2d] + reg[%.2d] \n", instr->rd, instr->rs1, instr->rs2);
                    printf("%41s reg[%.2d] = 0x%x + 0x%x = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
                    reg[instr->rd] = reg[instr->rs1] + reg[instr->rs2];
                    printf("0x%x\n", reg[instr->rd]);
                break;

                case F7_SUB: // SUB
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SUB: reg[%.2d] = reg[%.2d] - reg[%.2d]\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SUB: reg[%.2d] = reg[%.2d] - reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("SUB\t: reg[%.2d] = reg[%.2d] - reg[%.2d] \n", instr->rd, instr->rs1, instr->rs2);
                    printf("%41s reg[%.2d] = 0x%x - 0x%x = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
                    reg[instr->rd] = reg[instr->rs1] - reg[instr->rs2];
                    printf("0x%x\n", reg[instr->rd]);
                break;
            }
        break;

        case F3_SLL: // Left Logical Shift
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLL : reg[%.2d] = reg[%.2d] << reg[%.2d] & 0x1f\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLL : reg[%.2d] = reg[%.2d] << reg[%.2d] & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLL\t: reg[%.2d] = reg[%.2d] << reg[%.2d] & 0x1f\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = 0x%x << 0x%x = ", "", instr->rd, reg[instr->rs1], shift);
            reg[instr->rd] = reg[instr->rs1] << shift;
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_SLT: // Logical compare with Immediate and sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLT : reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLT : reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLT\t: reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = ( %d < %d ) ? = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
            reg[instr->rd] = ((int32_t) reg[instr->rs1] < (int32_t) reg[instr->rs2] ) ? 1 : 0;
            printf("%d\n", reg[instr->rd]);
        break;

        case F3_SLTU: // Logical compare with Immediate and withouth sign consideration
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SLTU: reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SLTU: reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("SLTU\t: reg[%.2d] = ( reg[%.2d] < reg[%.2d] ) ? 1 : 0\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = ( 0x%x < 0x%x ) ? = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
            reg[instr->rd] = (reg[instr->rs1] < reg[instr->rs2] ) ? 1 : 0;
            printf("%d\n", reg[instr->rd]);
        break;

        case F3_XOR: // XOR
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- XOR: reg[%.2d] = reg[%.2d] ^ reg[%.2d]\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;XOR: reg[%.2d] = reg[%.2d] ^ reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("XOR\t: reg[%.2d] = reg[%.2d] ^ reg[%.2d]\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = 0x%x ^ 0x%x = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
            reg[instr->rd] = reg[instr->rs1] ^ reg[instr->rs2];
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_SRL:
        // case F3_SRA: duplicate value
            switch(instr->funct7)
            {
                case F7_SRL: //Right Logical Shift
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SRL : reg[%.2d] = reg[%.2d] >> reg[%.2d] & 0x1f\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SRL : reg[%.2d] = reg[%.2d] >> reg[%.2d] & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("SRL\t: reg[%.2d] = reg[%.2d] >> reg[%.2d] & 0x1f\n", instr->rd, instr->rs1, instr->rs2);
                    printf("%41s reg[%.2d] = 0x%x >> 0x%x = ", "", instr->rd, reg[instr->rs1], shift);
                    reg[instr->rd] = reg[instr->rs1] >> shift;
                    printf("0x%x\n", reg[instr->rd]);
                break;

                case F7_SRA: // Right arithmetic Shift
                    fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- SRA: reg[%.2d] = (int) reg[%.2d] >> (int) reg[%.2d] & 0x1f\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
                    fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;SRA: reg[%.2d] = (int) reg[%.2d] >> (int) reg[%.2d] & 0x1f;OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
                    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

                    printf("SRA\t: reg[%.2d] = (int) reg[%.2d] >> (int) reg[%.2d] & 0x1f\n", instr->rd, instr->rs1, instr->rs2);
                    printf("%41s reg[%.2d] = 0x%x >> 0x%x = ", "", instr->rd, reg[instr->rs1], shift);
                    reg[instr->rd] = (int32_t) reg[instr->rs1] >> shift;
                    printf("0x%x\n", reg[instr->rd]);
                break;
            }
        break;

        case F3_OR: // OR
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- OR: reg[%.2d] = reg[%.2d] | reg[%.2d]\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;OR: reg[%.2d] = reg[%.2d] | reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("OR\t: reg[%.2d] = reg[%.2d] | reg[%.2d]\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = 0x%x | 0x%x = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
            reg[instr->rd] = reg[instr->rs1] | reg[instr->rs2];
            printf("0x%x\n", reg[instr->rd]);
        break;

        case F3_AND:// AND
            fprintf(VHDLFile, "\t\t\tinstr <= x\"%.8x\"; -- AND: reg[%.2d] = reg[%.2d] & reg[%.2d]\n", instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
            fprintf(VHDLFile, "\t\t\tassert false report \"%d;0x%.8x;AND: reg[%.2d] = reg[%.2d] & reg[%.2d];OK; ;\" severity note;\n",instrCount, instr->instr, instr->rd, instr->rs1, instr->rs2);
            fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2);

            printf("AND\t: reg[%.2d] = reg[%.2d] & reg[%.2d]\n", instr->rd, instr->rs1, instr->rs2);
            printf("%41s reg[%.2d] = 0x%x & 0x%x = ", "", instr->rd, reg[instr->rs1], reg[instr->rs2]);
            reg[instr->rd] = reg[instr->rs1] & reg[instr->rs2];
            printf("0x%x\n", reg[instr->rd]);
        break;
    }
    PC = PC + 4;

    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
    fprintf(VHDLFile, "\t\t-- execute instruction %d\n", instrCount);
    fprintf(VHDLFile, "\t\t\tck <= '1';\n");
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    if (regsOn == 1)
    {
        assertRegs(instrCount*2+1, VHDLFile);
    }
    fprintf(VHDLFile, "\t\t\tassert progcounter = x\"%.8x\" report \"progcounter error at step %d\" severity error;\n", PC, instrCount*2+1);
    fprintf(VHDLFile, "\t\t\twait for 5 ns;\n");
    fprintf(VHDLFile, "\n");
}

void executeMISC_MEM(struct command* instr, FILE *VHDLFile){}
void executeSYSTEM(struct command* instr, FILE *VHDLFile){}

/* Debug function if needed */
void printInstr(struct command* instr)
{
    printf("\nOpCode:\t0x%x\n", instr->opcode);
    printf("rd:\t0x%x\n", instr->rd);
    printf("funct3:\t0x%x\n", instr->funct3);
    printf("rs1:\t0x%x\n", instr->rs1);
    printf("rs2:\t0x%x\n", instr->rs2);
    printf("funct7:\t0x%x\n", instr->funct7);
}

void printGui(void)
{
    printf("Instruction memory\n");
    for(uint32_t i = 0; i < instrSize ; i+=4)
    {
        printf("%.8x:%.8x\n",i, readProgramMemory(i));
    }
    printf("Registers\n");
    printf("reg[00] zero :%.8x\n", reg[0]);
    printf("reg[01] ra   :%.8x\n", reg[1]);
    printf("reg[02] sp   :%.8x\n", reg[2]);
    printf("reg[03] gp   :%.8x\n", reg[3]);
    printf("reg[04] tp   :%.8x\n", reg[4]);
    printf("reg[05] t0   :%.8x\n", reg[5]);
    printf("reg[06] t1   :%.8x\n", reg[6]);
    printf("reg[07] t2   :%.8x\n", reg[7]);
    printf("reg[08] s0/fp:%.8x\n", reg[8]);
    printf("reg[09] s1   :%.8x\n", reg[9]);
    printf("reg[10] a0   :%.8x\n", reg[10]);
    printf("reg[11] a1   :%.8x\n", reg[11]);
    printf("reg[12] a2   :%.8x\n", reg[12]);
    printf("reg[13] a3   :%.8x\n", reg[13]);
    printf("reg[14] a4   :%.8x\n", reg[14]);
    printf("reg[15] a5   :%.8x\n", reg[15]);
    printf("reg[16] a6   :%.8x\n", reg[16]);
    printf("reg[17] a7   :%.8x\n", reg[17]);
    printf("reg[18] s2   :%.8x\n", reg[18]);
    printf("reg[19] s3   :%.8x\n", reg[19]);
    printf("reg[20] s4   :%.8x\n", reg[20]);
    printf("reg[21] s5   :%.8x\n", reg[21]);
    printf("reg[22] s6   :%.8x\n", reg[22]);
    printf("reg[23] s7   :%.8x\n", reg[23]);
    printf("reg[24] s8   :%.8x\n", reg[24]);
    printf("reg[25] s9   :%.8x\n", reg[25]);
    printf("reg[26] s10  :%.8x\n", reg[26]);
    printf("reg[27] s11  :%.8x\n", reg[27]);
    printf("reg[28] t3   :%.8x\n", reg[28]);
    printf("reg[29] t4   :%.8x\n", reg[29]);
    printf("reg[30] t5   :%.8x\n", reg[30]);
    printf("reg[31] t6   :%.8x\n", reg[31]);

    printf("Data Memory\n");
    for (uint32_t addr = 0 ; addr < sizeof(dataMem); addr+=0x8)
    {
        printf("%.3x:", addr);
        for (uint8_t i=0 ; i < 2*sizeof(uint32_t) ; i+= sizeof(uint32_t))
        {
            printf("%.8x ", readDataMemory(addr+i, sizeof(uint32_t)));
        }
        printf("\n");
    }

}

void printVerbose(void)
{
    // Register    ABI Name    Description                       Saver
    // x0          zero        Hard-wired zero                   —
    // x1          ra          Return address                    Caller
    // x2          sp          Stack pointer                     Callee
    // x3          gp          Global pointer                    —
    // x4          tp          Thread pointer                    —
    // x5          t0          Temporary/alternate link register Caller
    // x6–7        t1–2        Temporaries                       Caller
    // x8          s0/fp       Saved register/frame pointer      Callee
    // x9          s1          Saved register                    Callee
    // x10–11      a0–1        Function arguments/return values  Caller
    // x12–17      a2–7        Function arguments                Caller
    // x18–27      s2–11       Saved registers                   Callee
    // x28–31      t3–6        Temporaries                       Caller
    // f0–7        ft0–7       FP temporaries                    Caller
    // f8–9        fs0–1       FP saved registers                Callee
    // f10–11      fa0–1       FP arguments/return values        Caller
    // f12–17      fa2–7       FP arguments                      Caller
    // f18–27      fs2–11      FP saved registers                Callee
    // f28–31      ft8–11      FP temporaries                    Caller

    uint16_t regNb = 16;
    uint8_t regEq[32][12]=
    {
        {"|   zero   \0"}, {"|   ra     \0"}, {"|   sp     \0"}, {"|   gp     \0"},
        {"|   tp     \0"}, {"|   t0     \0"}, {"|   t1     \0"}, {"|   t2     \0"},
        {"|   s0/fp  \0"}, {"|   s1     \0"}, {"|   a0     \0"}, {"|   a1     \0"},
        {"|   a2     \0"}, {"|   a3     \0"}, {"|   a4     \0"}, {"|   a5     \0"},
        {"|   a6     \0"}, {"|   a7     \0"}, {"|   s2     \0"}, {"|   s3     \0"},
        {"|   s4     \0"}, {"|   s5     \0"}, {"|   s6     \0"}, {"|   s7     \0"},
        {"|   s8     \0"}, {"|   s9     \0"}, {"|   s10    \0"}, {"|   s11    \0"},
        {"|   t3     \0"}, {"|   t4     \0"}, {"|   t5     \0"}, {"|   t6     \0"}
    };

    // uint8_t filePointerEq[32][11]=
    // {
    //     {"|   ft0    "}, {"|   ft1    "}, {"|   ft2    "}, {"|   ft3    "},
    //     {"|   ft4    "}, {"|   ft5    "}, {"|   ft6    "}, {"|   ft7    "},
    //     {"|   fs0    "}, {"|   fs1    "}, {"|   fa0    "}, {"|   fa1    "},
    //     {"|   fa2    "}, {"|   fa3    "}, {"|   fa4    "}, {"|   fa5    "},
    //     {"|   fa6    "}, {"|   fa7    "}, {"|   fs2    "}, {"|   fs3    "},
    //     {"|   fs4    "}, {"|   fs5    "}, {"|   fs6    "}, {"|   fs7    "},
    //     {"|   fs8    "}, {"|   fs9    "}, {"|   fs10   "}, {"|   fs11   "},
    //     {"|   ft8    "}, {"|   ft9    "}, {"|   ft10   "}, {"|   ft11   "}
    // };

    printf("\n");
    for (uint8_t i = 0 ; i < regNb ; i++)
    {
        printf("|   REG%.2d  ", i);
    }
    printf("\n");

    for (uint8_t i = 0 ; i < regNb ; i++)
    {
        printf("%s", regEq[i]);
    }
    printf("\n");

    for (uint8_t i = 0 ; i < regNb ; i++)
    {
        printf("|----------");
    }
    printf("\n");


    for (uint8_t j = 0 ; j < regNb ; j++)
    {
        printf("|%*x", 10, reg[j]);
    }
    printf("\n");


    printf("\nData Memory :\n");
    for (uint32_t addr = 0 ; addr < sizeof(dataMem); addr+=0x8)
    {
        printf("0x%.8x: ", addr);
        for (uint8_t i=0 ; i < 2*sizeof(uint32_t) ; i+= sizeof(uint32_t))
        {
            printf("%.8x ", readDataMemory(addr+i, sizeof(uint32_t)));
        }
        printf("\n");
    }
}
