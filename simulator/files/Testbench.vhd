library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testbench is
	
end testbench;

architecture vhdl of testbench is

	component Top is
		Port (
			-- INPUTS
			TOPclock		: in std_logic;
			TOPreset		: in std_logic;
			TOPresetIM	: in std_logic;
			TOPinstr		: in std_logic_vector(31 downto 0);
			-- INOUTS
			-- 32 registers of register file
			TOPprogcounter	: inout std_logic_vector(31 downto 0);
			
			TOPreg00	: inout std_logic_vector(31 downto 0);
			TOPreg01	: inout std_logic_vector(31 downto 0);
			TOPreg02	: inout std_logic_vector(31 downto 0);
			TOPreg03	: inout std_logic_vector(31 downto 0);
			TOPreg04	: inout std_logic_vector(31 downto 0);
			TOPreg05	: inout std_logic_vector(31 downto 0);
			TOPreg06	: inout std_logic_vector(31 downto 0);
			TOPreg07	: inout std_logic_vector(31 downto 0);
			TOPreg08	: inout std_logic_vector(31 downto 0);
			TOPreg09	: inout std_logic_vector(31 downto 0);
			TOPreg0A	: inout std_logic_vector(31 downto 0);
			TOPreg0B	: inout std_logic_vector(31 downto 0);
			TOPreg0C	: inout std_logic_vector(31 downto 0);
			TOPreg0D	: inout std_logic_vector(31 downto 0);
			TOPreg0E	: inout std_logic_vector(31 downto 0);
			TOPreg0F	: inout std_logic_vector(31 downto 0);
			TOPreg10	: inout std_logic_vector(31 downto 0);
			TOPreg11	: inout std_logic_vector(31 downto 0);
			TOPreg12	: inout std_logic_vector(31 downto 0);
			TOPreg13	: inout std_logic_vector(31 downto 0);
			TOPreg14	: inout std_logic_vector(31 downto 0);
			TOPreg15	: inout std_logic_vector(31 downto 0);
			TOPreg16	: inout std_logic_vector(31 downto 0);
			TOPreg17	: inout std_logic_vector(31 downto 0);
			TOPreg18	: inout std_logic_vector(31 downto 0);
			TOPreg19	: inout std_logic_vector(31 downto 0);
			TOPreg1A	: inout std_logic_vector(31 downto 0);
			TOPreg1B	: inout std_logic_vector(31 downto 0);
			TOPreg1C	: inout std_logic_vector(31 downto 0);
			TOPreg1D	: inout std_logic_vector(31 downto 0);
			TOPreg1E	: inout std_logic_vector(31 downto 0);
			TOPreg1F	: inout std_logic_vector(31 downto 0)
		);
	end component;
		
	signal ck, reset, resetIM : STD_LOGIC;
	signal instr, progcounter: std_logic_vector(31 downto 0);	
	signal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);
	
	BEGIN

	--instanciation de l'entité Top
	itop : Top port map (
		TOPclock => ck,
		TOPreset => reset,
		TOPresetIM => resetIM,
		TOPprogcounter => progcounter,
		TOPinstr => instr,
		TOPreg00	=> reg00,
		TOPreg01	=> reg01,
		TOPreg02	=> reg02,
		TOPreg03	=> reg03,
		TOPreg04	=> reg04,
		TOPreg05	=> reg05,
		TOPreg06	=> reg06,
		TOPreg07	=> reg07,
		TOPreg08	=> reg08,
		TOPreg09	=> reg09,
		TOPreg0A	=> reg0A,
		TOPreg0B	=> reg0B,
		TOPreg0C	=> reg0C,
		TOPreg0D	=> reg0D,
		TOPreg0E	=> reg0E,
		TOPreg0F	=> reg0F,
		TOPreg10	=> reg10,
		TOPreg11	=> reg11,
		TOPreg12	=> reg12,
		TOPreg13	=> reg13,
		TOPreg14	=> reg14,
		TOPreg15	=> reg15,
		TOPreg16	=> reg16,
		TOPreg17	=> reg17,
		TOPreg18	=> reg18,
		TOPreg19	=> reg19,
		TOPreg1A	=> reg1A,
		TOPreg1B	=> reg1B,
		TOPreg1C	=> reg1C,
		TOPreg1D	=> reg1D,
		TOPreg1E	=> reg1E,
		TOPreg1F	=> reg1F
	);	
	
	VecteurTest : process
		begin
		
		-- init  simulation
			ck <= '0';
			reset <= '1';
			instr <= x"00000000"; -- init
			wait for 10 ns;
			ck <= '1';
			wait for 5 ns;
			reset <= '0';
			resetIM <= '0';	
			wait for 5 ns;
			assert false report "Index;Instruction;Description;Status;Note" severity note;

		-- load instruction 0
			ck <= '0';
			instr <= x"fe010113"; -- ADDI : reg[02] = reg[02] + -32
			wait for 5 ns;
			assert progcounter = x"00000000" report "progcounter error at step 0" severity error
			assert false report "0;0xfe010113;ADDI : reg[02] = reg[02] + -32;OK;"
			wait for 5 ns;

		-- execute instruction 0
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000004" report "progcounter error at step 1" severity error
			wait for 5 ns;

		-- load instruction 1
			ck <= '0';
			instr <= x"00112e23"; -- STRW : dataMem[reg[02] + 28] = reg[01]
			wait for 5 ns;
			assert progcounter = x"00000004" report "progcounter error at step 2" severity error
			assert dataAddr = x"0000007c"    report "address error at step 2"     severity error
			assert inputData = x"00000000"   report "data error at step  2"       severity error
			assert dataLength = "010"        report "length error at step 2"      severity error
			assert store = '1'               report "store error at step 2"       severity error
			assert false report "1;0x00112e23;STRW : dataMem[reg[02] + 28] = reg[01];OK;"
			wait for 5 ns;

		-- execute instruction 1
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000008" report "progcounter error at step 3" severity error
			wait for 5 ns;

		-- load instruction 2
			ck <= '0';
			instr <= x"00812c23"; -- STRW : dataMem[reg[02] + 24] = reg[08]
			wait for 5 ns;
			assert progcounter = x"00000008" report "progcounter error at step 4" severity error
			assert dataAddr = x"00000078"    report "address error at step 4"     severity error
			assert inputData = x"000000a0"   report "data error at step  4"       severity error
			assert dataLength = "010"        report "length error at step 4"      severity error
			assert store = '1'               report "store error at step 4"       severity error
			assert false report "2;0x00812c23;STRW : dataMem[reg[02] + 24] = reg[08];OK;"
			wait for 5 ns;

		-- execute instruction 2
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000000c" report "progcounter error at step 5" severity error
			wait for 5 ns;

		-- load instruction 3
			ck <= '0';
			instr <= x"02010413"; -- ADDI : reg[08] = reg[02] + 32
			wait for 5 ns;
			assert progcounter = x"0000000c" report "progcounter error at step 6" severity error
			assert false report "3;0x02010413;ADDI : reg[08] = reg[02] + 32;OK;"
			wait for 5 ns;

		-- execute instruction 3
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000010" report "progcounter error at step 7" severity error
			wait for 5 ns;

		-- load instruction 4
			ck <= '0';
			instr <= x"00200793"; -- ADDI : reg[15] = reg[00] + 2
			wait for 5 ns;
			assert progcounter = x"00000010" report "progcounter error at step 8" severity error
			assert false report "4;0x00200793;ADDI : reg[15] = reg[00] + 2;OK;"
			wait for 5 ns;

		-- execute instruction 4
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000014" report "progcounter error at step 9" severity error
			wait for 5 ns;

		-- load instruction 5
			ck <= '0';
			instr <= x"fef42623"; -- STRW : dataMem[reg[08] + -20] = reg[15]
			wait for 5 ns;
			assert progcounter = x"00000014" report "progcounter error at step 10" severity error
			assert dataAddr = x"0000006c"    report "address error at step 10"     severity error
			assert inputData = x"00000002"   report "data error at step  10"       severity error
			assert dataLength = "010"        report "length error at step 10"      severity error
			assert store = '1'               report "store error at step 10"       severity error
			assert false report "5;0xfef42623;STRW : dataMem[reg[08] + -20] = reg[15];OK;"
			wait for 5 ns;

		-- execute instruction 5
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000018" report "progcounter error at step 11" severity error
			wait for 5 ns;

		-- load instruction 6
			ck <= '0';
			instr <= x"018000ef"; -- JAL	: reg[01] = PC+4 and PC = 0x18 + 24
			wait for 5 ns;
			assert progcounter = x"00000018" report "progcounter error at step 12" severity error
			assert false report "6;0x018000ef;JAL	: reg[01] = PC+4 and PC = 0x18 + 24
;OK;"
			wait for 5 ns;

		-- execute instruction 6
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000030" report "progcounter error at step 13" severity error
			wait for 5 ns;

		-- load instruction 7
			ck <= '0';
			instr <= x"fe010113"; -- ADDI : reg[02] = reg[02] + -32
			wait for 5 ns;
			assert progcounter = x"00000030" report "progcounter error at step 14" severity error
			assert false report "7;0xfe010113;ADDI : reg[02] = reg[02] + -32;OK;"
			wait for 5 ns;

		-- execute instruction 7
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000034" report "progcounter error at step 15" severity error
			wait for 5 ns;

		-- load instruction 8
			ck <= '0';
			instr <= x"00812e23"; -- STRW : dataMem[reg[02] + 28] = reg[08]
			wait for 5 ns;
			assert progcounter = x"00000034" report "progcounter error at step 16" severity error
			assert dataAddr = x"0000005c"    report "address error at step 16"     severity error
			assert inputData = x"00000080"   report "data error at step  16"       severity error
			assert dataLength = "010"        report "length error at step 16"      severity error
			assert store = '1'               report "store error at step 16"       severity error
			assert false report "8;0x00812e23;STRW : dataMem[reg[02] + 28] = reg[08];OK;"
			wait for 5 ns;

		-- execute instruction 8
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000038" report "progcounter error at step 17" severity error
			wait for 5 ns;

		-- load instruction 9
			ck <= '0';
			instr <= x"02010413"; -- ADDI : reg[08] = reg[02] + 32
			wait for 5 ns;
			assert progcounter = x"00000038" report "progcounter error at step 18" severity error
			assert false report "9;0x02010413;ADDI : reg[08] = reg[02] + 32;OK;"
			wait for 5 ns;

		-- execute instruction 9
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000003c" report "progcounter error at step 19" severity error
			wait for 5 ns;

		-- load instruction 10
			ck <= '0';
			instr <= x"00300793"; -- ADDI : reg[15] = reg[00] + 3
			wait for 5 ns;
			assert progcounter = x"0000003c" report "progcounter error at step 20" severity error
			assert false report "10;0x00300793;ADDI : reg[15] = reg[00] + 3;OK;"
			wait for 5 ns;

		-- execute instruction 10
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000040" report "progcounter error at step 21" severity error
			wait for 5 ns;

		-- load instruction 11
			ck <= '0';
			instr <= x"fef42623"; -- STRW : dataMem[reg[08] + -20] = reg[15]
			wait for 5 ns;
			assert progcounter = x"00000040" report "progcounter error at step 22" severity error
			assert dataAddr = x"0000004c"    report "address error at step 22"     severity error
			assert inputData = x"00000003"   report "data error at step  22"       severity error
			assert dataLength = "010"        report "length error at step 22"      severity error
			assert store = '1'               report "store error at step 22"       severity error
			assert false report "11;0xfef42623;STRW : dataMem[reg[08] + -20] = reg[15];OK;"
			wait for 5 ns;

		-- execute instruction 11
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000044" report "progcounter error at step 23" severity error
			wait for 5 ns;

		-- load instruction 12
			ck <= '0';
			instr <= x"00000013"; -- ADDI : reg[00] = reg[00] + 0
			wait for 5 ns;
			assert progcounter = x"00000044" report "progcounter error at step 24" severity error
			assert false report "12;0x00000013;ADDI : reg[00] = reg[00] + 0;OK;"
			wait for 5 ns;

		-- execute instruction 12
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000048" report "progcounter error at step 25" severity error
			wait for 5 ns;

		-- load instruction 13
			ck <= '0';
			instr <= x"01c12403"; -- LDW : reg[08] = dataMem[reg[02] + 28]
			outputData = x"00000080";
			wait for 5 ns;
			assert progcounter = x"00000048" report "progcounter error at step 26" severity error
			assert dataAddr = x"0000005c"    report "address error at step 26"     severity error
			assert dataLength = "010"        report "length error at step 26"      severity error
			assert load = '1'                report "store error at step 26"       severity error
			assert false report "13;0x00000008;LDW : reg[02] = dataMem[reg[28] + 6388456];OK;"
			wait for 5 ns;

		-- execute instruction 13
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000004c" report "progcounter error at step 27" severity error
			wait for 5 ns;

		-- load instruction 14
			ck <= '0';
			instr <= x"02010113"; -- ADDI : reg[02] = reg[02] + 32
			wait for 5 ns;
			assert progcounter = x"0000004c" report "progcounter error at step 28" severity error
			assert false report "14;0x02010113;ADDI : reg[02] = reg[02] + 32;OK;"
			wait for 5 ns;

		-- execute instruction 14
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000050" report "progcounter error at step 29" severity error
			wait for 5 ns;

		-- load instruction 15
			ck <= '0';
			instr <= x"00008067"; -- JALR	: reg[01] = PC+4 and PC = (reg[00] + 17) & ~1 
			wait for 5 ns;
			assert progcounter = x"00000050" report "progcounter error at step 30" severity error
			assert false report "15;0x00008067;JALR	: reg[01] = PC+4 and PC = (reg[00] + 0) & ~1;OK;"
			wait for 5 ns;

		-- execute instruction 15
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000001c" report "progcounter error at step 31" severity error
			wait for 5 ns;

		-- load instruction 16
			ck <= '0';
			instr <= x"00000013"; -- ADDI : reg[00] = reg[00] + 0
			wait for 5 ns;
			assert progcounter = x"0000001c" report "progcounter error at step 32" severity error
			assert false report "16;0x00000013;ADDI : reg[00] = reg[00] + 0;OK;"
			wait for 5 ns;

		-- execute instruction 16
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000020" report "progcounter error at step 33" severity error
			wait for 5 ns;

		-- load instruction 17
			ck <= '0';
			instr <= x"01c12083"; -- LDW : reg[01] = dataMem[reg[02] + 28]
			outputData = x"00000000";
			wait for 5 ns;
			assert progcounter = x"00000020" report "progcounter error at step 34" severity error
			assert dataAddr = x"0000007c"    report "address error at step 34"     severity error
			assert dataLength = "010"        report "length error at step 34"      severity error
			assert load = '1'                report "store error at step 34"       severity error
			assert false report "17;0x00000001;LDW : reg[02] = dataMem[reg[28] + 6388456];OK;"
			wait for 5 ns;

		-- execute instruction 17
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000024" report "progcounter error at step 35" severity error
			wait for 5 ns;

		-- load instruction 18
			ck <= '0';
			instr <= x"01812403"; -- LDW : reg[08] = dataMem[reg[02] + 24]
			outputData = x"000000a0";
			wait for 5 ns;
			assert progcounter = x"00000024" report "progcounter error at step 36" severity error
			assert dataAddr = x"00000078"    report "address error at step 36"     severity error
			assert dataLength = "010"        report "length error at step 36"      severity error
			assert load = '1'                report "store error at step 36"       severity error
			assert false report "18;0x00000008;LDW : reg[02] = dataMem[reg[24] + 6388456];OK;"
			wait for 5 ns;

		-- execute instruction 18
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000028" report "progcounter error at step 37" severity error
			wait for 5 ns;

		-- load instruction 19
			ck <= '0';
			instr <= x"02010113"; -- ADDI : reg[02] = reg[02] + 32
			wait for 5 ns;
			assert progcounter = x"00000028" report "progcounter error at step 38" severity error
			assert false report "19;0x02010113;ADDI : reg[02] = reg[02] + 32;OK;"
			wait for 5 ns;

		-- execute instruction 19
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000002c" report "progcounter error at step 39" severity error
			wait for 5 ns;

		-- load instruction 20
			ck <= '0';
			instr <= x"00008067"; -- JALR	: reg[01] = PC+4 and PC = (reg[00] + 17) & ~1 
			wait for 5 ns;
			assert progcounter = x"0000002c" report "progcounter error at step 40" severity error
			assert false report "20;0x00008067;JALR	: reg[01] = PC+4 and PC = (reg[00] + 0) & ~1;OK;"
			wait for 5 ns;

		-- execute instruction 20
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"00000000" report "progcounter error at step 41" severity error
			wait for 5 ns;

			wait:
		end process;
END vhdl;