library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testbench is
end testbench;

architecture vhdl of testbench is

	component Processor is
		Port (
		-- INPUTS
		PROCclock        : in std_logic;
		PROCreset        : in std_logic;
		PROCinstruction  : in std_logic_vector(31 downto 0);
		PROCoutputDM     : in std_logic_vector(31 downto 0);
		-- OUTPUTS
		PROCprogcounter  : out std_logic_vector(31 downto 0);
		PROCstore        : out std_logic;
		PROCload         : out std_logic;
		PROCfunct3       : out std_logic_vector(2 downto 0);
		PROCaddrDM       : out std_logic_vector(31 downto 0);
		PROCinputDM      : out std_logic_vector(31 downto 0);
		-- 32 registers of register file
		PROCreg00        : inout std_logic_vector(31 downto 0);
		PROCreg01        : inout std_logic_vector(31 downto 0);
		PROCreg02        : inout std_logic_vector(31 downto 0);
		PROCreg03        : inout std_logic_vector(31 downto 0);
		PROCreg04        : inout std_logic_vector(31 downto 0);
		PROCreg05        : inout std_logic_vector(31 downto 0);
		PROCreg06        : inout std_logic_vector(31 downto 0);
		PROCreg07        : inout std_logic_vector(31 downto 0);
		PROCreg08        : inout std_logic_vector(31 downto 0);
		PROCreg09        : inout std_logic_vector(31 downto 0);
		PROCreg0A        : inout std_logic_vector(31 downto 0);
		PROCreg0B        : inout std_logic_vector(31 downto 0);
		PROCreg0C        : inout std_logic_vector(31 downto 0);
		PROCreg0D        : inout std_logic_vector(31 downto 0);
		PROCreg0E        : inout std_logic_vector(31 downto 0);
		PROCreg0F        : inout std_logic_vector(31 downto 0);
		PROCreg10        : inout std_logic_vector(31 downto 0);
		PROCreg11        : inout std_logic_vector(31 downto 0);
		PROCreg12        : inout std_logic_vector(31 downto 0);
		PROCreg13        : inout std_logic_vector(31 downto 0);
		PROCreg14        : inout std_logic_vector(31 downto 0);
		PROCreg15        : inout std_logic_vector(31 downto 0);
		PROCreg16        : inout std_logic_vector(31 downto 0);
		PROCreg17        : inout std_logic_vector(31 downto 0);
		PROCreg18        : inout std_logic_vector(31 downto 0);
		PROCreg19        : inout std_logic_vector(31 downto 0);
		PROCreg1A        : inout std_logic_vector(31 downto 0);
		PROCreg1B        : inout std_logic_vector(31 downto 0);
		PROCreg1C        : inout std_logic_vector(31 downto 0);
		PROCreg1D        : inout std_logic_vector(31 downto 0);
		PROCreg1E        : inout std_logic_vector(31 downto 0);
		PROCreg1F        : inout std_logic_vector(31 downto 0)
	);
	end component;

	signal ck, reset, load, store : STD_LOGIC;
	signal dataLength : std_logic_vector(2 downto 0);
	signal instr, progcounter, inputData, outputData, dataAddr: std_logic_vector(31 downto 0);
	signal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);

	BEGIN

	--instanciation de l'entité PROC
	iProcessor : Processor port map (
		PROCclock        => ck,
		PROCreset        => reset,
		PROCinstruction  => instr,
		PROCoutputDM     => outputData,
		PROCprogcounter  => progcounter,
		PROCstore        => store,
		PROCload         => load,
		PROCfunct3       => dataLength,
		PROCaddrDM       => dataAddr,
		PROCinputDM      => inputData,
		PROCreg00        => reg00,
		PROCreg01        => reg01,
		PROCreg02        => reg02,
		PROCreg03        => reg03,
		PROCreg04        => reg04,
		PROCreg05        => reg05,
		PROCreg06        => reg06,
		PROCreg07        => reg07,
		PROCreg08        => reg08,
		PROCreg09        => reg09,
		PROCreg0A        => reg0A,
		PROCreg0B        => reg0B,
		PROCreg0C        => reg0C,
		PROCreg0D        => reg0D,
		PROCreg0E        => reg0E,
		PROCreg0F        => reg0F,
		PROCreg10        => reg10,
		PROCreg11        => reg11,
		PROCreg12        => reg12,
		PROCreg13        => reg13,
		PROCreg14        => reg14,
		PROCreg15        => reg15,
		PROCreg16        => reg16,
		PROCreg17        => reg17,
		PROCreg18        => reg18,
		PROCreg19        => reg19,
		PROCreg1A        => reg1A,
		PROCreg1B        => reg1B,
		PROCreg1C        => reg1C,
		PROCreg1D        => reg1D,
		PROCreg1E        => reg1E,
		PROCreg1F        => reg1F
	);

	VecteurTest : process
		begin
		-- init  simulation
			ck <= '0';
			reset <= '1';
			instr <= x"00000000"; -- init
			wait for 10 ns;
			ck <= '1';
			wait for 5 ns;
			reset <= '0';
			wait for 5 ns;
			assert false report "Index;Instruction;Description;Status;Note" severity note;

		-- load instruction 0
			ck <= '0';
			instr <= x"00001137"; -- LUI : reg[02] = 0x1 << 12
			wait for 5 ns;
			assert false report "0;0x00001137;LUI : reg[02] = 0x1 << 12;OK; ;" severity note;
			assert progcounter = x"00000000" report "progcounter error at step 0" severity error;
			wait for 5 ns;

		-- execute instruction 0
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 1" severity error;
			assert reg01 = x"00000000" report "reg01 error at step 1" severity error;
			assert reg02 = x"00001000" report "reg02 error at step 1" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 1" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 1" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 1" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 1" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 1" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 1" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 1" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 1" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 1" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 1" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 1" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 1" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 1" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 1" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 1" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 1" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 1" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 1" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 1" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 1" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 1" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 1" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 1" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 1" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 1" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 1" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 1" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 1" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 1" severity error;
			assert progcounter = x"00000004" report "progcounter error at step 1" severity error;
			wait for 5 ns;

		-- load instruction 1
			ck <= '0';
			instr <= x"00c000ef"; -- JAL : reg[01] = PC+4 and PC = 0x4 + 12
			wait for 5 ns;
			assert false report "1;0x00c000ef;JAL : reg[01] = PC+4 and PC = 0x4 + 12;OK; ;" severity note;
			assert progcounter = x"00000004" report "progcounter error at step 2" severity error;
			wait for 5 ns;

		-- execute instruction 1
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 3" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 3" severity error;
			assert reg02 = x"00001000" report "reg02 error at step 3" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 3" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 3" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 3" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 3" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 3" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 3" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 3" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 3" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 3" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 3" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 3" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 3" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 3" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 3" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 3" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 3" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 3" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 3" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 3" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 3" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 3" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 3" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 3" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 3" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 3" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 3" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 3" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 3" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 3" severity error;
			assert progcounter = x"00000010" report "progcounter error at step 3" severity error;
			wait for 5 ns;

		-- load instruction 2
			ck <= '0';
			instr <= x"ff010113"; -- ADDI : reg[02] = reg[02] + -16
			wait for 5 ns;
			assert false report "2;0xff010113;ADDI : reg[02] = reg[02] + -16;OK; ;" severity note;
			assert progcounter = x"00000010" report "progcounter error at step 4" severity error;
			wait for 5 ns;

		-- execute instruction 2
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 5" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 5" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 5" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 5" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 5" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 5" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 5" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 5" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 5" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 5" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 5" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 5" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 5" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 5" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 5" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 5" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 5" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 5" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 5" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 5" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 5" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 5" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 5" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 5" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 5" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 5" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 5" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 5" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 5" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 5" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 5" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 5" severity error;
			assert progcounter = x"00000014" report "progcounter error at step 5" severity error;
			wait for 5 ns;

		-- load instruction 3
			ck <= '0';
			instr <= x"00200793"; -- ADDI : reg[15] = reg[00] + 2
			wait for 5 ns;
			assert false report "3;0x00200793;ADDI : reg[15] = reg[00] + 2;OK; ;" severity note;
			assert progcounter = x"00000014" report "progcounter error at step 6" severity error;
			wait for 5 ns;

		-- execute instruction 3
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 7" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 7" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 7" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 7" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 7" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 7" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 7" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 7" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 7" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 7" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 7" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 7" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 7" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 7" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 7" severity error;
			assert reg0f = x"00000002" report "reg0f error at step 7" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 7" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 7" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 7" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 7" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 7" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 7" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 7" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 7" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 7" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 7" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 7" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 7" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 7" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 7" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 7" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 7" severity error;
			assert progcounter = x"00000018" report "progcounter error at step 7" severity error;
			wait for 5 ns;

		-- load instruction 4
			ck <= '0';
			instr <= x"00f12223"; -- STRW : dataMem[reg[02] + 4] = reg[15]
			wait for 5 ns;
			assert false report "4;0x00f12223;STRW : dataMem[reg[02] + 4] = reg[15];OK; ;" severity note;
			assert progcounter = x"00000018" report "progcounter error at step 8" severity error;
			assert dataAddr = x"00000ff4"    report "address error at step 8"     severity error;
			assert inputData = x"00000002"   report "data error at step  8"       severity error;
			assert dataLength = "010"        report "length error at step 8"      severity error;
			assert store = '1'               report "store error at step 8"       severity error;
			wait for 5 ns;

		-- execute instruction 4
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 9" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 9" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 9" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 9" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 9" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 9" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 9" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 9" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 9" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 9" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 9" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 9" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 9" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 9" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 9" severity error;
			assert reg0f = x"00000002" report "reg0f error at step 9" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 9" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 9" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 9" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 9" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 9" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 9" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 9" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 9" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 9" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 9" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 9" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 9" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 9" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 9" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 9" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 9" severity error;
			assert progcounter = x"0000001c" report "progcounter error at step 9" severity error;
			wait for 5 ns;

		-- load instruction 5
			ck <= '0';
			instr <= x"00300793"; -- ADDI : reg[15] = reg[00] + 3
			wait for 5 ns;
			assert false report "5;0x00300793;ADDI : reg[15] = reg[00] + 3;OK; ;" severity note;
			assert progcounter = x"0000001c" report "progcounter error at step 10" severity error;
			wait for 5 ns;

		-- execute instruction 5
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 11" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 11" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 11" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 11" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 11" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 11" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 11" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 11" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 11" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 11" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 11" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 11" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 11" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 11" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 11" severity error;
			assert reg0f = x"00000003" report "reg0f error at step 11" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 11" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 11" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 11" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 11" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 11" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 11" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 11" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 11" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 11" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 11" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 11" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 11" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 11" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 11" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 11" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 11" severity error;
			assert progcounter = x"00000020" report "progcounter error at step 11" severity error;
			wait for 5 ns;

		-- load instruction 6
			ck <= '0';
			instr <= x"00f12423"; -- STRW : dataMem[reg[02] + 8] = reg[15]
			wait for 5 ns;
			assert false report "6;0x00f12423;STRW : dataMem[reg[02] + 8] = reg[15];OK; ;" severity note;
			assert progcounter = x"00000020" report "progcounter error at step 12" severity error;
			assert dataAddr = x"00000ff8"    report "address error at step 12"     severity error;
			assert inputData = x"00000003"   report "data error at step  12"       severity error;
			assert dataLength = "010"        report "length error at step 12"      severity error;
			assert store = '1'               report "store error at step 12"       severity error;
			wait for 5 ns;

		-- execute instruction 6
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 13" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 13" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 13" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 13" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 13" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 13" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 13" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 13" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 13" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 13" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 13" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 13" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 13" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 13" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 13" severity error;
			assert reg0f = x"00000003" report "reg0f error at step 13" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 13" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 13" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 13" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 13" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 13" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 13" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 13" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 13" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 13" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 13" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 13" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 13" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 13" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 13" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 13" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 13" severity error;
			assert progcounter = x"00000024" report "progcounter error at step 13" severity error;
			wait for 5 ns;

		-- load instruction 7
			ck <= '0';
			instr <= x"00412783"; -- LDW : reg[15] = dataMem[reg[02] + 4]
			outputData <= x"00000002";
			wait for 5 ns;
			assert false report "7;0x00412783;LDW : reg[15] = dataMem[reg[02] + 4];OK; ;" severity note;
			assert progcounter = x"00000024" report "progcounter error at step 14" severity error;
			assert dataAddr = x"00000ff4"    report "address error at step 14"     severity error;
			assert dataLength = "010"        report "length error at step 14"      severity error;
			assert load = '1'                report "load error at step 14"        severity error;
			wait for 5 ns;

		-- execute instruction 7
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 15" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 15" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 15" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 15" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 15" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 15" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 15" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 15" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 15" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 15" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 15" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 15" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 15" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 15" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 15" severity error;
			assert reg0f = x"00000002" report "reg0f error at step 15" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 15" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 15" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 15" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 15" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 15" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 15" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 15" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 15" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 15" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 15" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 15" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 15" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 15" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 15" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 15" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 15" severity error;
			assert progcounter = x"00000028" report "progcounter error at step 15" severity error;
			wait for 5 ns;

		-- load instruction 8
			ck <= '0';
			instr <= x"00812703"; -- LDW : reg[14] = dataMem[reg[02] + 8]
			outputData <= x"00000003";
			wait for 5 ns;
			assert false report "8;0x00812703;LDW : reg[14] = dataMem[reg[02] + 8];OK; ;" severity note;
			assert progcounter = x"00000028" report "progcounter error at step 16" severity error;
			assert dataAddr = x"00000ff8"    report "address error at step 16"     severity error;
			assert dataLength = "010"        report "length error at step 16"      severity error;
			assert load = '1'                report "load error at step 16"        severity error;
			wait for 5 ns;

		-- execute instruction 8
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 17" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 17" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 17" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 17" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 17" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 17" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 17" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 17" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 17" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 17" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 17" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 17" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 17" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 17" severity error;
			assert reg0e = x"00000003" report "reg0e error at step 17" severity error;
			assert reg0f = x"00000002" report "reg0f error at step 17" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 17" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 17" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 17" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 17" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 17" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 17" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 17" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 17" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 17" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 17" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 17" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 17" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 17" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 17" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 17" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 17" severity error;
			assert progcounter = x"0000002c" report "progcounter error at step 17" severity error;
			wait for 5 ns;

		-- load instruction 9
			ck <= '0';
			instr <= x"00e787b3"; -- ADD: reg[15] = reg[15] + reg[14]
			wait for 5 ns;
			assert false report "9;0x00e787b3;ADD: reg[15] = reg[15] + reg[14];OK; ;" severity note;
			assert progcounter = x"0000002c" report "progcounter error at step 18" severity error;
			wait for 5 ns;

		-- execute instruction 9
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 19" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 19" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 19" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 19" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 19" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 19" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 19" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 19" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 19" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 19" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 19" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 19" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 19" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 19" severity error;
			assert reg0e = x"00000003" report "reg0e error at step 19" severity error;
			assert reg0f = x"00000005" report "reg0f error at step 19" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 19" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 19" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 19" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 19" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 19" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 19" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 19" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 19" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 19" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 19" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 19" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 19" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 19" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 19" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 19" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 19" severity error;
			assert progcounter = x"00000030" report "progcounter error at step 19" severity error;
			wait for 5 ns;

		-- load instruction 10
			ck <= '0';
			instr <= x"00f12623"; -- STRW : dataMem[reg[02] + 12] = reg[15]
			wait for 5 ns;
			assert false report "10;0x00f12623;STRW : dataMem[reg[02] + 12] = reg[15];OK; ;" severity note;
			assert progcounter = x"00000030" report "progcounter error at step 20" severity error;
			assert dataAddr = x"00000ffc"    report "address error at step 20"     severity error;
			assert inputData = x"00000005"   report "data error at step  20"       severity error;
			assert dataLength = "010"        report "length error at step 20"      severity error;
			assert store = '1'               report "store error at step 20"       severity error;
			wait for 5 ns;

		-- execute instruction 10
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 21" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 21" severity error;
			assert reg02 = x"00000ff0" report "reg02 error at step 21" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 21" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 21" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 21" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 21" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 21" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 21" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 21" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 21" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 21" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 21" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 21" severity error;
			assert reg0e = x"00000003" report "reg0e error at step 21" severity error;
			assert reg0f = x"00000005" report "reg0f error at step 21" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 21" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 21" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 21" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 21" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 21" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 21" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 21" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 21" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 21" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 21" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 21" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 21" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 21" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 21" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 21" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 21" severity error;
			assert progcounter = x"00000034" report "progcounter error at step 21" severity error;
			wait for 5 ns;

		-- load instruction 11
			ck <= '0';
			instr <= x"01010113"; -- ADDI : reg[02] = reg[02] + 16
			wait for 5 ns;
			assert false report "11;0x01010113;ADDI : reg[02] = reg[02] + 16;OK; ;" severity note;
			assert progcounter = x"00000034" report "progcounter error at step 22" severity error;
			wait for 5 ns;

		-- execute instruction 11
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 23" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 23" severity error;
			assert reg02 = x"00001000" report "reg02 error at step 23" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 23" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 23" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 23" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 23" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 23" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 23" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 23" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 23" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 23" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 23" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 23" severity error;
			assert reg0e = x"00000003" report "reg0e error at step 23" severity error;
			assert reg0f = x"00000005" report "reg0f error at step 23" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 23" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 23" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 23" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 23" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 23" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 23" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 23" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 23" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 23" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 23" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 23" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 23" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 23" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 23" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 23" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 23" severity error;
			assert progcounter = x"00000038" report "progcounter error at step 23" severity error;
			wait for 5 ns;

		-- load instruction 12
			ck <= '0';
			instr <= x"00008067"; -- JALR : reg[01] = PC+4 and PC = (reg[00] + 17) & ~1 
			wait for 5 ns;
			assert false report "12;0x00008067;JALR : reg[01] = PC+4 and PC = (reg[00] + 0) & ~1;OK; ;" severity note;
			assert progcounter = x"00000038" report "progcounter error at step 24" severity error;
			wait for 5 ns;

		-- execute instruction 12
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 25" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 25" severity error;
			assert reg02 = x"00001000" report "reg02 error at step 25" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 25" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 25" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 25" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 25" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 25" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 25" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 25" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 25" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 25" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 25" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 25" severity error;
			assert reg0e = x"00000003" report "reg0e error at step 25" severity error;
			assert reg0f = x"00000005" report "reg0f error at step 25" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 25" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 25" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 25" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 25" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 25" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 25" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 25" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 25" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 25" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 25" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 25" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 25" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 25" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 25" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 25" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 25" severity error;
			assert progcounter = x"00000008" report "progcounter error at step 25" severity error;
			wait for 5 ns;

			wait;
		end process;
END vhdl;