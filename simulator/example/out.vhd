library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testbench is
end testbench;

architecture vhdl of testbench is

	component Processor is
		Port (
		-- INPUTS
		PROCclock        : in std_logic;
		PROCreset        : in std_logic;
		PROCinstruction  : in std_logic_vector(31 downto 0);
		PROCoutputDM     : in std_logic_vector(31 downto 0);
		-- OUTPUTS
		PROCprogcounter  : out std_logic_vector(31 downto 0);
		PROCstore        : out std_logic;
		PROCload         : out std_logic;
		PROCfunct3       : out std_logic_vector(2 downto 0);
		PROCaddrDM       : out std_logic_vector(31 downto 0);
		PROCinputDM      : out std_logic_vector(31 downto 0);
		-- 32 registers of register file
		PROCreg00        : inout std_logic_vector(31 downto 0);
		PROCreg01        : inout std_logic_vector(31 downto 0);
		PROCreg02        : inout std_logic_vector(31 downto 0);
		PROCreg03        : inout std_logic_vector(31 downto 0);
		PROCreg04        : inout std_logic_vector(31 downto 0);
		PROCreg05        : inout std_logic_vector(31 downto 0);
		PROCreg06        : inout std_logic_vector(31 downto 0);
		PROCreg07        : inout std_logic_vector(31 downto 0);
		PROCreg08        : inout std_logic_vector(31 downto 0);
		PROCreg09        : inout std_logic_vector(31 downto 0);
		PROCreg0A        : inout std_logic_vector(31 downto 0);
		PROCreg0B        : inout std_logic_vector(31 downto 0);
		PROCreg0C        : inout std_logic_vector(31 downto 0);
		PROCreg0D        : inout std_logic_vector(31 downto 0);
		PROCreg0E        : inout std_logic_vector(31 downto 0);
		PROCreg0F        : inout std_logic_vector(31 downto 0);
		PROCreg10        : inout std_logic_vector(31 downto 0);
		PROCreg11        : inout std_logic_vector(31 downto 0);
		PROCreg12        : inout std_logic_vector(31 downto 0);
		PROCreg13        : inout std_logic_vector(31 downto 0);
		PROCreg14        : inout std_logic_vector(31 downto 0);
		PROCreg15        : inout std_logic_vector(31 downto 0);
		PROCreg16        : inout std_logic_vector(31 downto 0);
		PROCreg17        : inout std_logic_vector(31 downto 0);
		PROCreg18        : inout std_logic_vector(31 downto 0);
		PROCreg19        : inout std_logic_vector(31 downto 0);
		PROCreg1A        : inout std_logic_vector(31 downto 0);
		PROCreg1B        : inout std_logic_vector(31 downto 0);
		PROCreg1C        : inout std_logic_vector(31 downto 0);
		PROCreg1D        : inout std_logic_vector(31 downto 0);
		PROCreg1E        : inout std_logic_vector(31 downto 0);
		PROCreg1F        : inout std_logic_vector(31 downto 0)
	);
	end component;

	signal ck, reset, load, store : STD_LOGIC;
	signal dataLength : std_logic_vector(2 downto 0);
	signal instr, progcounter, inputData, outputData, dataAddr: std_logic_vector(31 downto 0);
	signal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);

	BEGIN

	--instanciation de l'entité PROC
	iProcessor : Processor port map (
		PROCclock        => ck,
		PROCreset        => reset,
		PROCinstruction  => instr,
		PROCoutputDM     => outputData,
		PROCprogcounter  => progcounter,
		PROCstore        => store,
		PROCload         => load,
		PROCfunct3       => dataLength,
		PROCaddrDM       => dataAddr,
		PROCinputDM      => inputData,
		PROCreg00        => reg00,
		PROCreg01        => reg01,
		PROCreg02        => reg02,
		PROCreg03        => reg03,
		PROCreg04        => reg04,
		PROCreg05        => reg05,
		PROCreg06        => reg06,
		PROCreg07        => reg07,
		PROCreg08        => reg08,
		PROCreg09        => reg09,
		PROCreg0A        => reg0A,
		PROCreg0B        => reg0B,
		PROCreg0C        => reg0C,
		PROCreg0D        => reg0D,
		PROCreg0E        => reg0E,
		PROCreg0F        => reg0F,
		PROCreg10        => reg10,
		PROCreg11        => reg11,
		PROCreg12        => reg12,
		PROCreg13        => reg13,
		PROCreg14        => reg14,
		PROCreg15        => reg15,
		PROCreg16        => reg16,
		PROCreg17        => reg17,
		PROCreg18        => reg18,
		PROCreg19        => reg19,
		PROCreg1A        => reg1A,
		PROCreg1B        => reg1B,
		PROCreg1C        => reg1C,
		PROCreg1D        => reg1D,
		PROCreg1E        => reg1E,
		PROCreg1F        => reg1F
	);

	VecteurTest : process
		begin
		-- init  simulation
			ck <= '0';
			reset <= '1';
			instr <= x"00000000"; -- init
			wait for 10 ns;
			ck <= '1';
			wait for 5 ns;
			reset <= '0';
			wait for 5 ns;
			assert false report "Index;Instruction;Description;Status;Note" severity note;

		-- load instruction 0
			ck <= '0';
			instr <= x"10000113"; -- ADDI : reg[02] = reg[00] + 256
			wait for 5 ns;
			assert false report "0;0x10000113;ADDI : reg[02] = reg[00] + 256;OK; ;" severity note;
			assert progcounter = x"00000000" report "progcounter error at step 0" severity error;
			wait for 5 ns;

		-- execute instruction 0
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 1" severity error;
			assert reg01 = x"00000000" report "reg01 error at step 1" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 1" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 1" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 1" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 1" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 1" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 1" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 1" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 1" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 1" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 1" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 1" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 1" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 1" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 1" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 1" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 1" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 1" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 1" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 1" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 1" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 1" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 1" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 1" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 1" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 1" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 1" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 1" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 1" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 1" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 1" severity error;
			assert progcounter = x"00000004" report "progcounter error at step 1" severity error;
			wait for 5 ns;

		-- load instruction 1
			ck <= '0';
			instr <= x"00c000ef"; -- JAL : reg[01] = PC+4 and PC = 0x4 + 12
			wait for 5 ns;
			assert false report "1;0x00c000ef;JAL : reg[01] = PC+4 and PC = 0x4 + 12;OK; ;" severity note;
			assert progcounter = x"00000004" report "progcounter error at step 2" severity error;
			wait for 5 ns;

		-- execute instruction 1
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 3" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 3" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 3" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 3" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 3" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 3" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 3" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 3" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 3" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 3" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 3" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 3" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 3" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 3" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 3" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 3" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 3" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 3" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 3" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 3" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 3" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 3" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 3" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 3" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 3" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 3" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 3" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 3" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 3" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 3" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 3" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 3" severity error;
			assert progcounter = x"00000010" report "progcounter error at step 3" severity error;
			wait for 5 ns;

		-- load instruction 2
			ck <= '0';
			instr <= x"800007b7"; -- LUI : reg[15] = 0x80000 << 12
			wait for 5 ns;
			assert false report "2;0x800007b7;LUI : reg[15] = 0x80000 << 12;OK; ;" severity note;
			assert progcounter = x"00000010" report "progcounter error at step 4" severity error;
			wait for 5 ns;

		-- execute instruction 2
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 5" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 5" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 5" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 5" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 5" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 5" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 5" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 5" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 5" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 5" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 5" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 5" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 5" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 5" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 5" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 5" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 5" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 5" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 5" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 5" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 5" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 5" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 5" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 5" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 5" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 5" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 5" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 5" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 5" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 5" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 5" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 5" severity error;
			assert progcounter = x"00000014" report "progcounter error at step 5" severity error;
			wait for 5 ns;

		-- load instruction 3
			ck <= '0';
			instr <= x"fff00713"; -- ADDI : reg[14] = reg[00] + -1
			wait for 5 ns;
			assert false report "3;0xfff00713;ADDI : reg[14] = reg[00] + -1;OK; ;" severity note;
			assert progcounter = x"00000014" report "progcounter error at step 6" severity error;
			wait for 5 ns;

		-- execute instruction 3
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 7" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 7" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 7" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 7" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 7" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 7" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 7" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 7" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 7" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 7" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 7" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 7" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 7" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 7" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 7" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 7" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 7" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 7" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 7" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 7" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 7" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 7" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 7" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 7" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 7" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 7" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 7" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 7" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 7" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 7" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 7" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 7" severity error;
			assert progcounter = x"00000018" report "progcounter error at step 7" severity error;
			wait for 5 ns;

		-- load instruction 4
			ck <= '0';
			instr <= x"00e7a223"; -- STRW : dataMem[reg[15] + 4] = reg[14]
			wait for 5 ns;
			assert false report "4;0x00e7a223;STRW : dataMem[reg[15] + 4] = reg[14];OK; ;" severity note;
			assert progcounter = x"00000018" report "progcounter error at step 8" severity error;
			assert dataAddr = x"80000004"    report "address error at step 8"     severity error;
			assert inputData = x"ffffffff"   report "data error at step  8"       severity error;
			assert dataLength = "010"        report "length error at step 8"      severity error;
			assert store = '1'               report "store error at step 8"       severity error;
			wait for 5 ns;

		-- execute instruction 4
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 9" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 9" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 9" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 9" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 9" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 9" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 9" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 9" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 9" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 9" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 9" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 9" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 9" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 9" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 9" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 9" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 9" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 9" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 9" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 9" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 9" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 9" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 9" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 9" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 9" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 9" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 9" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 9" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 9" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 9" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 9" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 9" severity error;
			assert progcounter = x"0000001c" report "progcounter error at step 9" severity error;
			wait for 5 ns;

		-- load instruction 5
			ck <= '0';
			instr <= x"00e7a423"; -- STRW : dataMem[reg[15] + 8] = reg[14]
			wait for 5 ns;
			assert false report "5;0x00e7a423;STRW : dataMem[reg[15] + 8] = reg[14];OK; ;" severity note;
			assert progcounter = x"0000001c" report "progcounter error at step 10" severity error;
			assert dataAddr = x"80000008"    report "address error at step 10"     severity error;
			assert inputData = x"ffffffff"   report "data error at step  10"       severity error;
			assert dataLength = "010"        report "length error at step 10"      severity error;
			assert store = '1'               report "store error at step 10"       severity error;
			wait for 5 ns;

		-- execute instruction 5
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 11" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 11" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 11" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 11" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 11" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 11" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 11" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 11" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 11" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 11" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 11" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 11" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 11" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 11" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 11" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 11" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 11" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 11" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 11" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 11" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 11" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 11" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 11" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 11" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 11" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 11" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 11" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 11" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 11" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 11" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 11" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 11" severity error;
			assert progcounter = x"00000020" report "progcounter error at step 11" severity error;
			wait for 5 ns;

		-- load instruction 6
			ck <= '0';
			instr <= x"017d88b7"; -- LUI : reg[17] = 0x17d8 << 12
			wait for 5 ns;
			assert false report "6;0x017d88b7;LUI : reg[17] = 0x17d8 << 12;OK; ;" severity note;
			assert progcounter = x"00000020" report "progcounter error at step 12" severity error;
			wait for 5 ns;

		-- execute instruction 6
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 13" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 13" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 13" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 13" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 13" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 13" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 13" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 13" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 13" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 13" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 13" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 13" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 13" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 13" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 13" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 13" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 13" severity error;
			assert reg11 = x"017d8000" report "reg11 error at step 13" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 13" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 13" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 13" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 13" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 13" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 13" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 13" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 13" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 13" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 13" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 13" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 13" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 13" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 13" severity error;
			assert progcounter = x"00000024" report "progcounter error at step 13" severity error;
			wait for 5 ns;

		-- load instruction 7
			ck <= '0';
			instr <= x"00010837"; -- LUI : reg[16] = 0x10 << 12
			wait for 5 ns;
			assert false report "7;0x00010837;LUI : reg[16] = 0x10 << 12;OK; ;" severity note;
			assert progcounter = x"00000024" report "progcounter error at step 14" severity error;
			wait for 5 ns;

		-- execute instruction 7
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 15" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 15" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 15" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 15" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 15" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 15" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 15" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 15" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 15" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 15" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 15" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 15" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 15" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 15" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 15" severity error;
			assert reg0f = x"80000000" report "reg0f error at step 15" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 15" severity error;
			assert reg11 = x"017d8000" report "reg11 error at step 15" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 15" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 15" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 15" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 15" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 15" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 15" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 15" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 15" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 15" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 15" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 15" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 15" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 15" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 15" severity error;
			assert progcounter = x"00000028" report "progcounter error at step 15" severity error;
			wait for 5 ns;

		-- load instruction 8
			ck <= '0';
			instr <= x"0e800793"; -- ADDI : reg[15] = reg[00] + 232
			wait for 5 ns;
			assert false report "8;0x0e800793;ADDI : reg[15] = reg[00] + 232;OK; ;" severity note;
			assert progcounter = x"00000028" report "progcounter error at step 16" severity error;
			wait for 5 ns;

		-- execute instruction 8
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 17" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 17" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 17" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 17" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 17" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 17" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 17" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 17" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 17" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 17" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 17" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 17" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 17" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 17" severity error;
			assert reg0e = x"ffffffff" report "reg0e error at step 17" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 17" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 17" severity error;
			assert reg11 = x"017d8000" report "reg11 error at step 17" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 17" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 17" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 17" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 17" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 17" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 17" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 17" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 17" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 17" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 17" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 17" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 17" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 17" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 17" severity error;
			assert progcounter = x"0000002c" report "progcounter error at step 17" severity error;
			wait for 5 ns;

		-- load instruction 9
			ck <= '0';
			instr <= x"ffe00713"; -- ADDI : reg[14] = reg[00] + -2
			wait for 5 ns;
			assert false report "9;0xffe00713;ADDI : reg[14] = reg[00] + -2;OK; ;" severity note;
			assert progcounter = x"0000002c" report "progcounter error at step 18" severity error;
			wait for 5 ns;

		-- execute instruction 9
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 19" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 19" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 19" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 19" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 19" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 19" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 19" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 19" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 19" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 19" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 19" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 19" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 19" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 19" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 19" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 19" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 19" severity error;
			assert reg11 = x"017d8000" report "reg11 error at step 19" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 19" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 19" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 19" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 19" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 19" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 19" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 19" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 19" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 19" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 19" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 19" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 19" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 19" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 19" severity error;
			assert progcounter = x"00000030" report "progcounter error at step 19" severity error;
			wait for 5 ns;

		-- load instruction 10
			ck <= '0';
			instr <= x"80000537"; -- LUI : reg[10] = 0x80000 << 12
			wait for 5 ns;
			assert false report "10;0x80000537;LUI : reg[10] = 0x80000 << 12;OK; ;" severity note;
			assert progcounter = x"00000030" report "progcounter error at step 20" severity error;
			wait for 5 ns;

		-- execute instruction 10
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 21" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 21" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 21" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 21" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 21" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 21" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 21" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 21" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 21" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 21" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 21" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 21" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 21" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 21" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 21" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 21" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 21" severity error;
			assert reg11 = x"017d8000" report "reg11 error at step 21" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 21" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 21" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 21" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 21" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 21" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 21" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 21" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 21" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 21" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 21" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 21" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 21" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 21" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 21" severity error;
			assert progcounter = x"00000034" report "progcounter error at step 21" severity error;
			wait for 5 ns;

		-- load instruction 11
			ck <= '0';
			instr <= x"83f88893"; -- ADDI : reg[17] = reg[17] + -1985
			wait for 5 ns;
			assert false report "11;0x83f88893;ADDI : reg[17] = reg[17] + -1985;OK; ;" severity note;
			assert progcounter = x"00000034" report "progcounter error at step 22" severity error;
			wait for 5 ns;

		-- execute instruction 11
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 23" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 23" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 23" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 23" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 23" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 23" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 23" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 23" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 23" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 23" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 23" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 23" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 23" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 23" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 23" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 23" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 23" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 23" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 23" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 23" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 23" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 23" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 23" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 23" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 23" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 23" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 23" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 23" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 23" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 23" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 23" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 23" severity error;
			assert progcounter = x"00000038" report "progcounter error at step 23" severity error;
			wait for 5 ns;

		-- load instruction 12
			ck <= '0';
			instr <= x"00600593"; -- ADDI : reg[11] = reg[00] + 6
			wait for 5 ns;
			assert false report "12;0x00600593;ADDI : reg[11] = reg[00] + 6;OK; ;" severity note;
			assert progcounter = x"00000038" report "progcounter error at step 24" severity error;
			wait for 5 ns;

		-- execute instruction 12
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 25" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 25" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 25" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 25" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 25" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 25" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 25" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 25" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 25" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 25" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 25" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 25" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 25" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 25" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 25" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 25" severity error;
			assert reg10 = x"00010000" report "reg10 error at step 25" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 25" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 25" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 25" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 25" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 25" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 25" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 25" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 25" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 25" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 25" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 25" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 25" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 25" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 25" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 25" severity error;
			assert progcounter = x"0000003c" report "progcounter error at step 25" severity error;
			wait for 5 ns;

		-- load instruction 13
			ck <= '0';
			instr <= x"f0080813"; -- ADDI : reg[16] = reg[16] + -256
			wait for 5 ns;
			assert false report "13;0xf0080813;ADDI : reg[16] = reg[16] + -256;OK; ;" severity note;
			assert progcounter = x"0000003c" report "progcounter error at step 26" severity error;
			wait for 5 ns;

		-- execute instruction 13
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 27" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 27" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 27" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 27" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 27" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 27" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 27" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 27" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 27" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 27" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 27" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 27" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 27" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 27" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 27" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 27" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 27" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 27" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 27" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 27" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 27" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 27" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 27" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 27" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 27" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 27" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 27" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 27" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 27" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 27" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 27" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 27" severity error;
			assert progcounter = x"00000040" report "progcounter error at step 27" severity error;
			wait for 5 ns;

		-- load instruction 14
			ck <= '0';
			instr <= x"00ff0eb7"; -- LUI : reg[29] = 0xff0 << 12
			wait for 5 ns;
			assert false report "14;0x00ff0eb7;LUI : reg[29] = 0xff0 << 12;OK; ;" severity note;
			assert progcounter = x"00000040" report "progcounter error at step 28" severity error;
			wait for 5 ns;

		-- execute instruction 14
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 29" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 29" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 29" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 29" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 29" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 29" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 29" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 29" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 29" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 29" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 29" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 29" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 29" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 29" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 29" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 29" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 29" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 29" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 29" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 29" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 29" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 29" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 29" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 29" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 29" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 29" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 29" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 29" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 29" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 29" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 29" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 29" severity error;
			assert progcounter = x"00000044" report "progcounter error at step 29" severity error;
			wait for 5 ns;

		-- load instruction 15
			ck <= '0';
			instr <= x"00c00e13"; -- ADDI : reg[28] = reg[00] + 12
			wait for 5 ns;
			assert false report "15;0x00c00e13;ADDI : reg[28] = reg[00] + 12;OK; ;" severity note;
			assert progcounter = x"00000044" report "progcounter error at step 30" severity error;
			wait for 5 ns;

		-- execute instruction 15
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 31" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 31" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 31" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 31" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 31" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 31" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 31" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 31" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 31" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 31" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 31" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 31" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 31" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 31" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 31" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 31" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 31" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 31" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 31" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 31" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 31" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 31" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 31" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 31" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 31" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 31" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 31" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 31" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 31" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 31" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 31" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 31" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 31" severity error;
			wait for 5 ns;

		-- load instruction 16
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "16;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 32" severity error;
			assert dataAddr = x"80000000"    report "address error at step 32"     severity error;
			assert dataLength = "010"        report "length error at step 32"      severity error;
			assert load = '1'                report "load error at step 32"        severity error;
			wait for 5 ns;

		-- execute instruction 16
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 33" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 33" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 33" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 33" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 33" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 33" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 33" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 33" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 33" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 33" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 33" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 33" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 33" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 33" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 33" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 33" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 33" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 33" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 33" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 33" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 33" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 33" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 33" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 33" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 33" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 33" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 33" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 33" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 33" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 33" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 33" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 33" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 33" severity error;
			wait for 5 ns;

		-- load instruction 17
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "17;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 34" severity error;
			wait for 5 ns;

		-- execute instruction 17
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 35" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 35" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 35" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 35" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 35" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 35" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 35" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 35" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 35" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 35" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 35" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 35" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 35" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 35" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 35" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 35" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 35" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 35" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 35" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 35" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 35" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 35" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 35" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 35" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 35" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 35" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 35" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 35" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 35" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 35" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 35" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 35" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 35" severity error;
			wait for 5 ns;

		-- load instruction 18
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "18;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 36" severity error;
			assert dataAddr = x"80000000"    report "address error at step 36"     severity error;
			assert dataLength = "010"        report "length error at step 36"      severity error;
			assert load = '1'                report "load error at step 36"        severity error;
			wait for 5 ns;

		-- execute instruction 18
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 37" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 37" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 37" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 37" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 37" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 37" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 37" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 37" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 37" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 37" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 37" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 37" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 37" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 37" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 37" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 37" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 37" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 37" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 37" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 37" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 37" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 37" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 37" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 37" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 37" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 37" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 37" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 37" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 37" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 37" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 37" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 37" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 37" severity error;
			wait for 5 ns;

		-- load instruction 19
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "19;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 38" severity error;
			wait for 5 ns;

		-- execute instruction 19
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 39" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 39" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 39" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 39" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 39" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 39" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 39" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 39" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 39" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 39" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 39" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 39" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 39" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 39" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 39" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 39" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 39" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 39" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 39" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 39" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 39" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 39" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 39" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 39" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 39" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 39" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 39" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 39" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 39" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 39" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 39" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 39" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 39" severity error;
			wait for 5 ns;

		-- load instruction 20
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "20;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 40" severity error;
			assert dataAddr = x"80000000"    report "address error at step 40"     severity error;
			assert dataLength = "010"        report "length error at step 40"      severity error;
			assert load = '1'                report "load error at step 40"        severity error;
			wait for 5 ns;

		-- execute instruction 20
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 41" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 41" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 41" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 41" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 41" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 41" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 41" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 41" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 41" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 41" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 41" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 41" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 41" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 41" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 41" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 41" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 41" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 41" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 41" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 41" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 41" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 41" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 41" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 41" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 41" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 41" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 41" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 41" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 41" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 41" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 41" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 41" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 41" severity error;
			wait for 5 ns;

		-- load instruction 21
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "21;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 42" severity error;
			wait for 5 ns;

		-- execute instruction 21
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 43" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 43" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 43" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 43" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 43" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 43" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 43" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 43" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 43" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 43" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 43" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 43" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 43" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 43" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 43" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 43" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 43" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 43" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 43" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 43" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 43" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 43" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 43" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 43" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 43" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 43" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 43" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 43" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 43" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 43" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 43" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 43" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 43" severity error;
			wait for 5 ns;

		-- load instruction 22
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "22;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 44" severity error;
			assert dataAddr = x"80000000"    report "address error at step 44"     severity error;
			assert dataLength = "010"        report "length error at step 44"      severity error;
			assert load = '1'                report "load error at step 44"        severity error;
			wait for 5 ns;

		-- execute instruction 22
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 45" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 45" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 45" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 45" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 45" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 45" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 45" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 45" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 45" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 45" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 45" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 45" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 45" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 45" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 45" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 45" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 45" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 45" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 45" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 45" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 45" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 45" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 45" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 45" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 45" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 45" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 45" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 45" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 45" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 45" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 45" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 45" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 45" severity error;
			wait for 5 ns;

		-- load instruction 23
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "23;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 46" severity error;
			wait for 5 ns;

		-- execute instruction 23
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 47" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 47" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 47" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 47" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 47" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 47" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 47" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 47" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 47" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 47" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 47" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 47" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 47" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 47" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 47" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 47" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 47" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 47" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 47" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 47" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 47" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 47" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 47" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 47" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 47" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 47" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 47" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 47" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 47" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 47" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 47" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 47" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 47" severity error;
			wait for 5 ns;

		-- load instruction 24
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "24;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 48" severity error;
			assert dataAddr = x"80000000"    report "address error at step 48"     severity error;
			assert dataLength = "010"        report "length error at step 48"      severity error;
			assert load = '1'                report "load error at step 48"        severity error;
			wait for 5 ns;

		-- execute instruction 24
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 49" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 49" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 49" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 49" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 49" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 49" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 49" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 49" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 49" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 49" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 49" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 49" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 49" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 49" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 49" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 49" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 49" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 49" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 49" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 49" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 49" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 49" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 49" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 49" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 49" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 49" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 49" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 49" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 49" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 49" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 49" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 49" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 49" severity error;
			wait for 5 ns;

		-- load instruction 25
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "25;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 50" severity error;
			wait for 5 ns;

		-- execute instruction 25
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 51" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 51" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 51" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 51" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 51" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 51" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 51" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 51" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 51" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 51" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 51" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 51" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 51" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 51" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 51" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 51" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 51" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 51" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 51" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 51" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 51" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 51" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 51" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 51" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 51" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 51" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 51" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 51" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 51" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 51" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 51" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 51" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 51" severity error;
			wait for 5 ns;

		-- load instruction 26
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "26;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 52" severity error;
			assert dataAddr = x"80000000"    report "address error at step 52"     severity error;
			assert dataLength = "010"        report "length error at step 52"      severity error;
			assert load = '1'                report "load error at step 52"        severity error;
			wait for 5 ns;

		-- execute instruction 26
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 53" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 53" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 53" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 53" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 53" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 53" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 53" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 53" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 53" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 53" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 53" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 53" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 53" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 53" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 53" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 53" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 53" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 53" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 53" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 53" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 53" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 53" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 53" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 53" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 53" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 53" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 53" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 53" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 53" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 53" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 53" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 53" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 53" severity error;
			wait for 5 ns;

		-- load instruction 27
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "27;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 54" severity error;
			wait for 5 ns;

		-- execute instruction 27
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 55" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 55" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 55" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 55" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 55" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 55" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 55" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 55" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 55" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 55" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 55" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 55" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 55" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 55" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 55" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 55" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 55" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 55" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 55" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 55" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 55" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 55" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 55" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 55" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 55" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 55" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 55" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 55" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 55" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 55" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 55" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 55" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 55" severity error;
			wait for 5 ns;

		-- load instruction 28
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "28;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 56" severity error;
			assert dataAddr = x"80000000"    report "address error at step 56"     severity error;
			assert dataLength = "010"        report "length error at step 56"      severity error;
			assert load = '1'                report "load error at step 56"        severity error;
			wait for 5 ns;

		-- execute instruction 28
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 57" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 57" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 57" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 57" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 57" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 57" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 57" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 57" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 57" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 57" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 57" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 57" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 57" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 57" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 57" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 57" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 57" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 57" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 57" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 57" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 57" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 57" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 57" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 57" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 57" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 57" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 57" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 57" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 57" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 57" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 57" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 57" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 57" severity error;
			wait for 5 ns;

		-- load instruction 29
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "29;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 58" severity error;
			wait for 5 ns;

		-- execute instruction 29
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 59" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 59" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 59" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 59" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 59" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 59" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 59" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 59" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 59" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 59" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 59" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 59" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 59" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 59" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 59" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 59" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 59" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 59" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 59" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 59" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 59" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 59" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 59" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 59" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 59" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 59" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 59" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 59" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 59" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 59" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 59" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 59" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 59" severity error;
			wait for 5 ns;

		-- load instruction 30
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "30;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 60" severity error;
			assert dataAddr = x"80000000"    report "address error at step 60"     severity error;
			assert dataLength = "010"        report "length error at step 60"      severity error;
			assert load = '1'                report "load error at step 60"        severity error;
			wait for 5 ns;

		-- execute instruction 30
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 61" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 61" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 61" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 61" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 61" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 61" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 61" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 61" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 61" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 61" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 61" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 61" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 61" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 61" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 61" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 61" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 61" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 61" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 61" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 61" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 61" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 61" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 61" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 61" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 61" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 61" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 61" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 61" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 61" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 61" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 61" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 61" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 61" severity error;
			wait for 5 ns;

		-- load instruction 31
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "31;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 62" severity error;
			wait for 5 ns;

		-- execute instruction 31
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 63" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 63" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 63" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 63" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 63" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 63" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 63" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 63" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 63" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 63" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 63" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 63" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 63" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 63" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 63" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 63" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 63" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 63" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 63" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 63" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 63" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 63" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 63" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 63" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 63" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 63" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 63" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 63" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 63" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 63" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 63" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 63" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 63" severity error;
			wait for 5 ns;

		-- load instruction 32
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "32;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 64" severity error;
			assert dataAddr = x"80000000"    report "address error at step 64"     severity error;
			assert dataLength = "010"        report "length error at step 64"      severity error;
			assert load = '1'                report "load error at step 64"        severity error;
			wait for 5 ns;

		-- execute instruction 32
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 65" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 65" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 65" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 65" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 65" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 65" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 65" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 65" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 65" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 65" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 65" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 65" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 65" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 65" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 65" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 65" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 65" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 65" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 65" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 65" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 65" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 65" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 65" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 65" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 65" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 65" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 65" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 65" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 65" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 65" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 65" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 65" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 65" severity error;
			wait for 5 ns;

		-- load instruction 33
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "33;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 66" severity error;
			wait for 5 ns;

		-- execute instruction 33
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 67" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 67" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 67" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 67" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 67" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 67" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 67" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 67" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 67" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 67" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 67" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 67" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 67" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 67" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 67" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 67" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 67" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 67" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 67" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 67" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 67" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 67" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 67" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 67" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 67" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 67" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 67" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 67" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 67" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 67" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 67" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 67" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 67" severity error;
			wait for 5 ns;

		-- load instruction 34
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "34;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 68" severity error;
			assert dataAddr = x"80000000"    report "address error at step 68"     severity error;
			assert dataLength = "010"        report "length error at step 68"      severity error;
			assert load = '1'                report "load error at step 68"        severity error;
			wait for 5 ns;

		-- execute instruction 34
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 69" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 69" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 69" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 69" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 69" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 69" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 69" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 69" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 69" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 69" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 69" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 69" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 69" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 69" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 69" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 69" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 69" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 69" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 69" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 69" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 69" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 69" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 69" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 69" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 69" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 69" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 69" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 69" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 69" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 69" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 69" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 69" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 69" severity error;
			wait for 5 ns;

		-- load instruction 35
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "35;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 70" severity error;
			wait for 5 ns;

		-- execute instruction 35
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 71" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 71" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 71" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 71" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 71" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 71" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 71" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 71" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 71" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 71" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 71" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 71" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 71" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 71" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 71" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 71" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 71" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 71" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 71" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 71" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 71" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 71" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 71" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 71" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 71" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 71" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 71" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 71" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 71" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 71" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 71" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 71" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 71" severity error;
			wait for 5 ns;

		-- load instruction 36
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "36;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 72" severity error;
			assert dataAddr = x"80000000"    report "address error at step 72"     severity error;
			assert dataLength = "010"        report "length error at step 72"      severity error;
			assert load = '1'                report "load error at step 72"        severity error;
			wait for 5 ns;

		-- execute instruction 36
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 73" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 73" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 73" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 73" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 73" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 73" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 73" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 73" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 73" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 73" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 73" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 73" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 73" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 73" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 73" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 73" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 73" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 73" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 73" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 73" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 73" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 73" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 73" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 73" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 73" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 73" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 73" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 73" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 73" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 73" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 73" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 73" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 73" severity error;
			wait for 5 ns;

		-- load instruction 37
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "37;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 74" severity error;
			wait for 5 ns;

		-- execute instruction 37
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 75" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 75" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 75" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 75" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 75" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 75" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 75" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 75" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 75" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 75" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 75" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 75" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 75" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 75" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 75" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 75" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 75" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 75" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 75" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 75" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 75" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 75" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 75" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 75" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 75" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 75" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 75" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 75" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 75" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 75" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 75" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 75" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 75" severity error;
			wait for 5 ns;

		-- load instruction 38
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "38;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 76" severity error;
			assert dataAddr = x"80000000"    report "address error at step 76"     severity error;
			assert dataLength = "010"        report "length error at step 76"      severity error;
			assert load = '1'                report "load error at step 76"        severity error;
			wait for 5 ns;

		-- execute instruction 38
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 77" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 77" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 77" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 77" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 77" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 77" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 77" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 77" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 77" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 77" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 77" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 77" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 77" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 77" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 77" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 77" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 77" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 77" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 77" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 77" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 77" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 77" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 77" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 77" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 77" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 77" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 77" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 77" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 77" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 77" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 77" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 77" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 77" severity error;
			wait for 5 ns;

		-- load instruction 39
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "39;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 78" severity error;
			wait for 5 ns;

		-- execute instruction 39
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 79" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 79" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 79" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 79" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 79" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 79" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 79" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 79" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 79" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 79" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 79" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 79" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 79" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 79" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 79" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 79" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 79" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 79" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 79" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 79" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 79" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 79" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 79" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 79" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 79" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 79" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 79" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 79" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 79" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 79" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 79" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 79" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 79" severity error;
			wait for 5 ns;

		-- load instruction 40
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "40;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 80" severity error;
			assert dataAddr = x"80000000"    report "address error at step 80"     severity error;
			assert dataLength = "010"        report "length error at step 80"      severity error;
			assert load = '1'                report "load error at step 80"        severity error;
			wait for 5 ns;

		-- execute instruction 40
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 81" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 81" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 81" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 81" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 81" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 81" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 81" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 81" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 81" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 81" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 81" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 81" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 81" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 81" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 81" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 81" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 81" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 81" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 81" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 81" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 81" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 81" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 81" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 81" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 81" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 81" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 81" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 81" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 81" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 81" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 81" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 81" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 81" severity error;
			wait for 5 ns;

		-- load instruction 41
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "41;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 82" severity error;
			wait for 5 ns;

		-- execute instruction 41
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 83" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 83" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 83" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 83" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 83" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 83" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 83" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 83" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 83" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 83" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 83" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 83" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 83" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 83" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 83" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 83" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 83" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 83" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 83" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 83" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 83" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 83" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 83" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 83" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 83" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 83" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 83" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 83" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 83" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 83" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 83" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 83" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 83" severity error;
			wait for 5 ns;

		-- load instruction 42
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "42;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 84" severity error;
			assert dataAddr = x"80000000"    report "address error at step 84"     severity error;
			assert dataLength = "010"        report "length error at step 84"      severity error;
			assert load = '1'                report "load error at step 84"        severity error;
			wait for 5 ns;

		-- execute instruction 42
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 85" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 85" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 85" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 85" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 85" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 85" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 85" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 85" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 85" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 85" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 85" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 85" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 85" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 85" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 85" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 85" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 85" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 85" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 85" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 85" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 85" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 85" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 85" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 85" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 85" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 85" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 85" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 85" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 85" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 85" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 85" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 85" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 85" severity error;
			wait for 5 ns;

		-- load instruction 43
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "43;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 86" severity error;
			wait for 5 ns;

		-- execute instruction 43
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 87" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 87" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 87" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 87" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 87" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 87" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 87" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 87" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 87" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 87" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 87" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 87" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 87" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 87" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 87" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 87" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 87" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 87" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 87" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 87" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 87" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 87" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 87" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 87" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 87" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 87" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 87" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 87" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 87" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 87" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 87" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 87" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 87" severity error;
			wait for 5 ns;

		-- load instruction 44
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "44;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 88" severity error;
			assert dataAddr = x"80000000"    report "address error at step 88"     severity error;
			assert dataLength = "010"        report "length error at step 88"      severity error;
			assert load = '1'                report "load error at step 88"        severity error;
			wait for 5 ns;

		-- execute instruction 44
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 89" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 89" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 89" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 89" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 89" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 89" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 89" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 89" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 89" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 89" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 89" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 89" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 89" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 89" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 89" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 89" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 89" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 89" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 89" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 89" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 89" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 89" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 89" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 89" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 89" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 89" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 89" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 89" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 89" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 89" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 89" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 89" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 89" severity error;
			wait for 5 ns;

		-- load instruction 45
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "45;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 90" severity error;
			wait for 5 ns;

		-- execute instruction 45
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 91" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 91" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 91" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 91" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 91" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 91" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 91" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 91" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 91" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 91" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 91" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 91" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 91" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 91" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 91" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 91" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 91" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 91" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 91" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 91" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 91" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 91" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 91" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 91" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 91" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 91" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 91" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 91" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 91" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 91" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 91" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 91" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 91" severity error;
			wait for 5 ns;

		-- load instruction 46
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "46;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 92" severity error;
			assert dataAddr = x"80000000"    report "address error at step 92"     severity error;
			assert dataLength = "010"        report "length error at step 92"      severity error;
			assert load = '1'                report "load error at step 92"        severity error;
			wait for 5 ns;

		-- execute instruction 46
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 93" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 93" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 93" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 93" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 93" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 93" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 93" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 93" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 93" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 93" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 93" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 93" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 93" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 93" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 93" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 93" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 93" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 93" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 93" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 93" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 93" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 93" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 93" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 93" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 93" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 93" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 93" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 93" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 93" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 93" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 93" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 93" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 93" severity error;
			wait for 5 ns;

		-- load instruction 47
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "47;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 94" severity error;
			wait for 5 ns;

		-- execute instruction 47
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 95" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 95" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 95" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 95" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 95" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 95" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 95" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 95" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 95" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 95" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 95" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 95" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 95" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 95" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 95" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 95" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 95" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 95" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 95" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 95" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 95" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 95" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 95" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 95" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 95" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 95" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 95" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 95" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 95" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 95" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 95" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 95" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 95" severity error;
			wait for 5 ns;

		-- load instruction 48
			ck <= '0';
			instr <= x"00052683"; -- LDW : reg[13] = dataMem[reg[10] + 0]
			outputData <= x"00000000";
			wait for 5 ns;
			assert false report "48;0x00052683;LDW : reg[13] = dataMem[reg[10] + 0];OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 96" severity error;
			assert dataAddr = x"80000000"    report "address error at step 96"     severity error;
			assert dataLength = "010"        report "length error at step 96"      severity error;
			assert load = '1'                report "load error at step 96"        severity error;
			wait for 5 ns;

		-- execute instruction 48
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 97" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 97" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 97" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 97" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 97" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 97" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 97" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 97" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 97" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 97" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 97" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 97" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 97" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 97" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 97" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 97" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 97" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 97" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 97" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 97" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 97" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 97" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 97" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 97" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 97" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 97" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 97" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 97" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 97" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 97" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 97" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 97" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 97" severity error;
			wait for 5 ns;

		-- load instruction 49
			ck <= '0';
			instr <= x"fed8fee3"; -- BGEU : if ( reg[17] > reg[13] ) PC = PC + -4
			wait for 5 ns;
			assert false report "49;0xfed8fee3;BGEU : if ( reg[17] > reg[13] ) PC = PC + -4;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 98" severity error;
			wait for 5 ns;

		-- execute instruction 49
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 99" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 99" severity error;
			assert reg02 = x"00000100" report "reg02 error at step 99" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 99" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 99" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 99" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 99" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 99" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 99" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 99" severity error;
			assert reg0a = x"80000000" report "reg0a error at step 99" severity error;
			assert reg0b = x"00000006" report "reg0b error at step 99" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 99" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 99" severity error;
			assert reg0e = x"fffffffe" report "reg0e error at step 99" severity error;
			assert reg0f = x"000000e8" report "reg0f error at step 99" severity error;
			assert reg10 = x"0000ff00" report "reg10 error at step 99" severity error;
			assert reg11 = x"017d783f" report "reg11 error at step 99" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 99" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 99" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 99" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 99" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 99" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 99" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 99" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 99" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 99" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 99" severity error;
			assert reg1c = x"0000000c" report "reg1c error at step 99" severity error;
			assert reg1d = x"00ff0000" report "reg1d error at step 99" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 99" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 99" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 99" severity error;
			wait for 5 ns;

			wait;
		end process;
END vhdl;