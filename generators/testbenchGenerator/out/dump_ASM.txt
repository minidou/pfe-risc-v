
main.elf:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <_start>:
   0:	00001137          	lui	sp,0x1
   4:	00c000ef          	jal	ra,10 <main>
   8:	00100073          	ebreak
   c:	0000006f          	j	c <_start+0xc>

00000010 <main>:
  10:	ff010113          	addi	sp,sp,-16 # ff0 <architectDesigner+0x914>
  14:	00112623          	sw	ra,12(sp)
  18:	00812423          	sw	s0,8(sp)
  1c:	01010413          	addi	s0,sp,16
  20:	00a00593          	li	a1,10
  24:	6dc00513          	li	a0,1756
  28:	248000ef          	jal	ra,270 <animL2RShift>
  2c:	00000013          	nop
  30:	00c12083          	lw	ra,12(sp)
  34:	00812403          	lw	s0,8(sp)
  38:	01010113          	addi	sp,sp,16
  3c:	00008067          	ret

00000040 <getCode7Seg>:
  40:	fe010113          	addi	sp,sp,-32
  44:	00812e23          	sw	s0,28(sp)
  48:	02010413          	addi	s0,sp,32
  4c:	00050793          	mv	a5,a0
  50:	fef407a3          	sb	a5,-17(s0)
  54:	fef44783          	lbu	a5,-17(s0)
  58:	fe078793          	addi	a5,a5,-32
  5c:	03f00713          	li	a4,63
  60:	14f76c63          	bltu	a4,a5,1b8 <getCode7Seg+0x178>
  64:	00279713          	slli	a4,a5,0x2
  68:	5dc00793          	li	a5,1500
  6c:	00f707b3          	add	a5,a4,a5
  70:	0007a783          	lw	a5,0(a5)
  74:	00078067          	jr	a5
  78:	00300793          	li	a5,3
  7c:	1400006f          	j	1bc <getCode7Seg+0x17c>
  80:	09f00793          	li	a5,159
  84:	1380006f          	j	1bc <getCode7Seg+0x17c>
  88:	02500793          	li	a5,37
  8c:	1300006f          	j	1bc <getCode7Seg+0x17c>
  90:	00d00793          	li	a5,13
  94:	1280006f          	j	1bc <getCode7Seg+0x17c>
  98:	09900793          	li	a5,153
  9c:	1200006f          	j	1bc <getCode7Seg+0x17c>
  a0:	04900793          	li	a5,73
  a4:	1180006f          	j	1bc <getCode7Seg+0x17c>
  a8:	04100793          	li	a5,65
  ac:	1100006f          	j	1bc <getCode7Seg+0x17c>
  b0:	01f00793          	li	a5,31
  b4:	1080006f          	j	1bc <getCode7Seg+0x17c>
  b8:	00100793          	li	a5,1
  bc:	1000006f          	j	1bc <getCode7Seg+0x17c>
  c0:	00900793          	li	a5,9
  c4:	0f80006f          	j	1bc <getCode7Seg+0x17c>
  c8:	01100793          	li	a5,17
  cc:	0f00006f          	j	1bc <getCode7Seg+0x17c>
  d0:	0c100793          	li	a5,193
  d4:	0e80006f          	j	1bc <getCode7Seg+0x17c>
  d8:	06300793          	li	a5,99
  dc:	0e00006f          	j	1bc <getCode7Seg+0x17c>
  e0:	08500793          	li	a5,133
  e4:	0d80006f          	j	1bc <getCode7Seg+0x17c>
  e8:	06100793          	li	a5,97
  ec:	0d00006f          	j	1bc <getCode7Seg+0x17c>
  f0:	07100793          	li	a5,113
  f4:	0c80006f          	j	1bc <getCode7Seg+0x17c>
  f8:	04300793          	li	a5,67
  fc:	0c00006f          	j	1bc <getCode7Seg+0x17c>
 100:	0d100793          	li	a5,209
 104:	0b80006f          	j	1bc <getCode7Seg+0x17c>
 108:	0df00793          	li	a5,223
 10c:	0b00006f          	j	1bc <getCode7Seg+0x17c>
 110:	08700793          	li	a5,135
 114:	0a80006f          	j	1bc <getCode7Seg+0x17c>
 118:	05100793          	li	a5,81
 11c:	0a00006f          	j	1bc <getCode7Seg+0x17c>
 120:	0e300793          	li	a5,227
 124:	0980006f          	j	1bc <getCode7Seg+0x17c>
 128:	05500793          	li	a5,85
 12c:	0900006f          	j	1bc <getCode7Seg+0x17c>
 130:	0d500793          	li	a5,213
 134:	0880006f          	j	1bc <getCode7Seg+0x17c>
 138:	0c500793          	li	a5,197
 13c:	0800006f          	j	1bc <getCode7Seg+0x17c>
 140:	03100793          	li	a5,49
 144:	0780006f          	j	1bc <getCode7Seg+0x17c>
 148:	01900793          	li	a5,25
 14c:	0700006f          	j	1bc <getCode7Seg+0x17c>
 150:	0f500793          	li	a5,245
 154:	0680006f          	j	1bc <getCode7Seg+0x17c>
 158:	04900793          	li	a5,73
 15c:	0600006f          	j	1bc <getCode7Seg+0x17c>
 160:	0e100793          	li	a5,225
 164:	0580006f          	j	1bc <getCode7Seg+0x17c>
 168:	0c300793          	li	a5,195
 16c:	0500006f          	j	1bc <getCode7Seg+0x17c>
 170:	0ad00793          	li	a5,173
 174:	0480006f          	j	1bc <getCode7Seg+0x17c>
 178:	0a900793          	li	a5,169
 17c:	0400006f          	j	1bc <getCode7Seg+0x17c>
 180:	09100793          	li	a5,145
 184:	0380006f          	j	1bc <getCode7Seg+0x17c>
 188:	08900793          	li	a5,137
 18c:	0300006f          	j	1bc <getCode7Seg+0x17c>
 190:	02500793          	li	a5,37
 194:	0280006f          	j	1bc <getCode7Seg+0x17c>
 198:	0ff00793          	li	a5,255
 19c:	0200006f          	j	1bc <getCode7Seg+0x17c>
 1a0:	0fe00793          	li	a5,254
 1a4:	0180006f          	j	1bc <getCode7Seg+0x17c>
 1a8:	0fd00793          	li	a5,253
 1ac:	0100006f          	j	1bc <getCode7Seg+0x17c>
 1b0:	0ef00793          	li	a5,239
 1b4:	0080006f          	j	1bc <getCode7Seg+0x17c>
 1b8:	07f00793          	li	a5,127
 1bc:	00078513          	mv	a0,a5
 1c0:	01c12403          	lw	s0,28(sp)
 1c4:	02010113          	addi	sp,sp,32
 1c8:	00008067          	ret

000001cc <getCounter32>:
 1cc:	fe010113          	addi	sp,sp,-32
 1d0:	00812e23          	sw	s0,28(sp)
 1d4:	02010413          	addi	s0,sp,32
 1d8:	800007b7          	lui	a5,0x80000
 1dc:	0007a783          	lw	a5,0(a5) # 80000000 <architectDesigner+0x7ffff924>
 1e0:	fef42623          	sw	a5,-20(s0)
 1e4:	fec42783          	lw	a5,-20(s0)
 1e8:	00078513          	mv	a0,a5
 1ec:	01c12403          	lw	s0,28(sp)
 1f0:	02010113          	addi	sp,sp,32
 1f4:	00008067          	ret

000001f8 <convertMillis2Counter>:
 1f8:	fd010113          	addi	sp,sp,-48
 1fc:	02812623          	sw	s0,44(sp)
 200:	03010413          	addi	s0,sp,48
 204:	fca42e23          	sw	a0,-36(s0)
 208:	fdc42783          	lw	a5,-36(s0)
 20c:	fef42623          	sw	a5,-20(s0)
 210:	fec42783          	lw	a5,-20(s0)
 214:	00078513          	mv	a0,a5
 218:	02c12403          	lw	s0,44(sp)
 21c:	03010113          	addi	sp,sp,48
 220:	00008067          	ret

00000224 <getstrlen>:
 224:	fd010113          	addi	sp,sp,-48
 228:	02812623          	sw	s0,44(sp)
 22c:	03010413          	addi	s0,sp,48
 230:	fca42e23          	sw	a0,-36(s0)
 234:	fe042623          	sw	zero,-20(s0)
 238:	0100006f          	j	248 <getstrlen+0x24>
 23c:	fec42783          	lw	a5,-20(s0)
 240:	00178793          	addi	a5,a5,1
 244:	fef42623          	sw	a5,-20(s0)
 248:	fdc42703          	lw	a4,-36(s0)
 24c:	fec42783          	lw	a5,-20(s0)
 250:	00f707b3          	add	a5,a4,a5
 254:	0007c783          	lbu	a5,0(a5)
 258:	fe0792e3          	bnez	a5,23c <getstrlen+0x18>
 25c:	fec42783          	lw	a5,-20(s0)
 260:	00078513          	mv	a0,a5
 264:	02c12403          	lw	s0,44(sp)
 268:	03010113          	addi	sp,sp,48
 26c:	00008067          	ret

00000270 <animL2RShift>:
 270:	fc010113          	addi	sp,sp,-64
 274:	02112e23          	sw	ra,60(sp)
 278:	02812c23          	sw	s0,56(sp)
 27c:	02912a23          	sw	s1,52(sp)
 280:	04010413          	addi	s0,sp,64
 284:	fca42623          	sw	a0,-52(s0)
 288:	fcb42423          	sw	a1,-56(s0)
 28c:	fc842783          	lw	a5,-56(s0)
 290:	00078513          	mv	a0,a5
 294:	f65ff0ef          	jal	ra,1f8 <convertMillis2Counter>
 298:	fca42e23          	sw	a0,-36(s0)
 29c:	800007b7          	lui	a5,0x80000
 2a0:	00478793          	addi	a5,a5,4 # 80000004 <architectDesigner+0x7ffff928>
 2a4:	fff00713          	li	a4,-1
 2a8:	00e7a023          	sw	a4,0(a5)
 2ac:	800007b7          	lui	a5,0x80000
 2b0:	00878793          	addi	a5,a5,8 # 80000008 <architectDesigner+0x7ffff92c>
 2b4:	fff00713          	li	a4,-1
 2b8:	00e7a023          	sw	a4,0(a5)
 2bc:	fcc42503          	lw	a0,-52(s0)
 2c0:	f65ff0ef          	jal	ra,224 <getstrlen>
 2c4:	fca42c23          	sw	a0,-40(s0)
 2c8:	fe042423          	sw	zero,-24(s0)
 2cc:	03c0006f          	j	308 <animL2RShift+0x98>
 2d0:	fcc42703          	lw	a4,-52(s0)
 2d4:	fe842783          	lw	a5,-24(s0)
 2d8:	00f707b3          	add	a5,a4,a5
 2dc:	0007c683          	lbu	a3,0(a5)
 2e0:	fcc42703          	lw	a4,-52(s0)
 2e4:	fe842783          	lw	a5,-24(s0)
 2e8:	00f704b3          	add	s1,a4,a5
 2ec:	00068513          	mv	a0,a3
 2f0:	d51ff0ef          	jal	ra,40 <getCode7Seg>
 2f4:	00050793          	mv	a5,a0
 2f8:	00f48023          	sb	a5,0(s1)
 2fc:	fe842783          	lw	a5,-24(s0)
 300:	00178793          	addi	a5,a5,1
 304:	fef42423          	sw	a5,-24(s0)
 308:	fe842703          	lw	a4,-24(s0)
 30c:	fd842783          	lw	a5,-40(s0)
 310:	fcf760e3          	bltu	a4,a5,2d0 <animL2RShift+0x60>
 314:	fe042623          	sw	zero,-20(s0)
 318:	2980006f          	j	5b0 <animL2RShift+0x340>
 31c:	eb1ff0ef          	jal	ra,1cc <getCounter32>
 320:	00050713          	mv	a4,a0
 324:	fdc42783          	lw	a5,-36(s0)
 328:	00e787b3          	add	a5,a5,a4
 32c:	fcf42a23          	sw	a5,-44(s0)
 330:	00000013          	nop
 334:	e99ff0ef          	jal	ra,1cc <getCounter32>
 338:	00050713          	mv	a4,a0
 33c:	fd442783          	lw	a5,-44(s0)
 340:	fef76ae3          	bltu	a4,a5,334 <animL2RShift+0xc4>
 344:	00600793          	li	a5,6
 348:	fef42423          	sw	a5,-24(s0)
 34c:	fec42703          	lw	a4,-20(s0)
 350:	fe842783          	lw	a5,-24(s0)
 354:	02f70e63          	beq	a4,a5,390 <animL2RShift+0x120>
 358:	fec42703          	lw	a4,-20(s0)
 35c:	fe842783          	lw	a5,-24(s0)
 360:	40f707b3          	sub	a5,a4,a5
 364:	fd842703          	lw	a4,-40(s0)
 368:	02e7f463          	bgeu	a5,a4,390 <animL2RShift+0x120>
 36c:	fec42703          	lw	a4,-20(s0)
 370:	fe842783          	lw	a5,-24(s0)
 374:	40f707b3          	sub	a5,a4,a5
 378:	fcc42703          	lw	a4,-52(s0)
 37c:	00f707b3          	add	a5,a4,a5
 380:	0007c783          	lbu	a5,0(a5)
 384:	00879793          	slli	a5,a5,0x8
 388:	fef42023          	sw	a5,-32(s0)
 38c:	0100006f          	j	39c <animL2RShift+0x12c>
 390:	000107b7          	lui	a5,0x10
 394:	f0078793          	addi	a5,a5,-256 # ff00 <architectDesigner+0xf824>
 398:	fef42023          	sw	a5,-32(s0)
 39c:	00500793          	li	a5,5
 3a0:	fef42423          	sw	a5,-24(s0)
 3a4:	fec42703          	lw	a4,-20(s0)
 3a8:	fe842783          	lw	a5,-24(s0)
 3ac:	04f70263          	beq	a4,a5,3f0 <animL2RShift+0x180>
 3b0:	fec42703          	lw	a4,-20(s0)
 3b4:	fe842783          	lw	a5,-24(s0)
 3b8:	40f707b3          	sub	a5,a4,a5
 3bc:	fd842703          	lw	a4,-40(s0)
 3c0:	02e7f863          	bgeu	a5,a4,3f0 <animL2RShift+0x180>
 3c4:	fec42703          	lw	a4,-20(s0)
 3c8:	fe842783          	lw	a5,-24(s0)
 3cc:	40f707b3          	sub	a5,a4,a5
 3d0:	fcc42703          	lw	a4,-52(s0)
 3d4:	00f707b3          	add	a5,a4,a5
 3d8:	0007c783          	lbu	a5,0(a5)
 3dc:	00078713          	mv	a4,a5
 3e0:	fe042783          	lw	a5,-32(s0)
 3e4:	00e7e7b3          	or	a5,a5,a4
 3e8:	fef42023          	sw	a5,-32(s0)
 3ec:	0100006f          	j	3fc <animL2RShift+0x18c>
 3f0:	fe042783          	lw	a5,-32(s0)
 3f4:	0ff7e793          	ori	a5,a5,255
 3f8:	fef42023          	sw	a5,-32(s0)
 3fc:	00400793          	li	a5,4
 400:	fef42423          	sw	a5,-24(s0)
 404:	fec42703          	lw	a4,-20(s0)
 408:	fe842783          	lw	a5,-24(s0)
 40c:	02f70e63          	beq	a4,a5,448 <animL2RShift+0x1d8>
 410:	fec42703          	lw	a4,-20(s0)
 414:	fe842783          	lw	a5,-24(s0)
 418:	40f707b3          	sub	a5,a4,a5
 41c:	fd842703          	lw	a4,-40(s0)
 420:	02e7f463          	bgeu	a5,a4,448 <animL2RShift+0x1d8>
 424:	fec42703          	lw	a4,-20(s0)
 428:	fe842783          	lw	a5,-24(s0)
 42c:	40f707b3          	sub	a5,a4,a5
 430:	fcc42703          	lw	a4,-52(s0)
 434:	00f707b3          	add	a5,a4,a5
 438:	0007c783          	lbu	a5,0(a5)
 43c:	01879793          	slli	a5,a5,0x18
 440:	fef42223          	sw	a5,-28(s0)
 444:	00c0006f          	j	450 <animL2RShift+0x1e0>
 448:	ff0007b7          	lui	a5,0xff000
 44c:	fef42223          	sw	a5,-28(s0)
 450:	00300793          	li	a5,3
 454:	fef42423          	sw	a5,-24(s0)
 458:	fec42703          	lw	a4,-20(s0)
 45c:	fe842783          	lw	a5,-24(s0)
 460:	04f70463          	beq	a4,a5,4a8 <animL2RShift+0x238>
 464:	fec42703          	lw	a4,-20(s0)
 468:	fe842783          	lw	a5,-24(s0)
 46c:	40f707b3          	sub	a5,a4,a5
 470:	fd842703          	lw	a4,-40(s0)
 474:	02e7fa63          	bgeu	a5,a4,4a8 <animL2RShift+0x238>
 478:	fec42703          	lw	a4,-20(s0)
 47c:	fe842783          	lw	a5,-24(s0)
 480:	40f707b3          	sub	a5,a4,a5
 484:	fcc42703          	lw	a4,-52(s0)
 488:	00f707b3          	add	a5,a4,a5
 48c:	0007c783          	lbu	a5,0(a5) # ff000000 <architectDesigner+0xfefff924>
 490:	01079793          	slli	a5,a5,0x10
 494:	00078713          	mv	a4,a5
 498:	fe442783          	lw	a5,-28(s0)
 49c:	00e7e7b3          	or	a5,a5,a4
 4a0:	fef42223          	sw	a5,-28(s0)
 4a4:	0140006f          	j	4b8 <animL2RShift+0x248>
 4a8:	fe442703          	lw	a4,-28(s0)
 4ac:	00ff07b7          	lui	a5,0xff0
 4b0:	00f767b3          	or	a5,a4,a5
 4b4:	fef42223          	sw	a5,-28(s0)
 4b8:	00200793          	li	a5,2
 4bc:	fef42423          	sw	a5,-24(s0)
 4c0:	fec42703          	lw	a4,-20(s0)
 4c4:	fe842783          	lw	a5,-24(s0)
 4c8:	04f70463          	beq	a4,a5,510 <animL2RShift+0x2a0>
 4cc:	fec42703          	lw	a4,-20(s0)
 4d0:	fe842783          	lw	a5,-24(s0)
 4d4:	40f707b3          	sub	a5,a4,a5
 4d8:	fd842703          	lw	a4,-40(s0)
 4dc:	02e7fa63          	bgeu	a5,a4,510 <animL2RShift+0x2a0>
 4e0:	fec42703          	lw	a4,-20(s0)
 4e4:	fe842783          	lw	a5,-24(s0)
 4e8:	40f707b3          	sub	a5,a4,a5
 4ec:	fcc42703          	lw	a4,-52(s0)
 4f0:	00f707b3          	add	a5,a4,a5
 4f4:	0007c783          	lbu	a5,0(a5) # ff0000 <architectDesigner+0xfef924>
 4f8:	00879793          	slli	a5,a5,0x8
 4fc:	00078713          	mv	a4,a5
 500:	fe442783          	lw	a5,-28(s0)
 504:	00e7e7b3          	or	a5,a5,a4
 508:	fef42223          	sw	a5,-28(s0)
 50c:	0180006f          	j	524 <animL2RShift+0x2b4>
 510:	fe442703          	lw	a4,-28(s0)
 514:	000107b7          	lui	a5,0x10
 518:	f0078793          	addi	a5,a5,-256 # ff00 <architectDesigner+0xf824>
 51c:	00f767b3          	or	a5,a4,a5
 520:	fef42223          	sw	a5,-28(s0)
 524:	00100793          	li	a5,1
 528:	fef42423          	sw	a5,-24(s0)
 52c:	fec42703          	lw	a4,-20(s0)
 530:	fe842783          	lw	a5,-24(s0)
 534:	04f70263          	beq	a4,a5,578 <animL2RShift+0x308>
 538:	fec42703          	lw	a4,-20(s0)
 53c:	fe842783          	lw	a5,-24(s0)
 540:	40f707b3          	sub	a5,a4,a5
 544:	fd842703          	lw	a4,-40(s0)
 548:	02e7f863          	bgeu	a5,a4,578 <animL2RShift+0x308>
 54c:	fec42703          	lw	a4,-20(s0)
 550:	fe842783          	lw	a5,-24(s0)
 554:	40f707b3          	sub	a5,a4,a5
 558:	fcc42703          	lw	a4,-52(s0)
 55c:	00f707b3          	add	a5,a4,a5
 560:	0007c783          	lbu	a5,0(a5)
 564:	00078713          	mv	a4,a5
 568:	fe442783          	lw	a5,-28(s0)
 56c:	00e7e7b3          	or	a5,a5,a4
 570:	fef42223          	sw	a5,-28(s0)
 574:	0100006f          	j	584 <animL2RShift+0x314>
 578:	fe442783          	lw	a5,-28(s0)
 57c:	0ff7e793          	ori	a5,a5,255
 580:	fef42223          	sw	a5,-28(s0)
 584:	800007b7          	lui	a5,0x80000
 588:	00478793          	addi	a5,a5,4 # 80000004 <architectDesigner+0x7ffff928>
 58c:	fe442703          	lw	a4,-28(s0)
 590:	00e7a023          	sw	a4,0(a5)
 594:	800007b7          	lui	a5,0x80000
 598:	00878793          	addi	a5,a5,8 # 80000008 <architectDesigner+0x7ffff92c>
 59c:	fe042703          	lw	a4,-32(s0)
 5a0:	00e7a023          	sw	a4,0(a5)
 5a4:	fec42783          	lw	a5,-20(s0)
 5a8:	00178793          	addi	a5,a5,1
 5ac:	fef42623          	sw	a5,-20(s0)
 5b0:	fd842783          	lw	a5,-40(s0)
 5b4:	00678793          	addi	a5,a5,6
 5b8:	fec42703          	lw	a4,-20(s0)
 5bc:	d6f760e3          	bltu	a4,a5,31c <animL2RShift+0xac>
 5c0:	00000013          	nop
 5c4:	00000013          	nop
 5c8:	03c12083          	lw	ra,60(sp)
 5cc:	03812403          	lw	s0,56(sp)
 5d0:	03412483          	lw	s1,52(sp)
 5d4:	04010113          	addi	sp,sp,64
 5d8:	00008067          	ret

Disassembly of section .rodata:

000005dc <.rodata>:
 5dc:	0198                	addi	a4,sp,192
 5de:	0000                	unimp
 5e0:	01b8                	addi	a4,sp,200
 5e2:	0000                	unimp
 5e4:	01b8                	addi	a4,sp,200
 5e6:	0000                	unimp
 5e8:	01b8                	addi	a4,sp,200
 5ea:	0000                	unimp
 5ec:	01b8                	addi	a4,sp,200
 5ee:	0000                	unimp
 5f0:	01b8                	addi	a4,sp,200
 5f2:	0000                	unimp
 5f4:	01b8                	addi	a4,sp,200
 5f6:	0000                	unimp
 5f8:	01b8                	addi	a4,sp,200
 5fa:	0000                	unimp
 5fc:	01b8                	addi	a4,sp,200
 5fe:	0000                	unimp
 600:	01b8                	addi	a4,sp,200
 602:	0000                	unimp
 604:	01b8                	addi	a4,sp,200
 606:	0000                	unimp
 608:	01b8                	addi	a4,sp,200
 60a:	0000                	unimp
 60c:	01b8                	addi	a4,sp,200
 60e:	0000                	unimp
 610:	01a8                	addi	a0,sp,200
 612:	0000                	unimp
 614:	01a0                	addi	s0,sp,200
 616:	0000                	unimp
 618:	01b8                	addi	a4,sp,200
 61a:	0000                	unimp
 61c:	0078                	addi	a4,sp,12
 61e:	0000                	unimp
 620:	0080                	addi	s0,sp,64
 622:	0000                	unimp
 624:	0088                	addi	a0,sp,64
 626:	0000                	unimp
 628:	0090                	addi	a2,sp,64
 62a:	0000                	unimp
 62c:	0098                	addi	a4,sp,64
 62e:	0000                	unimp
 630:	00a0                	addi	s0,sp,72
 632:	0000                	unimp
 634:	00a8                	addi	a0,sp,72
 636:	0000                	unimp
 638:	00b0                	addi	a2,sp,72
 63a:	0000                	unimp
 63c:	00b8                	addi	a4,sp,72
 63e:	0000                	unimp
 640:	00c0                	addi	s0,sp,68
 642:	0000                	unimp
 644:	01b8                	addi	a4,sp,200
 646:	0000                	unimp
 648:	01b8                	addi	a4,sp,200
 64a:	0000                	unimp
 64c:	01b8                	addi	a4,sp,200
 64e:	0000                	unimp
 650:	01b8                	addi	a4,sp,200
 652:	0000                	unimp
 654:	01b8                	addi	a4,sp,200
 656:	0000                	unimp
 658:	01b8                	addi	a4,sp,200
 65a:	0000                	unimp
 65c:	01b8                	addi	a4,sp,200
 65e:	0000                	unimp
 660:	00c8                	addi	a0,sp,68
 662:	0000                	unimp
 664:	00d0                	addi	a2,sp,68
 666:	0000                	unimp
 668:	00d8                	addi	a4,sp,68
 66a:	0000                	unimp
 66c:	00e0                	addi	s0,sp,76
 66e:	0000                	unimp
 670:	00e8                	addi	a0,sp,76
 672:	0000                	unimp
 674:	00f0                	addi	a2,sp,76
 676:	0000                	unimp
 678:	00f8                	addi	a4,sp,76
 67a:	0000                	unimp
 67c:	0100                	addi	s0,sp,128
 67e:	0000                	unimp
 680:	0108                	addi	a0,sp,128
 682:	0000                	unimp
 684:	0110                	addi	a2,sp,128
 686:	0000                	unimp
 688:	0118                	addi	a4,sp,128
 68a:	0000                	unimp
 68c:	0120                	addi	s0,sp,136
 68e:	0000                	unimp
 690:	0128                	addi	a0,sp,136
 692:	0000                	unimp
 694:	0130                	addi	a2,sp,136
 696:	0000                	unimp
 698:	0138                	addi	a4,sp,136
 69a:	0000                	unimp
 69c:	0140                	addi	s0,sp,132
 69e:	0000                	unimp
 6a0:	0148                	addi	a0,sp,132
 6a2:	0000                	unimp
 6a4:	0150                	addi	a2,sp,132
 6a6:	0000                	unimp
 6a8:	0158                	addi	a4,sp,132
 6aa:	0000                	unimp
 6ac:	0160                	addi	s0,sp,140
 6ae:	0000                	unimp
 6b0:	0168                	addi	a0,sp,140
 6b2:	0000                	unimp
 6b4:	0170                	addi	a2,sp,140
 6b6:	0000                	unimp
 6b8:	0178                	addi	a4,sp,140
 6ba:	0000                	unimp
 6bc:	0180                	addi	s0,sp,192
 6be:	0000                	unimp
 6c0:	0188                	addi	a0,sp,192
 6c2:	0000                	unimp
 6c4:	0190                	addi	a2,sp,192
 6c6:	0000                	unimp
 6c8:	01b8                	addi	a4,sp,200
 6ca:	0000                	unimp
 6cc:	01b8                	addi	a4,sp,200
 6ce:	0000                	unimp
 6d0:	01b8                	addi	a4,sp,200
 6d2:	0000                	unimp
 6d4:	01b8                	addi	a4,sp,200
 6d6:	0000                	unimp
 6d8:	01b0                	addi	a2,sp,200
	...

Disassembly of section .data:

000006dc <architectDesigner>:
 6dc:	5250                	lw	a2,36(a2)
 6de:	452d434f          	0x452d434f
 6e2:	2d204543          	0x2d204543
 6e6:	4c20                	lw	s0,88(s0)
 6e8:	4455                	li	s0,21
 6ea:	4349564f          	fnmadd.d	fa2,fs2,fs4,fs0,unknown
 6ee:	2d20                	fld	fs0,88(a0)
 6f0:	5020                	lw	s0,96(s0)
 6f2:	5541                	li	a0,-16
 6f4:	204c                	fld	fa1,128(s0)
 6f6:	202d                	jal	720 <architectDesigner+0x44>
 6f8:	554a                	lw	a0,176(sp)
 6fa:	494c                	lw	a1,20(a0)
 6fc:	5445                	li	s0,-15
 6fe:	4554                	lw	a3,12(a0)
 700:	2d20                	fld	fs0,88(a0)
 702:	5120                	lw	s0,96(a0)
 704:	4555                	li	a0,21
 706:	544e                	lw	s0,240(sp)
 708:	4e49                	li	t3,18
 70a:	2d20                	fld	fs0,88(a0)
 70c:	5920                	lw	s0,112(a0)
 70e:	5353554f          	fnmadd.d	fa0,ft6,fs5,fa0,unknown
 712:	4850554f          	fnmadd.s	fa0,ft0,ft5,fs1,unknown
 716:	2d20                	fld	fs0,88(a0)
 718:	5020                	lw	s0,96(s0)
 71a:	4549                	li	a0,18
 71c:	5252                	lw	tp,52(sp)
 71e:	0045                	c.nop	17

Disassembly of section .riscv.attributes:

00000000 <.riscv.attributes>:
   0:	1b41                	addi	s6,s6,-16
   2:	0000                	unimp
   4:	7200                	flw	fs0,32(a2)
   6:	7369                	lui	t1,0xffffa
   8:	01007663          	bgeu	zero,a6,14 <main+0x4>
   c:	0011                	c.nop	4
   e:	0000                	unimp
  10:	1004                	addi	s1,sp,32
  12:	7205                	lui	tp,0xfffe1
  14:	3376                	fld	ft6,376(sp)
  16:	6932                	flw	fs2,12(sp)
  18:	7032                	flw	ft0,44(sp)
  1a:	0030                	addi	a2,sp,8

Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347          	fmsub.d	ft6,ft6,ft4,ft7,rmm
   4:	2820                	fld	fs0,80(s0)
   6:	736f7263          	bgeu	t5,s6,72a <architectDesigner+0x4e>
   a:	6f6f7473          	csrrci	s0,0x6f6,30
   e:	2d6c                	fld	fa1,216(a0)
  10:	474e                	lw	a4,208(sp)
  12:	3120                	fld	fs0,96(a0)
  14:	322e                	fld	ft4,232(sp)
  16:	2e34                	fld	fa3,88(a2)
  18:	2e30                	fld	fa2,88(a2)
  1a:	3035                	jal	fffff846 <architectDesigner+0xfffff16a>
  1c:	5f30                	lw	a2,120(a4)
  1e:	3835                	jal	fffff85a <architectDesigner+0xfffff17e>
  20:	6534                	flw	fa3,72(a0)
  22:	3735                	jal	ffffff4e <architectDesigner+0xfffff872>
  24:	2965                	jal	4dc <animL2RShift+0x26c>
  26:	3120                	fld	fs0,96(a0)
  28:	2e31                	jal	344 <animL2RShift+0xd4>
  2a:	2e32                	fld	ft8,264(sp)
  2c:	0030                	addi	a2,sp,8
