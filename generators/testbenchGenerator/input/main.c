#include <stdint.h>

adresses in data memory
#define AD_CPT32     0x80000000
#define AD_DISPLAY1  0x80000004    // MSB=Hex3, Hex2, Hex1, LSB=Hex0 (Hex described in DE10LITE User manual)
#define AD_DISPLAY2  0x80000008    // MSB=X, X, Hex5, LSB=Hex4
#define AD_LED       0x8000000C
#define AD_SWITCH    0x80000010

uint8_t architectDesigner[] = "PROC-ECE - LUDOVIC - PAUL - JULIETTE - QUENTIN - YOUSSOUPH - PIERRE";

void animL2RShift(uint8_t *s, int32_t speed);
uint32_t getstrlen(uint8_t *s);
uint32_t convertMillis2Counter(uint32_t millis);
uint32_t convertCounter2Millis(uint32_t counter);
uint32_t getCounter32(void);
uint8_t getCode7Seg(uint8_t c);

void main(void) {
	animL2RShift(architectDesigner, 10);
	return;
}

// return the corresponding code of the specified uint8_t to be displayed on 7 segments display
// encoded value is coded as shown in "DE10Lite User Manual". '0' to light a segment.
uint8_t getCode7Seg(uint8_t c) {
	switch(c) {
		case '0' : return 0x03;
        case '1' : return 0x9f;
        case '2' : return 0x25;
        case '3' : return 0x0d;
        case '4' : return 0x99;
        case '5' : return 0x49;
        case '6' : return 0x41;
        case '7' : return 0x1f;
        case '8' : return 0x01;
        case '9' : return 0x09;
        case 'A' : return 0x11;
        case 'B' : return 0xc1;
        case 'C' : return 0x63;
        case 'D' : return 0x85;
        case 'E' : return 0x61;
        case 'F' : return 0x71;
		case 'G' : return 0x43;
        case 'H' : return 0xD1;
        case 'I' : return 0xDF;
        case 'J' : return 0x87;
        case 'K' : return 0x51;
        case 'L' : return 0xE3;
        case 'M' : return 0x55;
        case 'N' : return 0xD5;
        case 'O' : return 0xC5;
		case 'P' : return 0x31;
		case 'Q' : return 0x19;
		case 'R' : return 0xf5;
		case 'S' : return 0x49;
		case 'T' : return 0xe1;
		case 'U' : return 0xc3;
		case 'V' : return 0xad;
		case 'W' : return 0xa9;
		case 'X' : return 0x91;
		case 'Y' : return 0x89;
		case 'Z' : return 0x25;
		case ' ' : return 0xff;
		case '.' : return 0xfe;
		case '-' : return 0xfd;
		case '_' : return 0xef;
		default  : return 0x7f;
	}
}

// 32 counter
uint32_t getCounter32(void) {
	uint32_t ret;
	ret=*(uint32_t *)AD_CPT32;
	return ret;
}

// convert a counter 50MHz value int16_to a millisecond value
/*uint32_t convertCounter2Millis(uint32_t counter) {
	uint32_t millis;
	millis=counter/50000;
	return millis;
}
*/
// convert a millisecond value int16_to a counter 50MHz value 
uint32_t convertMillis2Counter(uint32_t millis) {
	uint32_t ret;
	ret=millis*1;
	return ret;
}

// return the string length.
uint32_t getstrlen(uint8_t *s) {
	uint32_t i;
	for(i=0; s[i]!=0; i++);
	return i;
}


// animation : string shifted from Left to Right. speed= time in millis between each animation step.
void animL2RShift(uint8_t *s, int32_t speed) {
	uint32_t laps, time, strLength, pos, i;
	uint32_t disp1, disp2;
	laps=convertMillis2Counter(speed);
	// turn off all displays
	// *(uint32_t *)AD_DISPLAY1=0xffffffff;
	// *(uint32_t *)AD_DISPLAY2=0xffffffff;

	// find string length
	strLength=getstrlen(s);
	
	// convert ASCII code in 7 seg code
	for(i=0; i<strLength; i++) s[i]=getCode7Seg(s[i]);
	
	for(pos=0; pos<strLength+6; pos++) {
		// wait time for the new animation step
		time=getCounter32()+laps;
		// while(time>getCounter32());
		
		// hex5
		i=6;
		if((pos-i)>0 && (pos-i)<strLength) disp2=((uint8_t)s[pos-i] &0xff) << 8; else disp2=0xff << 8;
		// hex4
		i=5;
		if((pos-i)>0 && (pos-i)<strLength) disp2|=((uint8_t)s[pos-i] &0xff); else disp2|=0xff;
		// hex3
		i=4;
		if((pos-i)>0 && (pos-i)<strLength) disp1=((uint8_t)s[pos-i] &0xff) << 24; else disp1=0xff << 24;
		// hex2
		i=3;
		if((pos-i)>0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff) << 16; else disp1|=0xff << 16;
		// hex1
		i=2;
		if((pos-i)>0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff) << 8; else disp1|=0xff << 8;
		// hex0
		i=1;
		if((pos-i)>0 && (pos-i)<strLength) disp1|=((uint8_t)s[pos-i] &0xff); else disp1|=0xff;
		
		printf("0x%x 0x%x\n", disp1, disp2);
	}
}
