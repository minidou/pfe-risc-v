#!/bin/bash
export  PATH="$PATH:../tools/riscv32-unknown-elf/bin/"

riscv32-unknown-elf-as -march=rv32i -mabi=ilp32 ../gcc-riscv/res/vectors.s -o vectors.o
riscv32-unknown-elf-gcc -march=rv32i -mabi=ilp32 -nostdlib -nostartfiles -ffreestanding -c input/main.c -o main.o
riscv32-unknown-elf-ld vectors.o main.o -T ../gcc-riscv/res/memmap -o main.elf
riscv32-unknown-elf-objcopy main.elf -O binary input/main.bin

../simulator/code/simulator.sh input/main.bin ./output/testbench.vhd 0

rm main.elf main.o vectors.o