#!/bin/bash

export  PATH="$PATH:../../../tools/MinGW/bin/"

# gcc code/simulator.c -o code/simulator.sh
gcc DataMemGenerator.c -o DataMemGenerator.exe

cp DataMemGenerator.exe ../example/DataMemGenerator.exe