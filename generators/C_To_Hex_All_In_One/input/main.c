#include <stdint.h>

// adresses in data memory
#define AD_CPT32    *((volatile uint32_t *)0x80000000)
#define AD_DISPLAY1 *((volatile uint32_t *)0x80000004)   // MSB=Hex3, Hex2, Hex1, LSB=Hex0 (Hex described in DE10LITE User manual)
#define AD_DISPLAY2 *((volatile uint32_t *)0x80000008)   // MSB=X, X, Hex5, LSB=Hex4
#define MASTER_CLK_RATE 50 * 1000000
#define PLL_DIV  50
#define CLK_RATE    MASTER_CLK_RATE/PLL_DIV
#define MS_TICK    CLK_RATE/1000

struct stDisplays
{
    uint32_t disp1;
    uint16_t disp2;
};

const struct stDisplays s[] = {
    {.disp2 = 0xffff, .disp1 = 0xffffffff}, //
    {.disp2 = 0xffff, .disp1 = 0xffffff8c}, //      P
    {.disp2 = 0xffff, .disp1 = 0xffff8cce}, //     PR
    {.disp2 = 0xffff, .disp1 = 0xff8ccec0}, //    PRO
    {.disp2 = 0xffff, .disp1 = 0x8ccec0c6}, //   PROC
    {.disp2 = 0xff8c, .disp1 = 0xcec0c6bf}, //  PROC-
    {.disp2 = 0x8cce, .disp1 = 0xc0c6bf86}, // PROC-E
    {.disp2 = 0xcec0, .disp1 = 0xc6bf86c6}, // ROC-EC
    {.disp2 = 0xc0c6, .disp1 = 0xbf86c686}, // OC-ECE
    {.disp2 = 0xc6bf, .disp1 = 0x86c686ff}, // C-ECE
    {.disp2 = 0xbf86, .disp1 = 0xc686ffff}, // -ECE
    {.disp2 = 0x86c6, .disp1 = 0x86ffffff}, // ECE
    {.disp2 = 0xc686, .disp1 = 0xffffffff}, // CE
    {.disp2 = 0x86ff, .disp1 = 0xffffffff}  // E
};

void main(void)
{
    for(;;)
    {
        for (uint8_t i = 0 ; i < sizeof(s)/sizeof(struct stDisplays) ; i++)
        {
            AD_DISPLAY1 = s[i].disp1;
            AD_DISPLAY2 = s[i].disp2;
            while(AD_CPT32 < (MS_TICK * 500));  //500ms delay
            AD_CPT32 = 0;
        }
    }
}