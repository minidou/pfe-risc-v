#!/bin/bash
export  PATH="$PATH:../../tools/riscv32-unknown-elf/bin/"

riscv32-unknown-elf-as -march=rv32i -mabi=ilp32 res/vectors.s -o vectors.o
riscv32-unknown-elf-gcc -march=rv32i -mabi=ilp32 -nostdlib -nostartfiles -ffreestanding -c -Os input/main.c -o main.o
riscv32-unknown-elf-ld vectors.o main.o -T res/memmap -o main.elf
riscv32-unknown-elf-objdump -D main.elf > output/bin_asm/dump.asm
riscv32-unknown-elf-objcopy main.elf -O binary output/bin_asm/main.bin
rm main.elf main.o vectors.o 

./res/DataMemGenerator.exe 1000 output/bin_asm/main.bin output/vhd/InstructionMemory8.vhd output/vhd/InstructionMemory32.vhd output/vhd/DataMemory.vhd
./res/intelHexGenerator.exe 1000 output/bin_asm/main.bin output/hex/RAMhex output/hex/IMhex.hex
