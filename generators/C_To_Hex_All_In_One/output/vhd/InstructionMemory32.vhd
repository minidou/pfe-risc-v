-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM
-- Instruction Memory VHDL

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity InstructionMemory is
	port (
		-- INPUTS
		IMclock			: in std_logic;
		IMreset			: in std_logic;
		IMprogcounter	: in std_logic_vector(31 downto 0);
		-- OUTPUTS
		IMout				: out std_logic_vector(31 downto 0)
	);
end entity;

-- ARCHITECTURE
architecture archi of InstructionMemory is
	type mem is array(0 to 46) of std_logic_vector(31 downto 0);
	signal SigIMmemory : mem :=(
		x"37110000",
		x"ef00c000",
		x"73001000",
		x"6f000000",
		x"b7a60700",
		x"37070080",
		x"9386f611",
		x"9307c004",
		x"93850707",
		x"03a50700",
		x"2322a700",
		x"03d54700",
		x"2324a700",
		x"03250700",
		x"e3fea6fe",
		x"23200700",
		x"93878700",
		x"e390b7fe",
		x"6ff05ffd",
		x"ffffffff",
		x"ffff0000",
		x"8cffffff",
		x"ffff0000",
		x"ce8cffff",
		x"ffff0000",
		x"c0ce8cff",
		x"ffff0000",
		x"c6c0ce8c",
		x"ffff0000",
		x"bfc6c0ce",
		x"8cff0000",
		x"86bfc6c0",
		x"ce8c0000",
		x"c686bfc6",
		x"c0ce0000",
		x"86c686bf",
		x"c6c00000",
		x"ff86c686",
		x"bfc60000",
		x"ffff86c6",
		x"86bf0000",
		x"ffffff86",
		x"c6860000",
		x"ffffffff",
		x"86c60000",
		x"ffffffff",
		x"ff860000"
	);
	
	signal sigad : integer;
	signal sigpc : std_logic_vector(2 downto 0);
	
begin

	sigpc <= IMprogcounter(4 downto 2);
	IMout <= SigIMmemory(sigad) when rising_edge(IMclock);
	Sigad <= 0 when ((unsigned(sigpc) > 46)) else to_integer(unsigned(sigpc));

end archi;
-- END FILE