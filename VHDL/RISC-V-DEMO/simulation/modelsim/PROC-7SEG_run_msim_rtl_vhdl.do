transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/db {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/db/pll_altpll.v}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/Top.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/RegisterFile.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/ProgramCounter.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/Processor.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/InstructionMemory.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/InstructionDecoder.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/Displays.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/DataMemory.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/Counter.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/Alu.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/PLL.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/RAM00.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/RAM08.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/RAM16.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/RAM24.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/IM.vhd}

vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/RISC-V-DEMO/TestBenchTop.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fiftyfivenm -L rtl_work -L work -voptargs="+acc"  TestBenchTop

add wave *
view structure
view signals
run -all
