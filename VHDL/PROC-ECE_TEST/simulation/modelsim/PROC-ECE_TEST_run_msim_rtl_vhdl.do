transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/RegisterFile.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/ProgramCounter.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/Processor.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/InstructionDecoder.vhd}
vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/Alu.vhd}

vcom -93 -work work {C:/Users/Bernacchia/Desktop/PFE/PFE_Risc-V/VHDL/PROC-ECE_TEST/TestBench.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fiftyfivenm -L rtl_work -L work -voptargs="+acc"  TestBench

add wave *
view structure
view signals
run -all
