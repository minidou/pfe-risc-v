library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testbench is
	
end testbench;

architecture vhdl of testbench is

	component Top is
		Port (
			-- INPUTS
			TOPclock		: in std_logic;
			TOPreset		: in std_logic;
			TOPresetIM	: in std_logic;
			TOPinstr		: in std_logic_vector(31 downto 0);
			-- INOUTS
			-- 32 registers of register file
			TOPprogcounter	: inout std_logic_vector(31 downto 0);
			
			TOPreg00	: inout std_logic_vector(31 downto 0);
			TOPreg01	: inout std_logic_vector(31 downto 0);
			TOPreg02	: inout std_logic_vector(31 downto 0);
			TOPreg03	: inout std_logic_vector(31 downto 0);
			TOPreg04	: inout std_logic_vector(31 downto 0);
			TOPreg05	: inout std_logic_vector(31 downto 0);
			TOPreg06	: inout std_logic_vector(31 downto 0);
			TOPreg07	: inout std_logic_vector(31 downto 0);
			TOPreg08	: inout std_logic_vector(31 downto 0);
			TOPreg09	: inout std_logic_vector(31 downto 0);
			TOPreg0A	: inout std_logic_vector(31 downto 0);
			TOPreg0B	: inout std_logic_vector(31 downto 0);
			TOPreg0C	: inout std_logic_vector(31 downto 0);
			TOPreg0D	: inout std_logic_vector(31 downto 0);
			TOPreg0E	: inout std_logic_vector(31 downto 0);
			TOPreg0F	: inout std_logic_vector(31 downto 0);
			TOPreg10	: inout std_logic_vector(31 downto 0);
			TOPreg11	: inout std_logic_vector(31 downto 0);
			TOPreg12	: inout std_logic_vector(31 downto 0);
			TOPreg13	: inout std_logic_vector(31 downto 0);
			TOPreg14	: inout std_logic_vector(31 downto 0);
			TOPreg15	: inout std_logic_vector(31 downto 0);
			TOPreg16	: inout std_logic_vector(31 downto 0);
			TOPreg17	: inout std_logic_vector(31 downto 0);
			TOPreg18	: inout std_logic_vector(31 downto 0);
			TOPreg19	: inout std_logic_vector(31 downto 0);
			TOPreg1A	: inout std_logic_vector(31 downto 0);
			TOPreg1B	: inout std_logic_vector(31 downto 0);
			TOPreg1C	: inout std_logic_vector(31 downto 0);
			TOPreg1D	: inout std_logic_vector(31 downto 0);
			TOPreg1E	: inout std_logic_vector(31 downto 0);
			TOPreg1F	: inout std_logic_vector(31 downto 0)
		);
	end component;
		
	signal ck, reset, resetIM : STD_LOGIC;
	signal instr, progcounter: std_logic_vector(31 downto 0);	
	signal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);
	
	BEGIN

	--instanciation de l'entité Top
	itop : Top port map (
		TOPclock => ck,
		TOPreset => reset,
		TOPresetIM => resetIM,
		TOPprogcounter => progcounter,
		TOPinstr => instr,
		TOPreg00	=> reg00,
		TOPreg01	=> reg01,
		TOPreg02	=> reg02,
		TOPreg03	=> reg03,
		TOPreg04	=> reg04,
		TOPreg05	=> reg05,
		TOPreg06	=> reg06,
		TOPreg07	=> reg07,
		TOPreg08	=> reg08,
		TOPreg09	=> reg09,
		TOPreg0A	=> reg0A,
		TOPreg0B	=> reg0B,
		TOPreg0C	=> reg0C,
		TOPreg0D	=> reg0D,
		TOPreg0E	=> reg0E,
		TOPreg0F	=> reg0F,
		TOPreg10	=> reg10,
		TOPreg11	=> reg11,
		TOPreg12	=> reg12,
		TOPreg13	=> reg13,
		TOPreg14	=> reg14,
		TOPreg15	=> reg15,
		TOPreg16	=> reg16,
		TOPreg17	=> reg17,
		TOPreg18	=> reg18,
		TOPreg19	=> reg19,
		TOPreg1A	=> reg1A,
		TOPreg1B	=> reg1B,
		TOPreg1C	=> reg1C,
		TOPreg1D	=> reg1D,
		TOPreg1E	=> reg1E,
		TOPreg1F	=> reg1F
	);	
	
	VecteurTest : process
		begin
		
		-- init  simulation
			ck <= '0';
			reset <= '1';
			instr <= x"00000000"; -- init
			wait for 10 ns;
			ck <= '1';
			wait for 5 ns;
			reset <= '0';
			resetIM <= '0';	
			wait for 5 ns;

		-- load instruction 0
			ck <= '0';
			wait for 5 ns;
			instr <= x"00800713"; -- ADDI : reg[14] = reg[00] + 8
			assert reg00 = x"00000000" report "reg error at step 0" severity error;
			assert progcounter = x"00000000" report "progcounter error at step 0" severity error;
			wait for 5 ns;

		-- execute instruction 0
			ck <= '1';
			wait for 5 ns;
			assert reg0e = x"00000008" report "reg error at step 1" severity error;
			assert progcounter = x"00000004" report "progcounter error at step 1" severity error;
			wait for 5 ns;

		-- load instruction 1
			ck <= '0';
			wait for 5 ns;
			instr <= x"0ff00793"; -- ADDI : reg[15] = reg[00] + 255
			assert reg0f = x"00000000" report "reg error at step 2" severity error;
			assert progcounter = x"00000004" report "progcounter error at step 2" severity error;
			wait for 5 ns;

		-- execute instruction 1
			ck <= '1';
			wait for 5 ns;
			assert reg0f = x"000000ff" report "reg error at step 3" severity error;
			assert progcounter = x"00000008" report "progcounter error at step 3" severity error;
			wait for 5 ns;

		-- load instruction 2
			ck <= '0';
			wait for 5 ns;
			instr <= x"00f72423"; -- STOREW : dataMem[reg[14] + 8] = reg[15]
			-- assert addr = x"00000010" report "addr error at step 4" severity error   !!! A SORTIR !!!
           -- assert dataIn = x"000000ff" report "reg error at step 4" severity error  !!! A SORTIR !!!
			assert progcounter = x"00000008" report "progcounter error at step 4" severity error;
			wait for 5 ns;

		-- execute instruction 2
			ck <= '1';
			wait for 5 ns;
			assert progcounter = x"0000000c" report "progcounter error at step 5" severity error;
			wait for 5 ns;

		-- load instruction 3
			ck <= '0';
			wait for 5 ns;
			instr <= x"00872703"; -- LOADW : reg[14] = dataMem[reg[14] + 8]
			assert reg0e = x"00000008" report "reg error at step 6" severity error;
            -- assert addr = x"00000010" report "addr error at step 6" severity error   !!! A SORTIR !!!
			assert progcounter = x"0000000c" report "progcounter error at step 6" severity error;
			wait for 5 ns;

		-- execute instruction 3
			ck <= '1';
			wait for 5 ns;
			assert reg0e = x"000000ff" report "reg error at step 7" severity error;
			assert progcounter = x"00000010" report "progcounter error at step 7" severity error;
			wait for 5 ns;

		-- load instruction 4
			ck <= '0';
			wait for 5 ns;
			instr <= x"00f70663"; -- BEQ : "if ( reg[14] == reg[15] ) PC = PC + 12
			assert reg0e = x"000000ff" report "reg error at step 8" severity error;
			assert reg0f = x"000000ff" report "reg error at step 8" severity error;
			assert progcounter = x"00000010" report "progcounter error at step 8" severity error;
			wait for 5 ns;
            
		-- execute instruction 4
			ck <= '1';
			wait for 5 ns;
			assert reg0e = x"000000ff" report "reg error at step 9" severity error;
			assert progcounter = x"0000001c" report "progcounter error at step 9" severity error;
			wait for 5 ns;
            
			assert false report "Execution done without error" severity note;
			wait;		
        end process ;
END vhdl;